/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.pintuan;

import cn.shoptnt.model.trade.cart.vo.CartSkuOriginVo;
import cn.shoptnt.model.trade.cart.vo.CartView;

/**
 * Created by 妙贤 on 2019-01-23.
 * 拼团购物车业务类接口
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-01-23
 */
public interface PintuanCartManager  {


    /**
     * 获取拼团购物车
     * @return 购物车视图
     */
    CartView  getCart();


    /**
     * 将一个拼团的sku加入到购物车中
     * @param skuId 商品sku id
     * @param num 加入的数量
     * @return 购物车原始数据
     */
    CartSkuOriginVo addSku(Long skuId, Integer num);




}
