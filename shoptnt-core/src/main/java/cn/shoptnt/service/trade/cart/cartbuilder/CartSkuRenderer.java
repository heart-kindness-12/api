/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.cart.cartbuilder;

import cn.shoptnt.model.trade.cart.enums.CartType;
import cn.shoptnt.model.trade.cart.enums.CheckedWay;
import cn.shoptnt.model.trade.cart.vo.CartVO;
import cn.shoptnt.service.trade.cart.cartbuilder.impl.CartSkuFilter;

import java.util.List;

/**
 * 购物车sku渲染器，负责生产一个全新的cartList<br/>
 * 此步生产的物料是{@link cn.shoptnt.model.trade.cart.vo.CartSkuOriginVo}
 * 文档请参考：<br>
 * <a href="http://doc.shoptnt.cn/current/achitecture/jia-gou/ding-dan/cart-and-checkout.html" >购物车架构</a>
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/12/11
 */
public interface CartSkuRenderer {

    /**
     * 渲染sku数据
     */
    void renderSku(List<CartVO> cartList, CartType cartType, CheckedWay way);

    /**
     * 渲染sku数据
     * 带过滤器式的
     */
    void renderSku(List<CartVO> cartList, CartSkuFilter cartFilter, CartType cartType, CheckedWay way);
}
