/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.goodssearch.impl;

import cn.shoptnt.client.member.ShopCatClient;
import cn.shoptnt.framework.elasticsearch.ElasticJestConfig;
import cn.shoptnt.framework.elasticsearch.ElasticOperationUtil;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.HexUtils;
import cn.shoptnt.model.errorcode.GoodsErrorCode;
import cn.shoptnt.model.goods.dos.CategoryDO;
import cn.shoptnt.model.goodssearch.GoodsIndex;
import cn.shoptnt.model.goodssearch.Param;
import cn.shoptnt.service.goods.CategoryManager;
import cn.shoptnt.service.goodssearch.GoodsIndexManager;
import cn.shoptnt.service.goodssearch.GoodsWordsManager;
import cn.shoptnt.model.shop.dos.ShopCatDO;
import cn.shoptnt.model.system.vo.TaskProgress;
import cn.shoptnt.model.system.vo.TaskProgressConstant;
import cn.shoptnt.model.util.progress.ProgressManager;
import cn.shoptnt.framework.elasticsearch.EsSettings;
import cn.shoptnt.framework.logs.Debugger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.util.StringUtil;
import io.searchbox.client.JestClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * es的商品索引实现
 *
 * @author fk
 * @version v6.4
 * @since v6.4
 * 2017年9月18日 上午11:41:44
 */
@Service
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "cluster")
public class GoodsIndexManagerImpl implements GoodsIndexManager {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private CategoryManager categoryManager;

    @Autowired
    protected ShopCatClient shopCatClient;

    @Autowired
    protected ProgressManager progressManager;

    @Autowired
    protected GoodsWordsManager goodsWordsManager;

    @Autowired
    protected Debugger debugger;

    @Autowired
    protected ElasticJestConfig elasticJestConfig;

    @Autowired
    private JestClient jestClient;

    /**
     * 将某个商品加入索引<br>
     * @param goods 商品数据
     */
    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public boolean addIndex(Map goods) {
        String goodsName = goods.get("goods_name").toString();

        //配置文件中定义的索引名字
        String indexName = elasticJestConfig.getIndexName() + "_" + EsSettings.GOODS_INDEX_NAME;
        List<String> wordsList = toWordsList(goodsName);
        // 分词入库
        this.wordsToDb(wordsList);

        GoodsIndex goodsIndex = this.getSource(goods);

        boolean result = ElasticOperationUtil.insert(jestClient,indexName,  goodsIndex);
        if (!result) {
            logger.error("为商品[" + goodsName + "]生成索引异常");
            debugger.log("为商品[" + goodsName + "]生成索引异常");

        }

        return result;

    }
    /**
     * 更新某个商品的索引
     * @param goods 商品数据
     */
    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void updateIndex(Map goods) {

        //删除
        this.deleteIndex(goods);
        //添加
        this.addIndex(goods);

    }
    /**
     * 更新
     * @param goods 商品数据
     */
    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void deleteIndex(Map goods) {

        ///配置文件中定义的索引名字
        String indexName = elasticJestConfig.getIndexName() + "_" + EsSettings.GOODS_INDEX_NAME;

        //构建删除请求
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.termQuery("goodsId", goods.get("goods_id").toString()));
        boolean result = ElasticOperationUtil.deleteByQuery(jestClient,indexName,searchSourceBuilder.toString());
        if (!result) {
            throw new ServiceException(GoodsErrorCode.E310.code(),"删除商品索引异常");
        }

        String goodsName = goods.get("goods_name").toString();
        List<String> wordsList = toWordsList(goodsName);
        this.deleteWords(wordsList);

    }

    /**
     * 将list中的分词减一
     *
     * @param wordsList
     */
    protected void deleteWords(List<String> wordsList) {
        wordsList = removeDuplicate(wordsList);
        for (String words : wordsList) {
            this.goodsWordsManager.delete(words);
        }
    }

    /**
     * 封装成内存需要格式数据
     *
     * @param goods
     * @return
     */
    protected GoodsIndex getSource(Map goods) {
        GoodsIndex goodsIndex = new GoodsIndex();
        goodsIndex.setGoodsId(StringUtil.toLong(goods.get("goods_id").toString(), 0));
        goodsIndex.setName(goods.get("goods_name").toString());
        goodsIndex.setThumbnail(goods.get("thumbnail") == null ? "" : goods.get("thumbnail").toString());
        goodsIndex.setSmall(goods.get("small") == null ? "" : goods.get("small").toString());
        Double p = goods.get("price") == null ? 0d : StringUtil.toDouble(goods.get("price").toString(), 0d);
        goodsIndex.setPrice(p);
        Double discountPrice = goods.get("discount_price") == null ? 0d : StringUtil.toDouble(goods.get("discount_price").toString(), 0d);
        goodsIndex.setDiscountPrice(discountPrice);
        goodsIndex.setBuyCount(goods.get("buy_count") == null ? 0 : StringUtil.toInt(goods.get("buy_count").toString(), 0));
        goodsIndex.setSellerId(StringUtil.toLong(goods.get("seller_id").toString(), 0));
        //店铺分组
        goodsIndex.setShopCatId(goods.get("shop_cat_id") == null ? 0 : StringUtil.toLong(goods.get("shop_cat_id").toString(), 0));
        goodsIndex.setShopCatPath("");
        if (goodsIndex.getShopCatId() != 0) {
            ShopCatDO shopCat = shopCatClient.getModel(goodsIndex.getShopCatId());
            if (shopCat != null) {
                goodsIndex.setShopCatPath(HexUtils.encode(shopCat.getCatPath()));
            }
        }

        goodsIndex.setSellerName(goods.get("seller_name").toString());
        goodsIndex.setCommentNum(goods.get("comment_num") == null ? 0 : StringUtil.toInt(goods.get("comment_num").toString(), 0));
        goodsIndex.setGrade(goods.get("grade") == null ? 100 : StringUtil.toDouble(goods.get("grade").toString(), 100d));

        goodsIndex.setBrand(goods.get("brand_id") == null ? 0 : StringUtil.toLong(goods.get("brand_id").toString(), 0));
        goodsIndex.setCategoryId(goods.get("category_id") == null ? 0 : StringUtil.toLong(goods.get("category_id").toString(), 0));
        CategoryDO cat = categoryManager.getModel(Long.valueOf(goods.get("category_id").toString()));
        goodsIndex.setCategoryPath(HexUtils.encode(cat.getCategoryPath()));
        goodsIndex.setDisabled(StringUtil.toInt(goods.get("disabled").toString(), 0));
        goodsIndex.setMarketEnable(StringUtil.toInt(goods.get("market_enable").toString(), 0));
        goodsIndex.setIsAuth(StringUtil.toInt(goods.get("is_auth").toString(), 0));
        goodsIndex.setIntro(goods.get("intro") == null ? "" : goods.get("intro").toString());
        goodsIndex.setSelfOperated(goods.get("self_operated") == null ? 0 : StringUtil.toInt(goods.get("self_operated").toString(), 0));
        //添加商品优先级维度
        goodsIndex.setPriority(goods.get("priority") == null ? 1 : StringUtil.toInt(goods.get("priority").toString(), 1));
        //参数维度,已填写参数
        List<Map> params = (List<Map>) goods.get("params");
        List<Param> paramsList = this.convertParam(params);
        goodsIndex.setParams(paramsList);

        return goodsIndex;
    }

    /**
     * 获取分词结果
     *
     * @param txt
     * @return 分词list
     */
    protected List<String> toWordsList(String txt) {
        List<String> list = ElasticOperationUtil.analyzer(jestClient,txt);
        return list;
    }

    /**
     * 转换参数
     *
     * @param params
     * @return
     */
    protected List<Param> convertParam(List<Map> params) {
        List<Param> paramIndices = new ArrayList<>();
        if (params != null && params.size() > 0) {

            for (Map param : params) {
                Param index = new Param();
                index.setName(param.get("param_name") == null ? "" : param.get("param_name").toString());
                index.setValue(param.get("param_value") == null ? "" : param.get("param_value").toString());
                paramIndices.add(index);
            }

        }
        return paramIndices;
    }


    /**
     * 将分词结果写入数据库
     *
     * @param wordsList
     */
    protected void wordsToDb(List<String> wordsList) {
        wordsList = removeDuplicate(wordsList);
        for (String words : wordsList) {
            goodsWordsManager.addWords(words);
        }
    }
    /**
     * 初始化索引
     * @param list 索引数据
     * @param index 数量
     * @return  是否是生成成功
     */
    @Override
    public boolean addAll(List<Map<String, Object>> list, int index) {
        //配置文件中定义的索引名字
        String indexName = elasticJestConfig.getIndexName() + "_" + EsSettings.GOODS_INDEX_NAME;
        //删除所有的索引
        if (index == 1) {
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            searchSourceBuilder.query(QueryBuilders.matchAllQuery());
            ElasticOperationUtil.deleteByQuery(jestClient,indexName,searchSourceBuilder.toString());
        }
        boolean hasError = false;
        //循环生成索引
        for (Map goods : list) {
            //如果任务停止则停止生成索引
            TaskProgress tk = progressManager.getProgress(TaskProgressConstant.GOODS_INDEX);
            if (tk != null) {

                try {
                    /** 生成索引消息 */
                    progressManager.taskUpdate(TaskProgressConstant.GOODS_INDEX, "正在生成[" + StringUtil.toString(goods.get("goods_name")) + "]");
                    /** 生成优惠价格索引 */
                    goods.put("discount_price", 0L);
                    this.addIndex(goods);
                } catch (Exception e) {
                    hasError = true;
                    logger.error(StringUtil.toString(goods.get("goods_name")) + "索引生成异常", e);
                }


            } else {
                return true;
            }
        }


        return hasError;
    }

    /**
     * list去重
     * @param list
     * @return
     */
    protected List<String> removeDuplicate(List<String> list){
        List<String> listTemp = new ArrayList();
        for (String words:list) {
            if(!listTemp.contains(words)){
                listTemp.add(words);
            }
        }
        return listTemp;
    }


}
