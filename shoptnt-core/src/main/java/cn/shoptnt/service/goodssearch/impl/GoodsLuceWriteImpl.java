package cn.shoptnt.service.goodssearch.impl;

import cn.shoptnt.client.member.ShopCatClient;
import cn.shoptnt.framework.lock.Lock;
import cn.shoptnt.framework.lock.LockFactory;
import cn.shoptnt.framework.util.HexUtils;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.model.goods.dos.CategoryDO;
import cn.shoptnt.model.goodssearch.GoodsIndex;
import cn.shoptnt.model.shop.dos.ShopCatDO;
import cn.shoptnt.model.system.vo.TaskProgress;
import cn.shoptnt.model.system.vo.TaskProgressConstant;
import cn.shoptnt.model.util.progress.ProgressManager;
import cn.shoptnt.service.goods.CategoryManager;
import cn.shoptnt.service.goods.GoodsQueryManager;
import cn.shoptnt.service.goodssearch.GoodsIndexManager;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Lucene 索引生成实现
 *
 * @author LiuXT
 * @version stand-alone
 * @data 2022/10/25
 **/
@Service
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class GoodsLuceWriteImpl implements GoodsIndexManager {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    protected ShopCatClient shopCatClient;
    @Autowired
    protected ProgressManager progressManager;
    @Autowired
    private LockFactory lockFactory;
    @Autowired
    private CategoryManager categoryManager;

    @Autowired
    private GoodsQueryManager goodsQueryManager;


    @Override
    public boolean addIndex(Map goods) {
        logger.debug("正常生成商品索引====================================");
        return this.add(goods);
    }

    private boolean add(Map goods) {
        // 将goods转化成goodsIndex
        GoodsIndex goodsIndex = this.getSource(goods);
        // 创建Doc文档
        Document document = goodsIndexToDoc(goodsIndex);
        String lockName = "goods_index_lock";
        Lock lock = lockFactory.getLock(lockName);
        lock.lock();
        IndexWriter indexWriter = null;
        boolean result = false;
        // 获取indexWriter
        try {
            indexWriter = getIndexWriter();
            BooleanQuery.Builder booleanBuilder = new BooleanQuery.Builder();
            TermQuery goodsIdTerm = new TermQuery(new Term("goodsId", goodsIndex.getGoodsId().toString()));
            booleanBuilder.add(goodsIdTerm, BooleanClause.Occur.MUST);
            indexWriter.deleteDocuments(booleanBuilder.build());
            // 将Doc文档添加进indexWriter中
            indexWriter.addDocument(document);
            result = true;
        } catch (Exception e) {
            logger.error("为商品" + goods.get("goods_name") + "生成索引异常", e);
            result = false;
        } finally {
            try {
                indexWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            lock.unlock();
        }
        return result;
    }

    private IndexWriter getIndexWriter() {
        return LuceneFactory.getIndexWriter();
    }

    @Override
    public void updateIndex(Map goods) throws IOException {
        // 获取indexWriter
        this.deleteIndex(goods);

        this.addIndex(goods);
    }

    @Override
    public void deleteIndex(Map goods) throws IOException {
        // 获取indexWriter
        IndexWriter indexWriter = getIndexWriter();
        try {
            // 构建条件，根据goodsId删除
            Term term = new Term("goodsId", goods.get("goods_id").toString());
            indexWriter.deleteDocuments(term);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            indexWriter.close();
        }
    }

    @Override
    public boolean addAll(List<Map<String, Object>> list, int index) throws IOException {
        //删除所有的索引
        if (index == 1) {
            deleteAllIndex();
        }
        boolean result = true;
        for (Map<String, Object> goods : list) {
            TaskProgress tk = progressManager.getProgress(TaskProgressConstant.GOODS_INDEX);

            if (tk != null) {
                /** 生成索引消息 */
                progressManager.taskUpdate(TaskProgressConstant.GOODS_INDEX, "正在生成[" + StringUtil.toString(goods.get("goods_name")) + "]");
                logger.debug("开始生成商品【" + goods.get("goods_name") + "】索引");
                goods.put("discount_price", 0L);

                result = result && addIndex(goods);
                logger.debug("商品【" + goods.get("goods_name"));

            }
        }
        return result;
    }

    private void deleteAllIndex() {

        IndexWriter indexWriter = null;
        // 获取indexWriter
        try {
            indexWriter = getIndexWriter();
            indexWriter.deleteAll();
        } catch (Exception e) {
            logger.error("删除索引异常", e);
        } finally {
            try {
                indexWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 将goodsIndex转化为doc
     *
     * @param goodsIndex
     * @return
     */
    public Document goodsIndexToDoc(GoodsIndex goodsIndex) {
        Document document = new Document();
        document.add(new StringField("goodsId", goodsIndex.getGoodsId().toString(), Field.Store.YES));
        document.add(new TextField("name", goodsIndex.getName(), Field.Store.YES));
        document.add(new StringField("thumbnail", goodsIndex.getThumbnail(), Field.Store.YES));
        document.add(new StringField("small", goodsIndex.getSmall(), Field.Store.YES));
        document.add(new StringField("buyCount", goodsIndex.getBuyCount().toString(), Field.Store.YES));
        document.add(new NumericDocValuesField("buyCountOrder", goodsIndex.getBuyCount()));
        document.add(new StringField("sellerId", goodsIndex.getSellerId().toString(), Field.Store.YES));
        document.add(new StringField("sellerName", goodsIndex.getSellerName(), Field.Store.YES));
        document.add(new StringField("shopCatId", goodsIndex.getShopCatId().toString(), Field.Store.YES));
        document.add(new StringField("shopCatPath", goodsIndex.getShopCatPath(), Field.Store.YES));
        document.add(new StringField("commentNum", goodsIndex.getCommentNum().toString(), Field.Store.YES));
        document.add(new NumericDocValuesField("commentNumOrder", goodsIndex.getCommentNum()));
        document.add(new StringField("grade", goodsIndex.getGrade().toString(), Field.Store.YES));
        document.add(new StringField("discountPrice", BigDecimal.valueOf(goodsIndex.getDiscountPrice()).toString(), Field.Store.YES));
        document.add(new DoubleDocValuesField("discountPriceOrder", goodsIndex.getDiscountPrice()));
        document.add(new DoublePoint("priceRange", BigDecimal.valueOf(goodsIndex.getPrice()).doubleValue()));
        document.add(new StringField("price", BigDecimal.valueOf(goodsIndex.getPrice()).toString(), Field.Store.YES));
        document.add(new DoubleDocValuesField("priceOrder", goodsIndex.getPrice()));
        document.add(new StringField("brand", goodsIndex.getBrand().toString(), Field.Store.YES));
        document.add(new StringField("categoryId", goodsIndex.getCategoryId().toString(), Field.Store.YES));
        document.add(new StringField("categoryPath", goodsIndex.getCategoryPath(), Field.Store.YES));
        document.add(new StringField("disabled", goodsIndex.getDisabled().toString(), Field.Store.YES));
        document.add(new StringField("marketEnable", goodsIndex.getMarketEnable().toString(), Field.Store.YES));
        document.add(new StringField("isAuth", goodsIndex.getIsAuth().toString(), Field.Store.YES));
        document.add(new StringField("intro", goodsIndex.getIntro(), Field.Store.NO));
        document.add(new StringField("priority", goodsIndex.getPriority().toString(), Field.Store.YES));
        document.add(new NumericDocValuesField("priorityOrder", goodsIndex.getPriority()));
        return document;
    }

    /**
     * 封装成内存需要格式数据
     *
     * @param goods
     * @return
     */
    protected GoodsIndex getSource(Map goods) {
        GoodsIndex goodsIndex = new GoodsIndex();
        goodsIndex.setGoodsId(StringUtil.toLong(goods.get("goods_id").toString(), 0));
        goodsIndex.setName(goods.get("goods_name").toString());
        goodsIndex.setThumbnail(goods.get("thumbnail") == null ? "" : goods.get("thumbnail").toString());
        goodsIndex.setSmall(goods.get("small") == null ? "" : goods.get("small").toString());
        Double p = goods.get("price") == null ? 0d : StringUtil.toDouble(goods.get("price").toString(), 0d);
        goodsIndex.setPrice(p);
        Double discountPrice = goods.get("discount_price") == null ? 0d : StringUtil.toDouble(goods.get("discount_price").toString(), 0d);
        goodsIndex.setDiscountPrice(discountPrice);
        goodsIndex.setBuyCount(goods.get("buy_count") == null ? 0 : StringUtil.toInt(goods.get("buy_count").toString(), 0));
        goodsIndex.setSellerId(StringUtil.toLong(goods.get("seller_id").toString(), 0));
        //店铺分组
        goodsIndex.setShopCatId(goods.get("shop_cat_id") == null ? 0 : StringUtil.toLong(goods.get("shop_cat_id").toString(), 0));
        goodsIndex.setShopCatPath("");
        if (goodsIndex.getShopCatId() != 0) {
            ShopCatDO shopCat = shopCatClient.getModel(goodsIndex.getShopCatId());
            if (shopCat != null) {
                goodsIndex.setShopCatPath(HexUtils.encode(shopCat.getCatPath()));
            }
        }
        goodsIndex.setSellerName(goods.get("seller_name").toString());
        goodsIndex.setCommentNum(goods.get("comment_num") == null ? 0 : StringUtil.toInt(goods.get("comment_num").toString(), 0));
        goodsIndex.setGrade(goods.get("grade") == null ? 100 : StringUtil.toDouble(goods.get("grade").toString(), 100d));
        goodsIndex.setBrand(goods.get("brand_id") == null ? 0 : StringUtil.toLong(goods.get("brand_id").toString(), 0));
        goodsIndex.setCategoryId(goods.get("category_id") == null ? 0 : StringUtil.toLong(goods.get("category_id").toString(), 0));
        CategoryDO cat = categoryManager.getModel(Long.valueOf(goods.get("category_id").toString()));
        goodsIndex.setCategoryPath(HexUtils.encode(cat.getCategoryPath()));
        goodsIndex.setDisabled(StringUtil.toInt(goods.get("disabled").toString(), 0));
        goodsIndex.setMarketEnable(StringUtil.toInt(goods.get("market_enable").toString(), 0));
        goodsIndex.setIsAuth(StringUtil.toInt(goods.get("is_auth").toString(), 0));
        goodsIndex.setIntro(goods.get("intro") == null ? "" : goods.get("intro").toString());
        goodsIndex.setSelfOperated(goods.get("self_operated") == null ? 0 : StringUtil.toInt(goods.get("self_operated").toString(), 0));
        // 添加商品优先级维度
        goodsIndex.setPriority(goods.get("priority") == null ? 1 : StringUtil.toInt(goods.get("priority").toString(), 1));
        return goodsIndex;
    }
}
