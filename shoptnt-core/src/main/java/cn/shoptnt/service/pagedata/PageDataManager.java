/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.pagedata;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.pagedata.PageData;
import cn.shoptnt.model.pagedata.PageQueryParam;


/**
 * 楼层业务层
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-21 16:39:22
 */
public interface PageDataManager {


    /**
     * 查询楼层列表
     * @param param 页面查询参数
     * @return WebPage
     */
    WebPage getPageList(PageQueryParam param);

    /**
     * 添加楼层
     *
     * @param page 楼层
     * @return PageData 楼层
     */
    PageData add(PageData page);

    /**
     * 修改楼层
     *
     * @param page 楼层
     * @param id   楼层主键
     * @return PageData 楼层
     */
    PageData edit(PageData page, Long id);

    /**
     * 设置首页
     *
     * @param sellerId 商家id
     * @param id       楼层主键
     * @return PageData 楼层
     */
    PageData editIndex(Long sellerId, Long id);

    /**
     * 查询首页
     *
     * @param sellerId   商家id
     * @param clientType 客户端类型
     * @return PageData 楼层
     */
    PageData getIndex(Long sellerId, String clientType);

    /**
     * 删除楼层
     *
     * @param id 楼层主键
     */
    void delete(Long id);

    /**
     * 删除楼层
     *
     * @param ids 楼层主键集合
     */
    void delete(Long[] ids);

    /**
     * 获取楼层
     *
     * @param id 楼层主键
     * @return PageData  楼层
     */
    PageData getModel(Long id);

    /**
     * 修改发布状态
     *
     * @param id 楼层主键
     * @return PageData  楼层
     */
    PageData updatePublish(Long id);



}
