/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.goods.util;

/**
 * 搜索分隔符
 * @author fk
 * @version v1.0
 * 2017年4月19日 下午4:35:57
 */
public class Separator {
	
	public static final String SEPARATOR_PROP = "@";
	
	public static final String SEPARATOR_PROP_VLAUE = "_";
	
	public static final String SEPARATOR_PROP_COMMA = ",";
}
