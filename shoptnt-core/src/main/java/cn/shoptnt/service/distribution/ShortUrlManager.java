/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.distribution;

import cn.shoptnt.model.distribution.dos.ShortUrlDO;

/**
 * 短链接Manager接口
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018/5/23 上午8:37
 * @Description:
 *
 */
public interface ShortUrlManager {

	/**
	 * 生成一个短链接
	 * @param memberId 会员id
	 * @param goodsId 商品id
	 * @return 短链接
	 */
    ShortUrlDO createShortUrl(Long memberId, Long goodsId);

	/**
	 * 根据短链接获得长链接
	 * @param shortUrl 短链接 （可带前缀 即：http:xxx/）
	 * @return 所对应的长链接
	 */
    ShortUrlDO getLongUrl(String shortUrl);

}
