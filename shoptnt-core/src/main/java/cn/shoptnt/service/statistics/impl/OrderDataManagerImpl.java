/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics.impl;

import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.client.trade.OrderClient;
import cn.shoptnt.mapper.statistics.OrderDataMapper;
import cn.shoptnt.mapper.statistics.OrderGoodsDataMapper;
import cn.shoptnt.model.goods.dos.CategoryDO;
import cn.shoptnt.model.statistics.dto.OrderData;
import cn.shoptnt.model.statistics.dto.OrderGoodsData;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import cn.shoptnt.model.trade.order.enums.PayStatusEnum;
import cn.shoptnt.service.statistics.OrderDataManager;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.dos.OrderItemsDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 订单实现
 *
 * @author chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/6/22 下午10:11
 */
@Service
public class OrderDataManagerImpl implements OrderDataManager {

    @Autowired
    private OrderClient orderClient;

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private OrderGoodsDataMapper orderGoodsDataMapper;

    @Autowired
    private OrderDataMapper orderDataMapper;

    /**
     * 订单新增
     *
     * @param order 订单数据
     */
    @Override
    public void put(OrderDO order) {
        List<OrderItemsDO> itemsDOList = orderClient.orderItems(order.getSn());
        int goodsNum = 0;
        for (OrderItemsDO oi : itemsDOList) {
            OrderGoodsData orderGoodsData = new OrderGoodsData(oi, order);
            CategoryDO categoryDO = goodsClient.getCategory(oi.getCatId());
            orderGoodsData.setIndustryId(getIndustry(categoryDO.getCategoryPath()));
            orderGoodsData.setCategoryPath(categoryDO.getCategoryPath());
            orderGoodsDataMapper.insert(orderGoodsData);
            goodsNum = goodsNum + oi.getNum();
        }
        order.setGoodsNum(goodsNum);
        if(OrderStatusEnum.PAID_OFF.name().equals(order.getOrderStatus())){
            order.setPayStatus(PayStatusEnum.PAY_YES.name());
        }
        orderDataMapper.insert(new OrderData(order));
    }

    /**
     * 订单修改
     *
     * @param order 订单数据
     */
    @Override
    public void change(OrderDO order) {

        //根据订单编号查询订单
        OrderData od = new QueryChainWrapper<>(orderDataMapper)
                .eq("sn", order.getSn())
                .one();

        if (od != null) {
            od.setOrderStatus(order.getOrderStatus());
            od.setPayStatus(order.getPayStatus());
            //根据订单编号修改订单
            new UpdateChainWrapper<>(orderDataMapper)
                .eq("sn", order.getSn())
                .update(od);
        } 

    }


    /**
     * 获取第二级别分类。
     *
     * @param path
     * @return
     */
    private Long getIndustry(String path) {
        try {
            String pattern = "(0\\|)(\\d+)";
            // 创建 Pattern 对象
            Pattern r = Pattern.compile(pattern);
            // 现在创建 matcher 对象
            Matcher m = r.matcher(path);
            if (m.find()) {
                return new Long(m.group(2));
            }
            return 0L;
        } catch (Exception e) {
            e.printStackTrace();
            return 0L;
        }
    }
}
