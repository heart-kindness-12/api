/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.payment.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.client.trade.OrderClient;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.logs.Debugger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.sncreator.SnCreator;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.framework.util.PageConvert;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.mapper.payment.PaymentBillMapper;
import cn.shoptnt.model.base.SubCode;
import cn.shoptnt.model.base.message.PaymentBillPaymentMethodChangeMsg;
import cn.shoptnt.model.base.message.PaymentBillStatusChangeMsg;
import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.model.errorcode.PaymentErrorCode;
import cn.shoptnt.model.payment.dos.PaymentBillDO;
import cn.shoptnt.model.payment.enums.PaymentBillChangeTypeEnum;
import cn.shoptnt.model.payment.vo.PayBill;
import cn.shoptnt.model.trade.order.dto.OrderDetailDTO;
import cn.shoptnt.model.trade.order.enums.TradeTypeEnum;
import cn.shoptnt.service.payment.PaymentBillManager;
import cn.shoptnt.framework.message.direct.DirectMessageSender;
import cn.shoptnt.service.payment.PaymentPluginManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 支付帐单业务类
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-04-16 17:28:07
 */
@Service
public class PaymentBillManagerImpl implements PaymentBillManager {


    @Autowired
    private Debugger debugger;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DirectMessageSender messageSender;

    @Autowired
    SnCreator snCreator;

    @Autowired
    private PaymentBillMapper payBillMapper;

    @Autowired
    private List<PaymentPluginManager> paymentPluginList;

    @Autowired
    private OrderClient orderClient;

    @Override
    public WebPage list(long page, long pageSize) {

        Page<PaymentBillDO> ipage = new QueryChainWrapper<>(payBillMapper).page(new Page<>(page, pageSize));

        return PageConvert.convert(ipage);
    }

    @Override
    @Transactional(value = "tradeTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public PayBill add(PaymentBillDO paymentBill) {
        //根据子业务单号和子业务类型获取账单
        PaymentBillDO paymentBillDO = this.getBySubSnAndServiceType(paymentBill.getSubSn(), paymentBill.getServiceType());
        //判断账单是否存在
        if (paymentBillDO == null) {
            //如果不存在，则新增
            paymentBill.setBillSn("" + snCreator.create(SubCode.PAY_BILL));
            this.payBillMapper.insert(paymentBill);
        } else if (!paymentBillDO.getTradePrice().equals(paymentBill.getTradePrice()) || "微信".equals(paymentBillDO.getPaymentName())) {
            //生成新的账单号,微信的不同的客户端要求订单号不能一样，所以这里直接将微信支付方式的更换账单号
            paymentBill.setBillSn("" + snCreator.create(SubCode.PAY_BILL));
            this.edit(paymentBill, paymentBillDO.getBillSn());
        } else {
            //如果存在则修改
            paymentBill.setBillSn(paymentBillDO.getBillSn());
            this.edit(paymentBill, null);
        }
        //校验是否存在需要关闭的交易
        this.closeThreadOrder(paymentBillDO, paymentBill);
        PayBill payBill = new PayBill(paymentBill);
        return payBill;
    }

    private PaymentBillDO getByBillSn(String billSn) {

        return new QueryChainWrapper<>(payBillMapper)
                //按支付账单编号查询
                .eq("bill_sn", billSn)
                //查询单个对象
                .one();
    }

    @Override
    public PaymentBillDO getBillByReturnTradeNo(String returnTradeNo) {

        return new QueryChainWrapper<>(payBillMapper)
                //按第三方平台返回交易号查询
                .eq("return_trade_no", returnTradeNo)
                //查询单个对象
                .one();
    }

    @Override
    public PaymentBillDO getModel(String billSn) {

        return new QueryChainWrapper<>(payBillMapper)
                //按支付账单编号查询
                .eq("bill_sn", billSn)
                //查询单个对象
                .one();
    }

    @Override
    public void edit(PaymentBillDO paymentBillDO, String sn) {
        String billSn;
        if (StringUtil.isEmpty(sn)) {
            billSn = paymentBillDO.getBillSn();
        } else {
            billSn = sn;
        }

        new UpdateChainWrapper<>(payBillMapper)
                //按支付账单编号修改
                .eq("bill_sn", billSn)
                //提交修改
                .update(paymentBillDO);
    }


    /**
     * 根据子业务编号和子业务类型获取账单信息
     *
     * @param subSn
     * @param serviceType
     * @return
     */
    @Override
    public PaymentBillDO getBySubSnAndServiceType(String subSn, String serviceType) {

        PaymentBillDO one = new QueryChainWrapper<>(payBillMapper)
                //拼接交易单号查询条件
                .eq("sub_sn", subSn)
                //拼接业务类型查询条件
                .eq("service_type", serviceType)
                //查询单个对象
                .one();

        return one;
    }

    @Override
    public boolean check(String billSn) {
        PaymentBillDO paymentBillDO = this.getModel(billSn);
        return paymentBillDO.getIsPay() == 0;
    }

    @Override
    @Transactional(value = "tradeTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void paySuccess(String billSn, String returnTradeNo, Double payPrice) {


        //根据支付单号找到交易单号
        PaymentBillDO bill = this.getByBillSn(billSn);
        if (bill == null) {
            debugger.log("支付回调失败，原因为：编号为：" + billSn + "没有找到相应的账单】");
            logger.error("支付回调失败，原因为：编号为：" + billSn + "没有找到相应的账单】");
            throw new RuntimeException("支付回调失败，编号为：" + billSn + "没有找到相应的账单】");
        }

        logger.debug("找到账单：");
        debugger.log("找到账单：");
        logger.debug(bill.toString());
        debugger.log(bill.toString());
        //验证回调金额和支付金额是否一致
        if (!bill.getTradePrice().equals(payPrice)) {
            logger.error("支付金额不一致，账单支付金额：" + bill.getTradePrice() + "，回调金额：" + payPrice);
            debugger.log("支付金额不一致，账单支付金额：" + bill.getTradePrice() + "，回调金额：" + payPrice);
            throw new ServiceException(PaymentErrorCode.E508.code(), "支付金额不一致");
        }

        //修改支付账单的状态
        new UpdateChainWrapper<>(payBillMapper)
                //设置是否支付
                .set("is_pay", 1)
                //设置第三方平台返回交易号
                .set("return_trade_no", returnTradeNo)
                //拼接支付账单id修改条件
                .eq("bill_id", bill.getBillId())
                //提交修改
                .update();

        //发送账单状态变化消息
        bill.setReturnTradeNo(returnTradeNo);
        PaymentBillStatusChangeMsg msg = new PaymentBillStatusChangeMsg(bill, payPrice);
        msg.setStatus(PaymentBillStatusChangeMsg.SUCCESS);
        this.messageSender.send(msg);

        logger.debug("更改支付账单状态成功");
        debugger.log("更改支付账单状态成功");
    }

    /**
     * 取消第三方支付账单
     *
     * @param paymentBillDO 原账单
     * @param paymentBill   新账单
     */
    private void closeThreadOrder(PaymentBillDO paymentBillDO, PaymentBillDO paymentBill) {
        PaymentBillPaymentMethodChangeMsg paymentBillPaymentMethodChangeMsg = new PaymentBillPaymentMethodChangeMsg();
        Map<String, Object> map = new HashMap<>();
        //原账单和新账单支付方式不一样 原账单和新账单支付方式一样 只有原支付账单不为空的情况才会出现
        if (paymentBillDO != null && paymentBillDO.getPaymentPluginId() != null) {
            //如果原账单和新账单支付方式不一样 需要取消原账单第三方支付订单
            if (!paymentBillDO.getPaymentPluginId().equals(paymentBill.getPaymentPluginId())) {
                map.put(PaymentBillChangeTypeEnum.PAYMENT_METHOD.name(), paymentBillDO);
            }
            //如果原账单和新账单支付方式一样，但是金额不一样 也需要取消原账单第三方支付订单
            if (paymentBillDO.getPaymentPluginId().equals(paymentBill.getPaymentPluginId()) && !paymentBill.getTradePrice().equals(paymentBillDO.getTradePrice())) {
                map.put(PaymentBillChangeTypeEnum.PRICE.name(), paymentBillDO);
            }
        }

        //如果原账单交易类型和新账单的交易类型不一样，需要取消原账单的第三方支付订单
        if (paymentBill.getServiceType().equals(TradeTypeEnum.ORDER.name())) {
            OrderDetailDTO orderDetailDTO = orderClient.getModel(paymentBill.getSubSn());
            PaymentBillDO bill = this.getBySubSnAndServiceType(orderDetailDTO.getTradeSn(), TradeTypeEnum.TRADE.name());
            if (bill != null && !StringUtil.isEmpty(bill.getPaymentPluginId())) {
                map.put(PaymentBillChangeTypeEnum.SERVICE_TYPE.name(), bill);
            }
        }
        //如果更换类型不为空则发送消息
        if (!map.isEmpty()) {
            //因为会存在改价格的情况 这样要把之前创建的账单全部取消并且删除无用账单，可能回存在多个
            paymentBillPaymentMethodChangeMsg.setBills(map);
            this.messageSender.send(paymentBillPaymentMethodChangeMsg);
        }

    }

    @Override
    public void closeTrade(PaymentBillDO paymentBillDO) {
        //查找插件
        PaymentPluginManager plugin = findPaymentPlugin(paymentBillDO.getPaymentPluginId());

        //支付参数
        Map map = JsonUtil.jsonToObject(paymentBillDO.getPayConfig(), Map.class);

        List<Map> list = (List<Map>) map.get("config_list");
        Map<String, String> result = new HashMap<>(list.size());
        if (list != null) {
            for (Map item : list) {
                result.put(item.get("name").toString(), item.get("value").toString());
            }
        }
        //取消账单
        plugin.closeTrade(result, paymentBillDO.getBillSn());
    }

    /**
     * 查找支付插件
     *
     * @param pluginId
     * @return
     */
    private PaymentPluginManager findPaymentPlugin(String pluginId) {
        for (PaymentPluginManager plugin : paymentPluginList) {
            if (plugin.getPluginId().equals(pluginId)) {
                return plugin;
            }
        }
        return null;
    }

    @Override
    public void delete(Long billId) {
        this.payBillMapper.delete(new QueryWrapper<PaymentBillDO>().eq("bill_id", billId));
    }

    @Override
    public void sendCloseTradeMessage(Map<String, Object> map) {
        //构建返回参数
        List<PaymentBillDO> list = new ArrayList<>();
        for (String key : map.keySet()) {
            PaymentBillDO paymentBillDO = this.getBySubSnAndServiceType(key, StringUtil.toString(map.get(key)));
            if (paymentBillDO != null) {
                list.add(paymentBillDO);
            }
        }
        //构建发送取消账单的数据
        if (list.size() > 0) {
            Map<String, Object> billMap = new HashMap<>();
            for (PaymentBillDO paymentBillDO : list) {
                billMap.put(PaymentBillChangeTypeEnum.SERVICE_TYPE.name(), paymentBillDO);
            }
            //构建数据结构发送取消账单消息
            PaymentBillPaymentMethodChangeMsg paymentBillPaymentMethodChangeMsg = new PaymentBillPaymentMethodChangeMsg();
            paymentBillPaymentMethodChangeMsg.setBills(billMap);
            this.messageSender.send(paymentBillPaymentMethodChangeMsg);
        }
    }
}
