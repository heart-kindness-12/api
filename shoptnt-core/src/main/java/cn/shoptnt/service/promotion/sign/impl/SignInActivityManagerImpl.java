package cn.shoptnt.service.promotion.sign.impl;

import cn.shoptnt.client.member.MemberClient;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.NoPermissionException;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.lock.Lock;
import cn.shoptnt.framework.lock.LockFactory;
import cn.shoptnt.framework.message.direct.DirectMessageSender;
import cn.shoptnt.framework.util.BeanUtil;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.PageConvert;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.mapper.promotion.sigin.SignInActivityMapper;
import cn.shoptnt.model.base.message.GoodsChangeMsg;
import cn.shoptnt.model.base.message.SignMessage;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.errorcode.PromotionErrorCode;
import cn.shoptnt.model.goods.dos.GoodsDO;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dos.MemberCoupon;
import cn.shoptnt.model.member.dos.MemberPointHistory;
import cn.shoptnt.model.promotion.coupon.dos.CouponDO;
import cn.shoptnt.model.promotion.coupon.enums.CouponType;
import cn.shoptnt.model.promotion.sign.dos.SignInActivity;
import cn.shoptnt.model.promotion.sign.dos.SignInRecord;
import cn.shoptnt.model.promotion.sign.dos.SignInReward;
import cn.shoptnt.model.promotion.sign.dto.*;
import cn.shoptnt.model.promotion.sign.enums.SignInActivityStatus;
import cn.shoptnt.model.promotion.sign.enums.SignInPrizeType;
import cn.shoptnt.model.promotion.sign.vos.SignBuyerInfoVO;
import cn.shoptnt.model.promotion.sign.vos.SignInActivityInfo;
import cn.shoptnt.model.util.SwitchEnum;
import cn.shoptnt.service.goods.GoodsQueryManager;
import cn.shoptnt.service.member.MemberCouponManager;
import cn.shoptnt.service.promotion.coupon.CouponManager;
import cn.shoptnt.service.promotion.sign.SignInActivityManager;
import cn.shoptnt.service.promotion.sign.SignInRecordManager;
import cn.shoptnt.service.promotion.sign.SignInRewardManager;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zh
 * @version 1.0
 * @title SignInActivityManagerImpl
 * @description 签到活动实现类
 * @program: api
 * 2024/3/12 13:43
 */
@Service
public class SignInActivityManagerImpl extends ServiceImpl<SignInActivityMapper, SignInActivity> implements SignInActivityManager {


    @Autowired
    private SignInRewardManager signInRewardManager;

    @Autowired
    private SignInRecordManager signInRecordManager;

    @Autowired
    private CouponManager couponManager;

    @Autowired
    private MemberCouponManager memberCouponManager;

    @Autowired
    private GoodsQueryManager goodsQueryManager;

    @Autowired
    private LockFactory lockFactory;

    @Autowired
    private MemberClient memberClient;

    @Autowired
    private DirectMessageSender messageSender;


    @Override
    public WebPage list(long page, long pageSize, SignInActivitySearch signInActivitySearch) {

        //查询 已经开始的活动 且 已经过期的活动 ，如果有 需要将状态修改为已过期
        List<SignInActivity> list = lambdaQuery()
                .lt(SignInActivity::getEndDate, DateUtil.getDateline())
                .eq(SignInActivity::getStatus, SignInActivityStatus.OPEN.name())
                .list();
        if (!list.isEmpty()) {
            list = list.stream().map(activity -> {
                activity.setStatus(SignInActivityStatus.FINISH.value());
                return activity;
            }).collect(Collectors.toList());
            this.updateBatchById(list);
        }
        //执行查询
        IPage<SignInActivity> iPage = lambdaQuery().ge(signInActivitySearch.getStartTime() != null && signInActivitySearch.getEndTime() != null, SignInActivity::getEndDate, signInActivitySearch.getStartTime())
                .le(signInActivitySearch.getStartTime() != null && signInActivitySearch.getEndTime() != null, SignInActivity::getStartDate, signInActivitySearch.getEndTime())
                .eq(!StringUtil.isEmpty(signInActivitySearch.getTitle()), SignInActivity::getTitle, signInActivitySearch.getTitle())
                .eq(!StringUtil.isEmpty(signInActivitySearch.getStatus()), SignInActivity::getStatus, signInActivitySearch.getStatus())
                .page(new Page<>(page, pageSize));

        return PageConvert.convert(iPage);


    }

    @Override
    public void close(Long activityId) {
        SignInActivity activity = this.getById(activityId);
        if (activity != null) {
            activity.setStatus(SignInActivityStatus.CLOSE.value());
            this.updateById(activity);
        }
    }


    @Override
    public SignInActivityInfo getInfo(Long activityId) {
        //获取活动信息
        SignInActivity activity = this.getById(activityId);
        if (activity == null) {
            return null;
        }
        List<SignInReward> list = signInRewardManager.getByActivityId(activityId);
        SignInActivityInfo sign = new SignInActivityInfo();
        BeanUtil.copyProperties(activity, sign);
        if (!list.isEmpty()) {
            sign.setSignInRewards(list);
        }
        return sign;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {RuntimeException.class, Exception.class, ServiceException.class, NoPermissionException.class})
    public SignInActivityInfo add(SignInActivityOperation signInActivityOperation) {
        //校验时间是否准确
        if (signInActivityOperation.getStartDate() > signInActivityOperation.getEndDate() || DateUtil.getDateline() >= signInActivityOperation.getStartDate()) {
            throw new ServiceException(PromotionErrorCode.E400.code(), "时间选择错误");
        }
        //校验是否存在重叠的活动时间
        if (checkoutDate(signInActivityOperation.getStartDate(), signInActivityOperation.getEndDate())) {
            throw new ServiceException(PromotionErrorCode.E400.code(), "时间段内存在相同签到活动");
        }
        SignInActivity sign = new SignInActivity();
        sign.setCreateTime(DateUtil.getDateline());
        sign.setTitle(signInActivityOperation.getTitle());
        sign.setStartDate(signInActivityOperation.getStartDate());
        sign.setEndDate(signInActivityOperation.getEndDate());
        sign.setStatus(SignInActivityStatus.WAIT.name());
        sign.setDescription(signInActivityOperation.getDescription());
        if (signInActivityOperation.getProducts() != null && !signInActivityOperation.getProducts().isEmpty()) {
            String result = signInActivityOperation.getProducts().stream()  // 将列表转换为流
                    .map(String::valueOf)  // 将每个整数转换为字符串
                    .collect(Collectors.joining(","));
            sign.setRecommendedProductId(result);
        }
        this.save(sign);

        List<SignInReward> rewards = signInActivityOperation.getRewards();
        if (!rewards.isEmpty()) {
            List<SignInReward> rewardList = new ArrayList<>();
            for (SignInReward reward : rewards) {
                SignInReward newSignInReward = new SignInReward();
                //如果赠送优惠券 则校验优惠券合法性
                if (reward.getCouponSwitch().equals(SwitchEnum.OPEN.name())) {
                    CouponDO couponDO = couponManager.getModel(reward.getCouponId());
                    if (couponDO == null || !couponDO.getSellerId().equals(0L) || !couponDO.getType().equals(CouponType.ACTIVITY_GIVE.name())) {
                        throw new ServiceException(PromotionErrorCode.E400.code(), "优惠券不合法");
                    }
                }
                BeanUtil.copyProperties(reward, newSignInReward);
                newSignInReward.setActivityId(sign.getId());
                rewardList.add(newSignInReward);
            }
            signInRewardManager.saveBatch(rewardList);
        }
        return getInfo(sign.getId());
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {RuntimeException.class, Exception.class, ServiceException.class, NoPermissionException.class})
    public SignInActivityInfo edit(Long id, SignInActivityOperation signInActivityOperation) {
        //获取活动信息
        SignInActivity activity = this.getById(id);
        if (activity == null) {
            throw new ServiceException(PromotionErrorCode.E400.code(), "签到活动不存在");
        }
        //校验时间是否准确
        if (signInActivityOperation.getStartDate() > signInActivityOperation.getEndDate() || DateUtil.getDateline() >= signInActivityOperation.getStartDate()) {
            throw new ServiceException(PromotionErrorCode.E400.code(), "时间选择错误");
        }
        //校验是否存在重叠的活动时间
        if (checkoutDate(signInActivityOperation.getStartDate(), signInActivityOperation.getEndDate())) {
            throw new ServiceException(PromotionErrorCode.E400.code(), "时间段内存在相同签到活动");
        }
        if (!activity.getStatus().equals(SignInActivityStatus.WAIT.name())) {
            throw new ServiceException(PromotionErrorCode.E400.code(), "只有未开始的签到活动支持修改");
        }
        activity.setTitle(signInActivityOperation.getTitle());
        activity.setStartDate(signInActivityOperation.getStartDate());
        activity.setEndDate(signInActivityOperation.getEndDate());
        activity.setDescription(signInActivityOperation.getDescription());
        if (!signInActivityOperation.getProducts().isEmpty()) {
            String result = signInActivityOperation.getProducts().stream()  // 将列表转换为流
                    .map(String::valueOf)  // 将每个整数转换为字符串
                    .collect(Collectors.joining(","));
            activity.setRecommendedProductId(result);
        }
        this.updateById(activity);
        //删除签到设置重新添加
        signInRewardManager.deleteByActivityId(id);
        //重新设置
        List<SignInReward> rewards = signInActivityOperation.getRewards();
        if (!rewards.isEmpty()) {
            List<SignInReward> rewardList = new ArrayList<>();
            for (SignInReward reward : rewards) {
                SignInReward newSignInReward = new SignInReward();
                //如果赠送优惠券 则校验优惠券合法性
                if (reward.getCouponSwitch().equals(SwitchEnum.OPEN.name())) {
                    CouponDO couponDO = couponManager.getModel(reward.getCouponId());
                    if (couponDO == null || !couponDO.getSellerId().equals(0L) || !couponDO.getType().equals(CouponType.ACTIVITY_GIVE.name())) {
                        throw new ServiceException(PromotionErrorCode.E400.code(), "优惠券不合法");
                    }
                }
                BeanUtil.copyProperties(reward, newSignInReward);
                newSignInReward.setActivityId(id);
                rewardList.add(newSignInReward);
            }
            signInRewardManager.saveBatch(rewardList);
        }
        return getInfo(id);
    }

    @Override
    public SignBuyerInfoVO getInfo() {
        //查询过期的活动 条件：状态为已开始  时间已经过去 修改为已结束
        List<SignInActivity> finishActivity = lambdaQuery()
                .lt(SignInActivity::getEndDate, DateUtil.getDateline())
                .eq(SignInActivity::getStatus, SignInActivityStatus.OPEN.name())
                .ne(SignInActivity::getStatus, SignInActivityStatus.CLOSE.name())
                .list();
        if (!finishActivity.isEmpty()) {
            finishActivity.stream().map(activity -> {
                activity.setStatus(SignInActivityStatus.FINISH.name());
                return activity;
            }).collect(Collectors.toList());
            this.updateBatchById(finishActivity);
        }

        //查询当前活动信息
        List<SignInActivity> list = lambdaQuery().le(SignInActivity::getStartDate, DateUtil.getDateline()) // 开始时间小于等于当前时间
                .ge(SignInActivity::getEndDate, DateUtil.getDateline()) // 结束时间大于等于当前时间
                .notIn(SignInActivity::getStatus, SignInActivityStatus.CLOSE.name(), SignInActivityStatus.FINISH.name())
                .list();

        if (list.isEmpty()) {
            return null;
        }
        SignInActivity signInActivity = list.get(0);
        signInActivity.setStatus(SignInActivityStatus.OPEN.name());
        this.updateById(signInActivity);

        SignBuyerInfoVO signBuyerInfoVO = new SignBuyerInfoVO();
        //构建商品信息
        if (!StringUtil.isEmpty(signInActivity.getRecommendedProductId())) {
            String[] productIdStrings = signInActivity.getRecommendedProductId().split(",");
            Long[] productIds = Arrays.stream(productIdStrings)
                    .map(s -> Long.parseLong(s)) // 使用lambda表达式
                    .toArray(Long[]::new);
            List<GoodsDO> goodsDOS = goodsQueryManager.queryDo(productIds);
            //排除未上架的商品
            List<GoodsDO> availableGoods = goodsDOS.stream()
                    .filter(goods -> goods.getMarketEnable().equals(1)) // 假设"OnSale"表示商品在售
                    .collect(Collectors.toList());
            //组织数据
            List<SignGoodsDTO> signGoods = new ArrayList<>();
            for (GoodsDO goodsDO : availableGoods) {
                SignGoodsDTO signGoodsDTO = new SignGoodsDTO(goodsDO);
                signGoods.add(signGoodsDTO);
            }
            signBuyerInfoVO.setGoods(signGoods);
        }
        //构建签到数据
        SignInRecordSearch signInRecordSearch = new SignInRecordSearch();
        signInRecordSearch.setMemberId(UserContext.getBuyer().getUid());
        signInRecordSearch.setSignActivityId(signInActivity.getId());
        //签到记录
        List<SignInRecord> signInRecords = signInRecordManager.list(signInRecordSearch);
        //签到设置
        List<SignInReward> rewards = signInRewardManager.getByActivityId(signInActivity.getId());
        //构建签到日期数据
        List<SignDay> signDays = this.generateSignDays(signInRecords, rewards, signInActivity);
        signBuyerInfoVO.setSignDays(signDays);
        signBuyerInfoVO.setRecords(signInRecords);
        signBuyerInfoVO.setTitle(signInActivity.getTitle());
        signBuyerInfoVO.setDescription(signInActivity.getDescription());
        signBuyerInfoVO.setRewards(rewards);

        return signBuyerInfoVO;
    }


    private boolean checkoutDate(Long startTime, Long endTime) {
        //校验是否存在重叠的活动时间
        int count = lambdaQuery().lt(SignInActivity::getStartDate, startTime) // 数据库中的startTime小于新的endTime
                .gt(SignInActivity::getEndDate, endTime) // 数据库中的endTime大于新的startTime
                .notIn(SignInActivity::getStatus, SignInActivityStatus.CLOSE.name(), SignInActivityStatus.FINISH.name()).count();
        return count != 0;
    }


    public List<SignDay> generateSignDays(List<SignInRecord> signInRecords, List<SignInReward> signInRewards, SignInActivity sign) {

        LocalDate currentDate = LocalDate.now(ZoneId.of("UTC"));
        List<SignDay> signDays = new ArrayList<>();
        int continuousSignDays = 0; // 用于记录连续签到的天数
        LocalDate lastSignDate = null; // 记录最后一次签到的日期

        LocalDate startDate = Instant.ofEpochMilli(sign.getStartDate() * 1000)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        LocalDate endDate = Instant.ofEpochMilli(sign.getEndDate() * 1000)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        List<LocalDate> dates = getDatesBetween(startDate, endDate);

        for (LocalDate date : dates) {
            SignDay signDay = new SignDay();
            signDay.setSignDay(date.getDayOfMonth());
            signDay.setSign(false); // 默认设置为未签到
            signDay.setSignPrize(false); // 默认设置为不赠送礼物
            signDay.setDay(date.toString());


            // 检查日期是否在活动时间内
            if (date.isBefore(startDate) || date.isAfter(endDate)) {
                signDays.add(signDay);
                continue; // 如果不在活动时间内，跳过此日期
            }

            // 检查是否签到
            for (SignInRecord record : signInRecords) {
                LocalDate signDate = LocalDate.ofEpochDay(record.getSignInDate() / (24 * 60 * 60));
                if (signDate.isEqual(date)) {
                    lastSignDate = signDate;
                    signDay.setSign(true);
                    continuousSignDays++; // 签到则连续签到天数加一
                    break;
                }
            }

            // 如果今天未签到，重置连续签到天数
            if (!signDay.getSign() && date.getDayOfMonth() < currentDate.getDayOfMonth()) {
                continuousSignDays = 0;
            }

            // 检查是否满足连续签到奖励条件
            // 当前遍历到的日期已经超过了最后一次签到日期，需要检查是否满足奖励条件
            if (date.isAfter(currentDate) && lastSignDate != null) {
                // 计算从最后一次签到日期到当前日期的天数差
                long daysAfterLastSign = date.toEpochDay() - lastSignDate.toEpochDay();
                // 检查是否满足连续签到奖励条件
                for (SignInReward reward : signInRewards) {
                    long daysNeededForReward = reward.getSignDay() - continuousSignDays; // 还需要多少天才能达到奖励条件
                    if (reward.getSignDay() != 1 && daysAfterLastSign == daysNeededForReward && !signDay.getSign()) {
                        signDay.setSignPrize(true); // 如果天数差等于还需要的天数，则设置奖励
                        break;
                    }
                }
            }

            signDays.add(signDay);
        }

        //计算好后返回当月数据
        if (!signDays.isEmpty()) {
            // 使用迭代器遍历列表，以便在迭代过程中删除元素
            Iterator<SignDay> iterator = signDays.iterator();

            int targetYear = currentDate.getYear();
            int targetMonth = currentDate.getMonthValue();

            while (iterator.hasNext()) {
                SignDay signDay = iterator.next();
                LocalDate date = LocalDate.parse(signDay.getDay());

                // 检查年份和月份是否匹配
                if (date.getYear() != targetYear || date.getMonthValue() != targetMonth) {
                    // 如果不匹配，就从列表中移除该元素
                    iterator.remove();
                }
            }
        }

        return signDays;


    }

    public static List<LocalDate> getDatesBetween(LocalDate startDate, LocalDate endDate) {
        List<LocalDate> dates = new ArrayList<>();
        while (!startDate.isAfter(endDate)) {
            dates.add(startDate);
            startDate = startDate.plusDays(1);
        }
        return dates;
    }


    @Override
    @Transactional(value = "tradeTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public SignBuyerInfoVO sign(Long memberId) {
        Lock lock = lockFactory.getLock(memberId + "");

        try {
            //在多线程的情况下 售后信息可能会出现脏读的情况 需要加锁
            lock.lock();
            //查询当前活动信息
            List<SignInActivity> list = lambdaQuery().le(SignInActivity::getStartDate, DateUtil.getDateline()) // 开始时间小于等于当前时间
                    .ge(SignInActivity::getEndDate, DateUtil.getDateline()) // 结束时间大于等于当前时间
                    .notIn(SignInActivity::getStatus, SignInActivityStatus.CLOSE.name(), SignInActivityStatus.CLOSE.name())
                    .list();

            if (list.isEmpty()) {
                return null;
            }
            SignInActivity signInActivity = list.get(0);
            //查询今日是否签到
            List<SignInRecord> records = signInRecordManager.getCurrent(memberId, signInActivity.getId());
            if (!records.isEmpty()) {
                throw new ServiceException(PromotionErrorCode.E400.code(), "当日已经签到");
            }
            //签到设置
            List<SignInReward> rewards = signInRewardManager.getByActivityId(signInActivity.getId());

            SignInRecordSearch signInRecordSearch = new SignInRecordSearch();
            signInRecordSearch.setSignActivityId(signInActivity.getId());
            signInRecordSearch.setMemberId(memberId);
            List<SignInRecord> signInRecords = signInRecordManager.list(signInRecordSearch);


            Member member = memberClient.getModel(memberId);
            if (member == null) {
                throw new ServiceException(MemberErrorCode.E107.code(), "会员不存在");
            }

            // 将签到记录按日期排序，并去重（如果一天签到多次，只保留一次）
            List<LocalDate> distinctDates = signInRecords.stream()
                    .map(SignInRecord::getSignInDate) // 假设getSignInDay返回的是以秒为单位的时间戳
                    .map(seconds -> Instant.ofEpochMilli(seconds * 1000L).atZone(ZoneId.systemDefault()).toLocalDate())
                    .distinct()
                    .sorted(Comparator.reverseOrder()) // 从最近的日期开始排序
                    .collect(Collectors.toList());


            int consecutiveDays = 0;
            LocalDate currentDate = LocalDate.now(ZoneId.of("UTC")); // 从当前日期开始检查

            for (LocalDate date : distinctDates) {
                // 如果列表中的日期是当前日期或者是当前日期的前一天，则连续签到天数加一
                if (date.equals(currentDate) || date.equals(currentDate.minusDays(1))) {
                    consecutiveDays++;
                    currentDate = date; // 更新当前日期为列表中的日期，以便继续检查前一天
                } else {
                    // 如果发现不连续的日期，则停止计算
                    break;
                }
            }
            //因为今天也签到 所以连续签到时间+1
            consecutiveDays++;
            SignInReward sign = null;
            SignInReward oneDaySign = null;
            //根据活动日期 和 连续签到时间计算 是否满足连续签到奖励
            for (SignInReward reward : rewards) {
                if (reward.getSignDay() == 1) {
                    oneDaySign = reward;
                }
                // 如果连续签到天数大于或等于奖励所需的天数，并且奖励天数大于1，则返回true
                if (reward.getSignDay() > 1 && consecutiveDays == reward.getSignDay()) {
                    sign = reward;
                }
            }

            List<SignInRecord> recordList = new ArrayList<>();
            // 使用提取的方法来简化原始代码
            if (sign != null) {
                if (SwitchEnum.OPEN.name().equals(sign.getPointSwitch())) {
                    SignInRecord signInRecord = createSignInRecord(signInActivity, member, SignInPrizeType.POINT.name(), (long) sign.getPoints());
                    signInRecord.setDescription("连续签到" + consecutiveDays + "天赠送" + sign.getPoints() + "积分");
                    recordList.add(signInRecord);
                }
                if (SwitchEnum.OPEN.name().equals(sign.getCouponSwitch())) {
                    CouponDO couponDO = couponManager.getModel(sign.getCouponId());
                    if (couponDO != null && DateUtil.getDateline() >= couponDO.getStartTime() && DateUtil.getDateline() <= couponDO.getEndTime() && couponDO.getReceivedNum() < couponDO.getCreateNum()) {
                        SignInRecord signInRecord = createSignInRecord(signInActivity, member, SignInPrizeType.COUPON.name(), sign.getCouponId());
                        signInRecord.setDescription("连续签到" + consecutiveDays + "天赠送" + couponDO.getTitle() + "优惠券");
                        recordList.add(signInRecord);
                    }
                }
            } else {
                if (oneDaySign != null) {
                    SignInRecord signInRecord = createSignInRecord(signInActivity, member, SignInPrizeType.POINT.name(), (long) oneDaySign.getPoints());
                    signInRecord.setDescription("签到赠送" + oneDaySign.getPoints() + "积分");
                    recordList.add(signInRecord);
                }
            }
            if (!recordList.isEmpty()) {
                signInRecordManager.saveBatch(recordList);
                // 发送消息
                SignMessage signMessage = new SignMessage(recordList);
                this.messageSender.send(signMessage);
            }

        } finally {
            lock.unlock();
        }

        return null;
    }


    @Override
    public void sendGift(List<SignInRecord> recordList) {

        for (SignInRecord signInRecord : recordList) {
            //如果赠送的是积分
            if (signInRecord.getPrizeType().equals(SignInPrizeType.POINT.name())) {
                MemberPointHistory memberPointHistory = new MemberPointHistory();
                memberPointHistory.setGradePoint(0);
                memberPointHistory.setGradePointType(0);
                memberPointHistory.setConsumPointType(1);
                memberPointHistory.setConsumPoint(signInRecord.getPrize());
                memberPointHistory.setReason(signInRecord.getDescription());
                memberPointHistory.setMemberId(signInRecord.getMemberId());
                memberPointHistory.setOperator("系统");
                memberClient.pointOperation(memberPointHistory);
            }
            //如果赠送的是优惠券
            if (signInRecord.getPrizeType().equals(SignInPrizeType.COUPON.name())) {
                memberCouponManager.receiveBonus(signInRecord.getMemberId(), signInRecord.getMemberName(), signInRecord.getPrize());
            }

        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id) {
        // 删除活动
        removeById(id);
        // 删除活动奖品
        signInRewardManager.deleteByActivityId(id);
    }

    private SignInRecord createSignInRecord(SignInActivity signInActivity, Member member, String prizeType, Long prize) {
        SignInRecord signInRecord = new SignInRecord();
        signInRecord.setSignInDate(DateUtil.getDateline());
        signInRecord.setSignActivityId(signInActivity.getId());
        signInRecord.setCreateTime(DateUtil.getDateline());
        signInRecord.setMemberId(member.getMemberId());
        signInRecord.setMemberName(member.getUname());
        signInRecord.setPrizeType(prizeType);
        signInRecord.setPrize(prize);
        return signInRecord;
    }

    public static void main(String[] args) {
        System.out.println(DateUtil.getDateline());
    }

}

