package cn.shoptnt.service.promotion.sign.impl;

import cn.shoptnt.mapper.promotion.sigin.SignInRewardMapper;
import cn.shoptnt.model.promotion.sign.dos.SignInReward;
import cn.shoptnt.service.promotion.sign.SignInRewardManager;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title SignInRewardManagerImpl
 * @description 签到奖品业务实现类
 * @program: api
 * 2024/3/12 14:12
 */
@Service
public class SignInRewardManagerImpl extends ServiceImpl<SignInRewardMapper, SignInReward> implements SignInRewardManager {


    @Override
    public List<SignInReward> getByActivityId(Long activityId) {
        return lambdaQuery().eq(SignInReward::getActivityId, activityId).list();
    }

    @Override
    public void deleteByActivityId(Long activityId) {
        this.lambdaUpdate().eq(SignInReward::getActivityId, activityId).remove();
    }
}
