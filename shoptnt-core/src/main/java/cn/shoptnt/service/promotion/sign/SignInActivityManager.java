package cn.shoptnt.service.promotion.sign;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.sign.dos.SignInActivity;
import cn.shoptnt.model.promotion.sign.dos.SignInRecord;
import cn.shoptnt.model.promotion.sign.dto.SignInActivityOperation;
import cn.shoptnt.model.promotion.sign.dto.SignInActivitySearch;
import cn.shoptnt.model.promotion.sign.vos.SignBuyerInfoVO;
import cn.shoptnt.model.promotion.sign.vos.SignInActivityInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title signInActivityManager
 * @description 签到活动业务类
 * @program: api
 * 2024/3/12 12:04
 */
public interface SignInActivityManager extends IService<SignInActivity> {


    /**
     * 分页查询签到活动
     *
     * @param page                 页码
     * @param pageSize             每页数量
     * @param signInActivitySearch 查询关键字
     * @return WebPage
     */
    WebPage list(long page, long pageSize, SignInActivitySearch signInActivitySearch);

    /**
     * 关闭活动
     *
     * @param activityId 活动id
     */
    void close(Long activityId);

    /**
     * 根据活动id查询活动详细信息
     *
     * @param activityId 活动id
     * @return 签到活动
     */
    SignInActivityInfo getInfo(Long activityId);

    /**
     * 获取当前正在进行的签到活动详细信息
     *
     * @return 签到活动
     */
    SignBuyerInfoVO getInfo();

    /**
     * 添加签到活动
     *
     * @param signInActivityOperation 签到活动
     * @return 签到活动
     */
    SignInActivityInfo add(SignInActivityOperation signInActivityOperation);

    /**
     * 修改签到活动
     *
     * @param id                      活动id
     * @param signInActivityOperation 签到活动
     * @return 签到活动
     */
    SignInActivityInfo edit(Long id, SignInActivityOperation signInActivityOperation);


    /**
     * 会员签到
     *
     * @param memberId 会员id
     * @return
     */
    SignBuyerInfoVO sign(Long memberId);

    /**
     * 根据签到记录赠送礼物
     *
     * @param recordList 签到记录
     */
    void sendGift(List<SignInRecord> recordList);


    /**
     * 删除活动
     * @param id 活动id
     */
    void delete(Long id);
}
