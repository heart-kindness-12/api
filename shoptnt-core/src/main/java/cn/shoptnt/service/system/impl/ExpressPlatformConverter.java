/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.system.impl;

import cn.shoptnt.service.base.plugin.express.ExpressPlatform;
import cn.shoptnt.model.system.vo.ExpressPlatformVO;

/**
 * Express platform 转换器
 * @author 妙贤
 * @version 1.0
 * @since 7.3.0
 * 2020/4/2
 */

public class ExpressPlatformConverter {

    /**
     * 使用一个插件转换vo
     * @param expressPlatform
     * @return
     */
    public static ExpressPlatformVO toExpressPlatformVO (ExpressPlatform expressPlatform) {

        ExpressPlatformVO vo = new ExpressPlatformVO();
        vo.setId(0L);
        vo.setName(expressPlatform.getPluginName());
        vo.setOpen(expressPlatform.getIsOpen());
        vo.setBean(expressPlatform.getPluginId());
        vo.setConfigItems( expressPlatform.definitionConfigItem());
        return vo;
    }
}
