/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.system.impl;

import cn.shoptnt.service.base.plugin.upload.Uploader;
import cn.shoptnt.model.system.vo.UploaderVO;

/**
 * Uploader vo转换器
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2020/4/2
 */

public class UploaderVoConverter {

    /**
     * 通过插件转换vo
     * @param plugin
     * @return
     */
    public static UploaderVO toValidatorPlatformVO(Uploader plugin) {
        UploaderVO vo = new UploaderVO();
        vo.setId(0L);
        vo.setName(plugin.getPluginName());
        vo.setOpen(plugin.getIsOpen());
        vo.setBean(plugin.getPluginId());
        vo.setConfigItems( plugin.definitionConfigItem());
        return vo;
    }


}
