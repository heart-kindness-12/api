/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.system;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.system.dos.SystemLogs;
import cn.shoptnt.model.system.dto.SystemLogsParam;

/**
 * 系统日志业务层
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2021-03-22 16:05:58
 */
public interface SystemLogsManager	{

	/**
	 * 查询系统日志列表
	 * @param param 查询参数
	 * @return WebPage
	 */
     WebPage list(SystemLogsParam param,String client);
	/**
	 * 添加系统日志
	 * @param systemLogs 系统日志
	 * @return SystemLogs 系统日志
	 */
	SystemLogs add(SystemLogs systemLogs);

	/**
	 * 删除系统日志
	 * @param id 系统日志主键
	 */
	void delete(Long id);
	
	/**
	 * 获取系统日志
	 * @param id 系统日志主键
	 * @return SystemLogs  系统日志
	 */
	SystemLogs getModel(Long id);

}