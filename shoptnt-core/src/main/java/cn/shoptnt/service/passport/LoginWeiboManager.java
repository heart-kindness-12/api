/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.passport;

import cn.shoptnt.model.member.dto.LoginAppDTO;

import java.util.Map;

/**
 * 新浪微博登陆相关接口
 * @author cs
 * @version v1.0
 * @since v7.2.2
 * 2020-10-30
 */
public interface LoginWeiboManager {


    /**
     * 获取wap登陆跳转地址
     * @param redirectUri
     * @return
     */
    String getLoginUrl(String redirectUri);


    /**
     * wap登陆
     * @param code
     * @param uuid
     * @param redirectUri
     * @return
     */
    Map wapLogin(String code, String uuid, String redirectUri);

    /**
     * app登陆
     * @param loginAppDTO
     * @return
     */
    Map appLogin(LoginAppDTO loginAppDTO);
}
