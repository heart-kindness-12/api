/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.goods.dos.GoodsSkuDO;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * GoodsSku的Mapper
 * @author fk
 * @version 1.0
 * @since 7.1.0
 * 2020/7/21
 */
//不做二级缓存：因为要做会话级别的加解密，用的是会话秘钥，会导致多个秘钥污染同一片缓存数据
////@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface GoodsSkuMapper extends BaseMapper<GoodsSkuDO> {

    /**
     * 查询sku库存
     * @param skuId skuId
     * @return
     */
    Map queryQuantity(@Param("sku_id") Long skuId);

    /**
     * 更新sku库存
     * @param param 条件
     */
    void updateQuantity(@Param("param") Map param);

    /**
     * 查询sku的list集合
     * @param skuIdList skuid集合
     * @return
     */
    List<GoodsSkuVO> queryGoodsSkuVOList(@Param("params") List<Long> skuIdList);

    /**
     * 查询某商家的上架，审核通过的sku集合
     * @param sellerId 商家id
     * @return
     */
    List<GoodsSkuDO> querySellerAllSku(@Param("seller_id")Long sellerId);

    /**
     * 获取商品sku所属分类关联的规格所占用的sku数量
     * @param categoryId 分类ID
     * @param specs 规格信息（示例："spec_id":54511313545454414）
     * @return
     */
    Integer selectSkuCategorySpecNum(@Param("category_id") Long categoryId, @Param("specs") String specs);
}
