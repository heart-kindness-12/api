/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import cn.shoptnt.model.goods.dos.TagGoodsDO;
import cn.shoptnt.model.goods.vo.GoodsSelectLine;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * TagGoods的Mapper
 * @author fk
 * @version 1.0
 * @since 7.1.0
 * 2020/7/21
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface TagGoodsMapper extends BaseMapper<TagGoodsDO> {

    /**
     * 查询标签商品
     * @param tagId 标签id
     * @param num 数量
     * @return
     */
    List<GoodsSelectLine> queryTagGoods(@Param("tag_id") Long tagId, @Param("num") Integer num);

    /**
     * 查询标签商品分页
     * @param iPage 分页条件
     * @param tagId 标签id
     * @return
     */
    IPage queryTagGoodsPage(IPage iPage,@Param("tag_id") Long tagId);


}
