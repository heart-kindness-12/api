/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.goods.dos.BrandDO;
import cn.shoptnt.model.goods.vo.BrandVO;
import cn.shoptnt.model.goods.vo.SelectVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 品牌BrandDO的Mapper
 * @author fk
 * @version 1.0
 * @since 7.1.0
 * 2020/7/21
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface BrandMapper extends BaseMapper<BrandDO> {

    /**
     * 查询某分类下的品牌
     *
     * @param categoryId 分类id
     * @return
     */
    List<BrandDO> getBrandsByCategory(@Param("category_id") Long categoryId);

    /**
     * 查询分类品牌，所有品牌，分类绑定的品牌为已选中状态
     *
     * @param categoryId 分类id
     * @return
     */
    List<SelectVO> getCatBrand(@Param("category_id")Long categoryId);

    /**
     * 查询所有关联了一级分类的品牌信息集合
     * @return
     */
    List<BrandVO> getAllBrandCategory();

    /**
     * 模糊查询分类所有品牌，分类绑定品牌为已选中状态
     * @param categoryId 分类 id
     * @param name 品牌名称
     * @return
     */
    List<SelectVO> searchBrand(@Param("category_id")Long categoryId,@Param("name")String name);
}
