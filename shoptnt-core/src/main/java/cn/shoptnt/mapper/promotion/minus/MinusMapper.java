/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.promotion.minus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.model.promotion.minus.dos.MinusDO;
import cn.shoptnt.model.promotion.minus.vo.MinusVO;
import cn.shoptnt.model.promotion.seckill.dos.SeckillApplyDO;
import cn.shoptnt.model.promotion.seckill.vo.SeckillApplyVO;
import org.apache.ibatis.annotations.Param;

/**
 * 单品立减mapper
 * @author zs
 * @version v1.0
 * @since v7.2.2
 * 2020-08-11
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface MinusMapper extends BaseMapper<MinusDO> {

    /**
     * 单品立减分页查询
     * @param page 分页数据
     * @param queryWrapper 查询参数
     * @return
     */
    IPage<MinusVO> selectMinusVoPage(Page page, @Param(Constants.WRAPPER) QueryWrapper<MinusDO> queryWrapper);
}
