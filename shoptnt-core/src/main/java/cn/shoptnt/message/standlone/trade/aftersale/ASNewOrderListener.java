package cn.shoptnt.message.standlone.trade.aftersale;

import cn.shoptnt.message.dispatcher.trade.aftersale.ASNewOrderDispatcher;
import cn.shoptnt.model.base.message.AsOrderStatusChangeMsg;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 创建 换货或补发产生的新订单 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class ASNewOrderListener {

    @Autowired
    private ASNewOrderDispatcher asNewOrderDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void createOrder(AsOrderStatusChangeMsg orderStatusChangeMsg) {
        asNewOrderDispatcher.dispatch(orderStatusChangeMsg);
    }
}
