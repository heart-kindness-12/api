package cn.shoptnt.message.standlone.payment;

import cn.shoptnt.message.dispatcher.payment.PaymentBillChangeDispatcher;
import cn.shoptnt.model.base.message.PaymentBillStatusChangeMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 支付账单状态改变 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class PaymentBillChangeListener {

    @Autowired
    private PaymentBillChangeDispatcher paymentBillChangeDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void billChange(PaymentBillStatusChangeMsg paymentBillMessage) {
        paymentBillChangeDispatcher.dispatch(paymentBillMessage);
    }
}
