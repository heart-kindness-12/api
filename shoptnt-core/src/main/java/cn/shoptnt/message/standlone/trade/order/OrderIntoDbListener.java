package cn.shoptnt.message.standlone.trade.order;

import cn.shoptnt.message.dispatcher.trade.order.OrderIntoDbDispatcher;
import cn.shoptnt.model.base.message.TradeCreateMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 订单入库 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class OrderIntoDbListener {

    @Autowired
    private OrderIntoDbDispatcher orderIntoDbDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void tradeIntoDb(TradeCreateMessage tradeCreateMessage) {
        orderIntoDbDispatcher.dispatch(tradeCreateMessage);
    }
}
