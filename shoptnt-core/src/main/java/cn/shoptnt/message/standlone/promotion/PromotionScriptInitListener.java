package cn.shoptnt.message.standlone.promotion;

import cn.shoptnt.message.dispatcher.trade.promotion.PromotionScriptInitDispatcher;
import cn.shoptnt.model.base.message.PromotionScriptInitMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 促销脚本初始化监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author zs
 * @since 2024-03-13
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class PromotionScriptInitListener {

    @Autowired
    private PromotionScriptInitDispatcher dispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void billChange(PromotionScriptInitMessage message) {
        dispatcher.dispatch(message);
    }
}
