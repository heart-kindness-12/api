/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.member;

import cn.shoptnt.client.trade.OrderMetaClient;
import cn.shoptnt.message.event.GoodsCommentEvent;
import cn.shoptnt.message.event.MemberLoginEvent;
import cn.shoptnt.message.event.MemberRegisterEvent;
import cn.shoptnt.message.event.OrderStatusChangeEvent;
import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.model.base.message.GoodsCommentMsg;
import cn.shoptnt.model.base.message.MemberRegisterMsg;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.client.member.MemberClient;
import cn.shoptnt.client.member.MemberCommentClient;
import cn.shoptnt.client.system.SettingClient;
import cn.shoptnt.model.member.dos.MemberPointHistory;
import cn.shoptnt.model.member.enums.CommentTypeEnum;
import cn.shoptnt.model.member.vo.MemberLoginMsg;
import cn.shoptnt.model.system.vo.PointSetting;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.enums.OrderMetaKeyEnum;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import cn.shoptnt.model.trade.order.enums.PaymentTypeEnum;
import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.framework.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 会员积分操作
 *
 * @author zh
 * @version v7.0
 * @date 18/7/16 上午10:44
 * @since v7.0
 */
@Component
public class MemberPointConsumer implements MemberLoginEvent, MemberRegisterEvent, GoodsCommentEvent, OrderStatusChangeEvent {

    @Autowired
    private SettingClient settingClient;
    @Autowired
    private MemberClient memberClient;
    @Autowired
    private MemberCommentClient memberCommentClient;

    @Autowired
    private OrderMetaClient orderMetaClient;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void memberLogin(MemberLoginMsg memberLoginMsg) {

        //如果是会员登录
        if (memberLoginMsg.getMemberOrSeller().equals(1)) {

            String pointSettingJson = settingClient.get(SettingGroup.POINT);

            PointSetting pointSetting = JsonUtil.jsonToObject(pointSettingJson, PointSetting.class);

            //会员登录送积分开启
            if (pointSetting.getLogin().equals(1)) {
                //当第一次注册登录的时候上次登录时间为null
                if (memberLoginMsg.getLastLoginTime() == null) {
                    this.setPoint(1, pointSetting.getLoginGradePoint(), 1, pointSetting.getLoginConsumerPoint().longValue(), "每天首次登录送积分", memberLoginMsg.getMemberId());
                } else {
                    //上次登录时间
                    long lDate = memberLoginMsg.getLastLoginTime() * 1000;
                    Date date = new Date(lDate);
                    //当前时间
                    Date today = new Date();
                    //判断本地登录是否是今天
                    if (!DateUtil.toString(date, "yyyy-MM-dd").equals(DateUtil.toString(today, "yyyy-MM-dd"))) {
                        this.setPoint(1, pointSetting.getLoginGradePoint(), 1, pointSetting.getLoginConsumerPoint().longValue(), "每天首次登录送积分", memberLoginMsg.getMemberId());
                    }
                }


            }
        }
    }


    @Override
    public void memberRegister(MemberRegisterMsg memberRegisterMsg) {
        String pointSettingJson = settingClient.get(SettingGroup.POINT);
        PointSetting pointSetting = JsonUtil.jsonToObject(pointSettingJson, PointSetting.class);
        //会员登录送积分开启
        if (pointSetting.getRegister().equals(1)) {
            this.setPoint(1, pointSetting.getRegisterGradePoint(), 1, pointSetting.getRegisterConsumerPoint().longValue(), "会员注册送积分", memberRegisterMsg.getMember().getMemberId());
        }
    }


    @Override
    public void goodsComment(GoodsCommentMsg goodsCommentMsg) {

        if(goodsCommentMsg.getComment() == null || goodsCommentMsg.getComment().isEmpty()){
            return;
        }

        //只有不是系统自动评论并且操作类型是添加操作才会赠送积分
        if (!goodsCommentMsg.isAutoComment() && GoodsCommentMsg.ADD == goodsCommentMsg.getOperaType()) {
            String pointSettingJson = settingClient.get(SettingGroup.POINT);
            PointSetting pointSetting = JsonUtil.jsonToObject(pointSettingJson, PointSetting.class);

            goodsCommentMsg.getComment().forEach(comment->{
                // 只有初次评论才赠送积分，追加评论不赠送积分
                if (CommentTypeEnum.INITIAL.value().equals(comment.getCommentsType())) {
                    //图片评论送积分
                    if (pointSetting.getCommentImg().equals(1) && comment.getHaveImage().equals(1)) {
                        this.setPoint(1, pointSetting.getCommentImgGradePoint(), 1, pointSetting.getCommentImgConsumerPoint().longValue(), "图片评论送积分", comment.getMemberId());
                    }
                    //文字评论送积分
                    if (pointSetting.getComment().equals(1) && comment.getHaveImage().equals(1)) {
                        this.setPoint(1, pointSetting.getCommentGradePoint(), 1, pointSetting.getCommentConsumerPoint().longValue(), "文字评论送积分", comment.getMemberId());
                    }
                    Integer count = memberCommentClient.getGoodsCommentCount(comment.getGoodsId());
                    //此处评论数量判断为1，因为此时评论数量已经添加了
                    if (pointSetting.getFirstComment().equals(1) && (count.equals(1)||count.equals(0))) {
                        this.setPoint(1, pointSetting.getFirstCommentGradePoint(), 1, pointSetting.getFirstCommentConsumerPoint().longValue(), "每个商品首次评论", comment.getMemberId());
                    }
                }
            });
        }
    }

    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {
        String pointSettingJson = settingClient.get(SettingGroup.POINT);
        PointSetting pointSetting = JsonUtil.jsonToObject(pointSettingJson, PointSetting.class);
        //获取订单信息
        OrderDO orderDO = orderMessage.getOrderDO();
        //已付款状态
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.PAID_OFF.name())) {
            //如果开启了在线支付送积分并且订单状态为已付款且订单支付为在线付款 则送积分
            if (pointSetting.getOnlinePay().equals(1) && orderDO.getPaymentType().equals(PaymentTypeEnum.ONLINE.name())) {
                this.setPoint(1, pointSetting.getOnlinePayGradePoint(), 1, pointSetting.getOnlinePayConsumerPoint().longValue(), "在线支付送积分", orderDO.getMemberId());
            }
        }
        //已完成状态
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.COMPLETE.name())) {
            //如果开启购买商品送积分并且为订单状态为已完成状态 则送积分
            if (pointSetting.getBuyGoods().equals(1)) {
                Double point  = CurrencyUtil.mul(pointSetting.getBuyGoodsConsumerPoint().doubleValue(), orderDO.getOrderPrice());
                this.setPoint(1, Integer.parseInt(new java.text.DecimalFormat("0").format(CurrencyUtil.mul(pointSetting.getBuyGoodsGradePoint(), orderDO.getOrderPrice().intValue()))), 1, point.longValue(), "购买商品送积分", orderDO.getMemberId());
            }
        }

        // 订单已收货 发放赠送积分
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.ROG.name())) {

            String metaJson = this.orderMetaClient.getMetaValue(orderMessage.getOrderDO().getSn(), OrderMetaKeyEnum.GIFT_POINT);

            if (StringUtil.isEmpty(metaJson) || "0".equals(metaJson)) {
                return;
            }

            this.setPoint(1, 0, 1, new Long(metaJson), "满赠优惠活动赠送", orderMessage.getOrderDO().getMemberId());

        }
    }

    /**
     * 对积分的操作
     *
     * @param gradePointType  等级积分类型 1为增加积分 ，如果等级积分为0的时候等级积分类型为0则为无操作，如果等级积分不为0的时候积分类型为0则为消费
     * @param gradePoint      等级积分
     * @param consumPointType 消费积分类型 1为增加积分 ，如果消费积分为0的时候消费积分型为0则为无操作，如果消费积分不为0的时候消费积分为0则为消费
     * @param consumPoint     消费积分
     * @param remark          备注
     * @param memberId        会员id
     */
    private void setPoint(Integer gradePointType, Integer gradePoint, Integer consumPointType, Long consumPoint, String remark, Long memberId) {

        if (gradePoint == 0 && consumPoint == 0) {
            return;
        }

        MemberPointHistory memberPointHistory = new MemberPointHistory();
        memberPointHistory.setGradePoint(gradePoint);
        memberPointHistory.setGradePointType(gradePointType);
        memberPointHistory.setConsumPointType(consumPointType);
        memberPointHistory.setConsumPoint(consumPoint);
        memberPointHistory.setReason(remark);
        memberPointHistory.setMemberId(memberId);
        memberPointHistory.setOperator("系统");
        memberClient.pointOperation(memberPointHistory);
    }

}
