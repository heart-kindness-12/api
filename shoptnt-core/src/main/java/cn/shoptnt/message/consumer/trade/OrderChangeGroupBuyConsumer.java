/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.trade;

import cn.shoptnt.client.promotion.PromotionGoodsClient;
import cn.shoptnt.client.trade.OrderOperateClient;
import cn.shoptnt.message.event.OrderStatusChangeEvent;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.enums.OrderOutStatusEnum;
import cn.shoptnt.model.trade.order.enums.OrderOutTypeEnum;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 团购商品库存变更
 *
 * @author Snow create in 2018/5/22
 * @version v2.0
 * @since v7.0.0
 */
@Component
public class OrderChangeGroupBuyConsumer implements OrderStatusChangeEvent {

    private final Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private PromotionGoodsClient promotionGoodsClient;

    @Autowired
    private OrderOperateClient orderOperateClient;

    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {

        OrderDO orderDO = orderMessage.getOrderDO();
        //订单确认，优惠库存扣减
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.CONFIRM.name())) {

            this.orderOperateClient.editOutStatus(orderDO.getSn(), OrderOutTypeEnum.GROUPBUY_GOODS, OrderOutStatusEnum.SUCCESS);

        } else if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.CANCELLED.name())) {
            //恢复团购商品库存
            promotionGoodsClient.addGroupbuyQuantity(orderMessage.getOrderDO().getSn());
        } else if(orderMessage.getNewStatus().name().equals(OrderStatusEnum.INTODB_ERROR.name())){

            logger.error("团购库存扣减失败");
            this.orderOperateClient.editOutStatus(orderDO.getSn(), OrderOutTypeEnum.GROUPBUY_GOODS, OrderOutStatusEnum.FAIL);
        }

    }

}
