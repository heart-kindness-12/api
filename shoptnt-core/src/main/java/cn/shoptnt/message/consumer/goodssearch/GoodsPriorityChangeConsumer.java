/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.goodssearch;

import cn.shoptnt.client.goods.GoodsIndexClient;
import cn.shoptnt.message.event.GoodsPriorityChangeEvent;
import cn.shoptnt.model.base.message.GoodsChangeMsg;
import cn.shoptnt.client.goods.GoodsClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
* @author liuyulei
 * @version 1.0
 * @Description: 商品优先级变更修改索引
 * @date 2019/6/13 17:18
 * @since v7.0
 */
@Service
public class GoodsPriorityChangeConsumer implements GoodsPriorityChangeEvent {

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private GoodsIndexClient goodsIndexClient;

    @Override
    public void priorityChange(GoodsChangeMsg goodsChangeMsg) throws IOException {

        //判断操作类型为商品优先级变更
        if(goodsChangeMsg.getOperationType() == GoodsChangeMsg.GOODS_PRIORITY_CHANGE){
            Long[] goodsIds = goodsChangeMsg.getGoodsIds();

            //查询商品信息
            List<Map<String, Object>> list = goodsClient.getGoodsAndParams(goodsIds);

            if (list != null) {
                for (Map<String, Object> map : list) {
                    //更新索引
                    goodsIndexClient.updateIndex(map);
                }
            }
        }
    }
}
