package cn.shoptnt.message.dispatcher.goods;

import cn.shoptnt.message.event.GoodsPriorityChangeEvent;
import cn.shoptnt.model.base.message.GoodsChangeMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品优先级变动 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class GoodsPriorityChangeDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<GoodsPriorityChangeEvent> events;

    public void dispatch(GoodsChangeMsg goodsChangeMsg) {
        if (events != null) {
            for (GoodsPriorityChangeEvent event : events) {
                try {
                    event.priorityChange(goodsChangeMsg);
                } catch (Exception e) {
                    logger.error("处理商品优先级变化消息出错", e);
                }
            }
        }
    }
}
