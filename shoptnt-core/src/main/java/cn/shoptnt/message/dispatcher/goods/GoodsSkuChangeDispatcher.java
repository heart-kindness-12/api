package cn.shoptnt.message.dispatcher.goods;

import cn.shoptnt.message.event.GoodsSkuChangeEvent;
import cn.shoptnt.model.base.message.SkuChangeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品sku变化 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class GoodsSkuChangeDispatcher {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired(required = false)
    private List<GoodsSkuChangeEvent> events;

    public void dispatch(SkuChangeMessage skuChangeMessage) {
        if (events != null) {
            for (GoodsSkuChangeEvent event : events) {
                try {
                    event.goodsSkuChange(skuChangeMessage.getSkuIds());
                } catch (Exception e) {
                    logger.error("处理商品sku变化消息出错", e);
                }
            }
        }
    }
}
