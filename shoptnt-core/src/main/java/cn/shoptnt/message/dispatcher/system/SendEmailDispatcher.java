package cn.shoptnt.message.dispatcher.system;

import cn.shoptnt.message.event.SendEmailEvent;
import cn.shoptnt.model.base.vo.EmailVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 发送邮件 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class SendEmailDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<SendEmailEvent> events;

    public void dispatch(EmailVO emailVO) {
        if (events != null) {
            for (SendEmailEvent event : events) {
                try {
                    event.sendEmail(emailVO);
                } catch (Exception e) {
                    logger.error("发送邮件出错", e);
                }
            }
        }
    }
}
