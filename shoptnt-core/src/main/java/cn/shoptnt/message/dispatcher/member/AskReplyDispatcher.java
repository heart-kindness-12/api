package cn.shoptnt.message.dispatcher.member;

import cn.shoptnt.message.event.AskReplyEvent;
import cn.shoptnt.model.base.message.AskReplyMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会员咨询 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class AskReplyDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<AskReplyEvent> events;

    public void dispatch(AskReplyMessage askReplyMessage) {
        if (events != null) {
            for (AskReplyEvent event : events) {
                try {
                    event.askReply(askReplyMessage);
                } catch (Exception e) {
                    logger.error("处理会员回复商品咨询出错", e);
                }
            }
        }
    }
}
