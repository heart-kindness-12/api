package cn.shoptnt.message.dispatcher.system;

import cn.shoptnt.message.event.SmsSendMessageEvent;
import cn.shoptnt.model.base.vo.SmsSendVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 发送短信 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class SmsSendMessageDispatcher {

    @Autowired(required = false)
    private List<SmsSendMessageEvent> events;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public void dispatch(SmsSendVO smsSendVO) {
        if (events != null) {
            for (SmsSendMessageEvent event : events) {
                try {
                    event.send(smsSendVO);
                } catch (Exception e) {
                    logger.error("发送短信出错", e);
                }
            }
        }
    }
}
