package cn.shoptnt.message.dispatcher.member;

import cn.shoptnt.message.event.MemberInfoChangeEvent;
import cn.shoptnt.model.base.message.MemberInfoChangeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 修改会员资料 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class MemberInfoChangeDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<MemberInfoChangeEvent> events;

    public void dispatch(MemberInfoChangeMessage message) {
        if (events != null) {
            for (MemberInfoChangeEvent event : events) {
                try {
                    event.memberInfoChange(message.getMemberId());

                } catch (Exception e) {
                    logger.error("处理会员资料变化消息出错", e);
                }
            }
        }
    }
}
