package cn.shoptnt.message.dispatcher.shop;

import cn.shoptnt.message.event.ShopChangeEvent;
import cn.shoptnt.model.shop.vo.ShopChangeMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 店铺信息变更 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class ShopChangeDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<ShopChangeEvent> events;

    public void dispatch(ShopChangeMsg shopNameChangeMsg) {
        if (events != null) {
            for (ShopChangeEvent event : events) {
                try {
                    event.shopChange(shopNameChangeMsg);
                } catch (Exception e) {
                    logger.error("店铺信息变更消息出错", e);
                }
            }
        }
    }
}
