package cn.shoptnt.message.dispatcher.member;

import cn.shoptnt.message.event.MemberLoginEvent;
import cn.shoptnt.model.member.vo.MemberLoginMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会员登录 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class MemberLoginDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<MemberLoginEvent> events;

    public void dispatch(MemberLoginMsg memberLoginMsg) {
        if (events != null) {
            for (MemberLoginEvent event : events) {
                try {
                    event.memberLogin(memberLoginMsg);
                } catch (Exception e) {
                    logger.error("会员登录出错", e);
                }
            }
        }
    }
}
