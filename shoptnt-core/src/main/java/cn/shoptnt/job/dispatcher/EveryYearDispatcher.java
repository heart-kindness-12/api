/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.dispatcher;

import cn.shoptnt.job.EveryYearExecute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 每年执行定时任务
 *
 * @author LiuXT
 * @date 2022-10-23
 * @since v7.3.0
 */
@Component
public class EveryYearDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<EveryYearExecute> everyYearExecutes;

    public Boolean dispatch() {
        boolean result = true;
        if (everyYearExecutes != null) {
            for (EveryYearExecute everyYearExecute : everyYearExecutes) {
                try {
                    everyYearExecute.everyYear();
                } catch (Exception e) {
                    logger.error("每年任务异常：", e);
                    result = false;
                }
            }
        }
        return result;
    }
}
