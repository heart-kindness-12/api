package cn.shoptnt.job.dispatcher;

import cn.shoptnt.job.EveryDayExecute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 每日执行定时任务
 *
 * @author LiuXT
 * @date 2022-10-23
 * @since v7.3.0
 */
@Service
public class EveryDayDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private List<EveryDayExecute> everyDayExecutes;

    public Boolean dispatch() {
        boolean result = true;
        if (everyDayExecutes != null) {
            for (EveryDayExecute everyDayExecute : everyDayExecutes) {
                try {
                    everyDayExecute.everyDay();
                } catch (Exception e) {
                    logger.error("每日任务异常：", e);
                    result = false;
                }
            }
        }
        return result;
    }
}
