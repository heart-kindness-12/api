/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.standlone;

import cn.shoptnt.job.dispatcher.EveryTenMinutesDispatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 每十分钟执行定时任务
 *
 * @author LiuXT
 * @date 2022-10-23
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class EveryTenMinutesSchedule {

    @Autowired
    private EveryTenMinutesDispatcher everyTenMinutesDispatcher;

    /**
     * 定时器定义，设置执行时间
     * cron: 每10分钟执行
     */
    @Scheduled(cron = "0 */10 * * * ?")
    public void execute() {
        everyTenMinutesDispatcher.dispatch();
    }
}
