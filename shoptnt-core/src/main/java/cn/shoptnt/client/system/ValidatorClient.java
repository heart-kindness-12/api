/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system;

/**
 * 验证方式客户端
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.6
 * 2019-12-23
 */
public interface ValidatorClient {

    /**
     * 验证参数操作
     */
    void validate();

}
