/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system.impl;

import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.service.base.service.SettingManager;
import cn.shoptnt.client.system.SettingClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @author fk
 * @version v1.0
 * @Description: 系统设置
 * @date 2018/7/30 10:49
 * @since v7.0.0
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class SettingClientDefaultImpl implements SettingClient {

    @Autowired
    private SettingManager settingManager;
    /**
     * 系统参数配置
     *
     * @param group    系统设置的分组
     * @param settings 要保存的设置对象
     */
    @Override
    public void save(SettingGroup group, Object settings) {

        this.settingManager.save(group,settings);

    }
    /**
     * 获取配置
     *
     * @param group 分组名称
     * @return 存储对象
     */
    @Override
    public String get(SettingGroup group) {
        return this.settingManager.get(group);
    }
}
