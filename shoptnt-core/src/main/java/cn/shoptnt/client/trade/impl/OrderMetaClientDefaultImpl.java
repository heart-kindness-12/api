/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.trade.impl;

import cn.shoptnt.client.trade.OrderMetaClient;
import cn.shoptnt.model.promotion.fulldiscount.dos.FullDiscountGiftDO;
import cn.shoptnt.model.trade.order.dos.OrderMetaDO;
import cn.shoptnt.model.trade.order.enums.OrderMetaKeyEnum;
import cn.shoptnt.service.trade.order.OrderMetaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 订单操作SDK
 *
 * @author fk create in 2020/4/7
 * @version v2.0
 * @since v7.2.0
 */
@Service
public class OrderMetaClientDefaultImpl implements OrderMetaClient {

    @Autowired
    private OrderMetaManager orderMetaManager;

    @Override
    public void add(OrderMetaDO orderMetaDO) {
        orderMetaManager.add(orderMetaDO);
    }

    @Override
    public List<FullDiscountGiftDO> getGiftList(String orderSn, String status) {
        return orderMetaManager.getGiftList(orderSn, status);
    }

    @Override
    public void updateMetaStatus(String orderSn, OrderMetaKeyEnum metaKey, String status) {
        orderMetaManager.updateMetaStatus(orderSn, metaKey, status);
    }

    @Override
    public String getMetaValue(String orderSn, OrderMetaKeyEnum metaKey) {
        return orderMetaManager.getMetaValue(orderSn, metaKey);
    }
}
