/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.trade;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.shoptnt.model.security.ScanModuleDTO;
import cn.shoptnt.model.security.ScanResult;
import cn.shoptnt.model.trade.order.dos.TradeDO;

/**
 * @author zs
 * @version v1.0
 * @Description: 交易
 * @date 2021-12-21
 * @since v5.2.3
 */
public interface TradeClient {

    /**
     * 交易数据签名扫描
     * @return
     */
    ScanResult scanModule(ScanModuleDTO scanModuleDTO);

    /**
     * 交易数据重新签名
     */
    void reSign();

    /**
     * 修复交易数据
     * @param tradeId
     */
    void repair(Long tradeId);
}
