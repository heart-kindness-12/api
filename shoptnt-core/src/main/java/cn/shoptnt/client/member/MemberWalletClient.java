/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.shoptnt.model.member.dos.MemberWalletDO;
import cn.shoptnt.model.security.ScanModuleDTO;
import cn.shoptnt.model.security.ScanResult;

/**
 * 会员预存款
 *
 * @author zs
 * @version v7.0
 * @date 2021-12-21
 * @since v5.2.3
 */

public interface MemberWalletClient {
    /**
     * 根据会员id获取会员预存款信息
     *
     * @param memberId
     * @return
     */
    MemberWalletDO getByMemberId(Long memberId);

    /**
     * 会员钱包钱包签名数据扫描
     * @return
     */
    ScanResult scanModule(ScanModuleDTO scanModuleDTO);

    /**
     * 会员钱包数据重新签名
     */
    void reSign();

    /**
     * 修复预存款数据
     * @param memberWalletId
     */
    void repair(Long memberWalletId);
}
