/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.ShopNoticeLogClient;
import cn.shoptnt.model.shop.dos.ShopNoticeLogDO;
import cn.shoptnt.service.shop.ShopNoticeLogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @author fk
 * @version v2.0
 * @Description: 店铺消息
 * @date 2018/8/14 10:22
 * @since v7.0.0
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class ShopNoticeLogClientDefaultImpl implements ShopNoticeLogClient {


    @Autowired
    private ShopNoticeLogManager shopNoticeLogManager;

    @Override
    public ShopNoticeLogDO add(ShopNoticeLogDO shopNoticeLog) {
        return shopNoticeLogManager.add(shopNoticeLog);
    }
}
