/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member;

import cn.shoptnt.model.member.dos.MemberReceipt;

/**
 * 会员发票
 *
 * @author zs
 * @version v7.0
 * @date 2021-12-21
 * @since v5.2.3
 */

public interface MemberReceiptClient {
    /**
     * 获取会员发票
     *
     * @param id 会员发票主键
     * @return MemberReceipt  会员发票
     */
    MemberReceipt getModel(Long id);
}
