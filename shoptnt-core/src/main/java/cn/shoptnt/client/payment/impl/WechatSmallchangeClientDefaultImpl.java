/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.payment.impl;

import cn.shoptnt.client.payment.WechatSmallchangeClient;
import cn.shoptnt.service.payment.WechatSmallchangeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @author fk
 * @version v2.0
 * @Description: 微信零钱接口client
 * @date 2018/8/13 16:01
 * @since v7.0.0
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class WechatSmallchangeClientDefaultImpl implements WechatSmallchangeClient {

    @Autowired
    private WechatSmallchangeManager wechatSmallchangeManager;

    @Override
    public boolean autoSend(String openId, Double price, String ip, String sn) {

        return wechatSmallchangeManager.autoSend(openId, price, ip, sn);
    }
}
