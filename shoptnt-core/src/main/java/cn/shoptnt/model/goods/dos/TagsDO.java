/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 商品标签实体
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-03-28 14:49:36
 */
@TableName("es_tags")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class TagsDO implements Serializable {

    private static final long serialVersionUID = 1899720595535600L;

    /**
     * 主键
     */
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long tagId;
    /**
     * 标签名字
     */
    @Schema(name = "tag_name",description =  "标签名字")
    private String tagName;
    /**
     * 所属卖家
     */
    @Schema(name = "seller_id",description =  "所属卖家")
    private Long sellerId;
    /**
     * 关键字
     */
    @Schema(name = "mark",description =  "关键字")
    private String mark;


    public TagsDO() {
    }

    public TagsDO(String tagName, Long sellerId, String mark) {
        super();
        this.tagName = tagName;
        this.sellerId = sellerId;
        this.mark = mark;
    }


    @PrimaryKeyField
    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    public String toString() {
        return "TagsDO{" +
                "tagId=" + tagId +
                ", tagName='" + tagName + '\'' +
                ", sellerId=" + sellerId +
                ", mark='" + mark + '\'' +
                '}';
    }
}
