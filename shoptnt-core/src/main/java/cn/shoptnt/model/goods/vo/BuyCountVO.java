/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.vo;

import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 购买数量
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-07-06 上午1:22
 */
@Schema
@JsonNaming(value =  PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BuyCountVO {


    /**
     * 主键
     */
    @Id(name = "goods_id")
    @Schema(hidden = true)
    private Long goodsId;
    /**
     * 购买数量
     */
    @Column(name = "buy_count")
    @Schema(name = "buy_count", description =  "购买数量")
    private Integer buyCount;

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getBuyCount() {
        return buyCount;
    }

    public void setBuyCount(Integer buyCount) {
        this.buyCount = buyCount;
    }

    @Override
    public String toString() {
        return "BuyCountVO{" +
                "goodsId=" + goodsId +
                ", buyCount=" + buyCount +
                '}';
    }
}
