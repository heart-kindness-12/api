/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.vo;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;

/**
 * 运费模板操作类型
 *
 * @author zh
 * @version v7.0
 * @date 2019/9/16 4:17 PM
 * @since v7.0
 */

public class ShipTemplateMsg implements Serializable, DirectMessage {

    private static final long serialVersionUID = 973421070569515297L;
    /**
     * 运费模板id
     */
    private Long templateId;
    /**
     * 操作类型
     */
    private Integer operationType;

    public ShipTemplateMsg() {

    }

    public ShipTemplateMsg(Long templateId, Integer operationType) {
        this.operationType = operationType;
        this.templateId = templateId;
    }


    /**
     * 添加
     */
    public final static int ADD_OPERATION = 1;

    /**
     * 修改
     */
    public final static int UPDATE_OPERATION = 2;


    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public Integer getOperationType() {
        return operationType;
    }

    public void setOperationType(Integer operationType) {
        this.operationType = operationType;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.SHIP_TEMPLATE_CHANGE;
    }
}
