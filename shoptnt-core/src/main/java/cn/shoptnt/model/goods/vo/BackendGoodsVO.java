/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.vo;

import cn.shoptnt.model.goods.dos.GoodsGalleryDO;
import cn.shoptnt.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.List;

/**
 * 后台首页商品vo
 *
 * @author chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/7/6 上午12:07
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BackendGoodsVO implements Serializable {

    private static final long serialVersionUID = 3922135264669953741L;
    @Schema(description =  "id")
    private Long goodsId;

    @Schema(name = "category_id", description =  "分类id")
    private Long categoryId;

    @Schema(description =  "分类名称")
    private String categoryName;

    @Schema(name = "shop_cat_id", description =  "店铺分类id")
    private Long shopCatId;

    @Schema(name = "brand_id", description =  "品牌id")
    private Long brandId;

    @Schema(name = "goods_name", description =  "商品名称")
    private String goodsName;

    @Schema(name = "sn", description =  "商品编号")
    private String sn;

    @Schema(name = "price", description =  "商品价格")
    private Double price;

    @Schema(name = "cost", description =  "成本价格")
    private Double cost;

    @Schema(name = "mktprice", description =  "市场价格")
    private Double mktprice;

    @Schema(name = "weight", description =  "重量")
    private Double weight;

    @Schema(name = "goods_transfee_charge", description =  "谁承担运费0：买家承担，1：卖家承担")
    private Integer goodsTransfeeCharge;

    @Schema(name = "intro", description =  "详情")
    private String intro;

    @Schema(name = "have_spec", description =  "是否有规格0没有1有")
    private Integer haveSpec;

    @Schema(name = "quantity", description =  "库存")
    private Integer quantity;

    @Schema(name = "market_enable", description =  "是否上架，1上架 0下架")
    private Integer marketEnable;

    @Schema(name = "goods_gallery_list", description =  "商品相册")
    private List<GoodsGalleryDO> goodsGalleryList;

    @Schema(name = "page_title", description =  "seo标题")
    private String pageTitle;

    @Schema(name = "meta_keywords", description =  "seo关键字")
    private String metaKeywords;

    @Schema(name = "meta_description", description =  "seo描述")
    private String metaDescription;

    @Schema(name = "template_id", description =  "运费模板id,不需要运费模板时值是0")
    private Long templateId;

    @Schema(description =  "商品是否审核")
    private Integer isAuth;

    @Schema(description = "可用库存")
    private Integer enableQuantity;


    /**
     * 缩略图路径
     */
    @Schema(name = "thumbnail", description = "缩略图路径")
    private String thumbnail;
    /**
     * 大图路径
     */
    @Schema(name = "big", description =  "大图路径")
    private String big;
    /**
     * 小图路径
     */
    @Schema(name = "small", description = "小图路径")
    private String small;
    /**
     * 原图路径
     */
    @Schema(name = "original", description = "原图路径")
    private String original;

    public BackendGoodsVO() {
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getShopCatId() {
        return shopCatId;
    }

    public void setShopCatId(Long shopCatId) {
        this.shopCatId = shopCatId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getMktprice() {
        return mktprice;
    }

    public void setMktprice(Double mktprice) {
        this.mktprice = mktprice;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getGoodsTransfeeCharge() {
        return goodsTransfeeCharge;
    }

    public void setGoodsTransfeeCharge(Integer goodsTransfeeCharge) {
        this.goodsTransfeeCharge = goodsTransfeeCharge;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public Integer getHaveSpec() {
        return haveSpec;
    }

    public void setHaveSpec(Integer haveSpec) {
        this.haveSpec = haveSpec;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public List<GoodsGalleryDO> getGoodsGalleryList() {
        return goodsGalleryList;
    }

    public void setGoodsGalleryList(List<GoodsGalleryDO> goodsGalleryList) {
        this.goodsGalleryList = goodsGalleryList;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public Integer getIsAuth() {
        return isAuth;
    }

    public void setIsAuth(Integer isAuth) {
        this.isAuth = isAuth;
    }

    public Integer getMarketEnable() {
        return marketEnable;
    }

    public void setMarketEnable(Integer marketEnable) {
        this.marketEnable = marketEnable;
    }

    public Integer getEnableQuantity() {
        return enableQuantity;
    }

    public void setEnableQuantity(Integer enableQuantity) {
        this.enableQuantity = enableQuantity;

    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getBig() {
        return big;
    }

    public void setBig(String big) {
        this.big = big;
    }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    @Override
    public String toString() {
        return "GoodsVO{" +
                "goodsId=" + goodsId +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", shopCatId=" + shopCatId +
                ", brandId=" + brandId +
                ", goodsName='" + goodsName + '\'' +
                ", sn='" + sn + '\'' +
                ", price=" + price +
                ", cost=" + cost +
                ", mktprice=" + mktprice +
                ", weight=" + weight +
                ", goodsTransfeeCharge=" + goodsTransfeeCharge +
                ", intro='" + intro + '\'' +
                ", haveSpec=" + haveSpec +
                ", quantity=" + quantity +
                ", marketEnable=" + marketEnable +
                ", goodsGalleryList=" + goodsGalleryList +
                ", pageTitle='" + pageTitle + '\'' +
                ", metaKeywords='" + metaKeywords + '\'' +
                ", metaDescription='" + metaDescription + '\'' +
                ", templateId=" + templateId +
                ", isAuth=" + isAuth +
                ", enableQuantity=" + enableQuantity +
                '}';
    }
}


