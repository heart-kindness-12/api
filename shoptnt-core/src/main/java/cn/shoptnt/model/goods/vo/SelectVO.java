/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.vo;

import cn.shoptnt.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 用于select标签使用
 *
 * @author fk
 * @version v1.0
 * 2017年5月10日 下午7:37:32
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SelectVO {

    @Schema(description = "选择器id")
    private Long id;

    @Schema(description = "选择器文本")
    private String text;

    @Schema(description = "是否选中，选中true，未选中false")
    private Boolean selected;

    @Schema(description = "选择器文本的ID")
    private String falgid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFalgid() {
        return falgid;
    }

    public void setFalgid(String falgid) {
        this.falgid = falgid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


    @Override
    public String toString() {
        return "SelectVO{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", selected=" + selected +
                '}';
    }
}
