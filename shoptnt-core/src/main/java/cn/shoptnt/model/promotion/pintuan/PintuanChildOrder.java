/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.pintuan;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


/**
 * 拼团子订单实体
 *
 * @author admin
 * @version vv1.0.0
 * @since vv7.1.0
 * 2019-01-25 15:13:30
 */
@TableName("es_pintuan_child_order")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PintuanChildOrder implements Serializable {

    private static final long serialVersionUID = 2898134410030384L;

    /**
     * 子订单id
     */
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long childOrderId;
    /**
     * 订单编号
     */
    @Column(name = "order_sn")
    @NotEmpty(message = "订单编号不能为空")
    @Schema(name = "order_sn", description =  "订单编号", required = true)
    private String orderSn;
    /**
     * 会员id
     */
    @Column(name = "member_id")
    @Schema(name = "member_id", description =  "会员id")
    private Long memberId;
    /**
     * 会员id
     */
    @Column(name = "sku_id")
    @Schema(name = "sku_id", description = "会员id")
    private Long skuId;
    /**
     * 拼团活动id
     */
    @Column(name = "pintuan_id")
    @Schema(name = "pintuan_id", description = "拼团活动id")
    private Long pintuanId;
    /**
     * 订单状态
     * wait 新订单等待付款
     * pay_off 已支付等待成团
     * formed 成团
     */
    @Column(name = "order_status")
    @Schema(name = "order_status", description = "订单状态")
    private String orderStatus;
    /**
     * 主订单id
     */
    @Column(name = "order_id")
    @Schema(name = "order_id", description = "主订单id")
    private Long orderId;
    /**
     * 买家名称
     */
    @Column(name = "member_name")
    @Schema(name = "member_name", description = "买家名称")
    private String memberName;

    @Column(name = "origin_price")
    @Min(message = "必须为数字", value = 0)
    @Schema(name = "origin_price", description = "原价")
    private Double originPrice;
    /**
     * 拼团价
     */
    @Column(name = "sales_price")
    @Min(message = "必须为数字", value = 0)
    @Schema(name = "sales_price", description = "拼团价")
    private Double salesPrice;


    @PrimaryKeyField
    public Long getChildOrderId() {
        return childOrderId;
    }

    public void setChildOrderId(Long childOrderId) {
        this.childOrderId = childOrderId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getPintuanId() {
        return pintuanId;
    }

    public void setPintuanId(Long pintuanId) {
        this.pintuanId = pintuanId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Double getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(Double originPrice) {
        this.originPrice = originPrice;
    }

    public Double getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(Double salesPrice) {
        this.salesPrice = salesPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PintuanChildOrder that = (PintuanChildOrder) o;

        return new EqualsBuilder()
                .append(getChildOrderId(), that.getChildOrderId())
                .append(getOrderSn(), that.getOrderSn())
                .append(getMemberId(), that.getMemberId())
                .append(getSkuId(), that.getSkuId())
                .append(getPintuanId(), that.getPintuanId())
                .append(getOrderStatus(), that.getOrderStatus())
                .append(getOrderId(), that.getOrderId())
                .append(getMemberName(), that.getMemberName())
                .append(getOriginPrice(), that.getOriginPrice())
                .append(getSalesPrice(), that.getSalesPrice())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getChildOrderId())
                .append(getOrderSn())
                .append(getMemberId())
                .append(getSkuId())
                .append(getPintuanId())
                .append(getOrderStatus())
                .append(getOrderId())
                .append(getMemberName())
                .append(getOriginPrice())
                .append(getSalesPrice())
                .toHashCode();
    }

    @Override
    public String toString() {
        return "PintuanChildOrder{" +
                "childOrderId=" + childOrderId +
                ", orderSn='" + orderSn + '\'' +
                ", memberId=" + memberId +
                ", skuId=" + skuId +
                ", pintuanId=" + pintuanId +
                ", orderStatus='" + orderStatus + '\'' +
                ", orderId=" + orderId +
                ", memberName='" + memberName + '\'' +
                ", originPrice=" + originPrice +
                ", salesPrice=" + salesPrice +
                '}';
    }


}
