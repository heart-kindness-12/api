package cn.shoptnt.model.promotion.sign.dos;

import cn.shoptnt.framework.database.annotation.Column;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author zh
 * @version 1.0
 * @title SignInRecord
 * @description 签到记录
 * @program: api
 * 2024/3/12 11:20
 */
@TableName("es_sign_in_record")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SignInRecord implements Serializable {
    private static final long serialVersionUID = 3395914319681631510L;


    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;
    @Column(name = "member_id")
    @Schema(description =  "会员id")
    private Long memberId;
    @Column(name = "member_name")
    @Schema(description =  "会员用户名")
    private String memberName;

    @Column(name = "sign_activity_id")
    @Schema(description =  "活动id")
    private Long signActivityId;

    @Column(name = "sign_in_date")
    @Schema(description = "签到日期")
    private Long signInDate;

    /**
     * @see cn.shoptnt.model.promotion.sign.enums.SignInPrizeType
     */
    @Column(name = "prize_type")
    @Schema(description = "奖品类型")
    private String prizeType;

    @Column(name = "prize")
    @Schema(description = "奖品")
    private Long prize;

    @Column(name = "description")
    @Schema(description = "描述")
    private String description;


    @Column(name = "create_time")
    @Schema(description = "创建时间")
    private Long createTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getSignActivityId() {
        return signActivityId;
    }

    public void setSignActivityId(Long signActivityId) {
        this.signActivityId = signActivityId;
    }

    public Long getSignInDate() {
        return signInDate;
    }

    public void setSignInDate(Long signInDate) {
        this.signInDate = signInDate;
    }

    public String getPrizeType() {
        return prizeType;
    }

    public void setPrizeType(String prizeType) {
        this.prizeType = prizeType;
    }

    public Long getPrize() {
        return prize;
    }

    public void setPrize(Long prize) {
        this.prize = prize;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    @Override
    public String toString() {
        return "SignInRecord{" +
                "id=" + id +
                ", memberId=" + memberId +
                ", memberName='" + memberName + '\'' +
                ", signActivityId=" + signActivityId +
                ", signInDate=" + signInDate +
                ", prizeType='" + prizeType + '\'' +
                ", prize=" + prize +
                ", description='" + description + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
