package cn.shoptnt.model.promotion.tool;

/**
 * 满优惠活动常量
 * @author zs
 * @version v7.2.2
 * @since v7.0.0
 * 2021-12-16
 */
public class FullDiscountConstant {
    /**
     * 如果活动送赠品，结算页显示给用户的提示
     */
    public static final String GIVE_GIFT_TIP = "送赠品";
}
