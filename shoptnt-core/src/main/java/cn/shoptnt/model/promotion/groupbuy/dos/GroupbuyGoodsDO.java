/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.groupbuy.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.*;
import cn.shoptnt.framework.validation.annotation.RichTextSafeDomain;
import cn.shoptnt.framework.validation.annotation.SafeDomain;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 团购商品实体
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-02 16:57:26
 */
@TableName(value = "es_groupbuy_goods")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GroupbuyGoodsDO implements Serializable {

    private static final long serialVersionUID = 870913884116003L;

    /**团购商品Id*/
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long gbId;

    /**货品Id*/
    @Schema(name="sku_id",description = "货品Id")
    private Long skuId;

    /**规格组合*/
    @Schema(name="specs",description = "规格组合")
    @JsonRawValue
    private String specs;

    /**活动Id*/
    @Schema(name="act_id",description = "活动Id")
    private Long actId;

    /**cat_id*/
    @NotNull(message = "请选择团购分类")
    @Schema(name="cat_id",description = "分类id")
    private Long catId;

    /**团购名称*/
    @NotEmpty(message = "请填写团购名称")
    @Schema(name="gb_name",description = "团购名称")
    private String gbName;

    /**副标题*/
    @Schema(name="gb_title",description = "副标题")
    private String gbTitle;

    /**商品名称*/
    @NotEmpty(message = "请选择商品")
    @Schema(name="goods_name",description = "商品名称")
    private String goodsName;

    /**商品Id*/
    @NotNull(message = "请选择商品")
    @Schema(name="goods_id",description="商品Id")
    private Long goodsId;

    /**原始价格*/
    @NotNull(message = "请选择商品")
    @Schema(name="original_price",description="原始价格")
    private Double originalPrice;

    /**团购价格*/
    @NotNull(message = "请输入团购价格")
    @Schema(name="price",description="团购价格")
    private Double price;

    /**图片地址*/
    @SafeDomain
    @NotEmpty(message = "请上传团购图片")
    @Schema(name="img_url",description="图片地址")
    private String imgUrl;

    /**商品总数*/
    @NotNull(message = "请输入商品总数")
    @Schema(name="goods_num",description="商品总数")
    private Integer goodsNum;

    /**虚拟数量*/
    @NotNull(message = "请输入虚拟数量")
    @Schema(name="visual_num",description="虚拟数量")
    private Integer visualNum;

    /**限购数量*/
    @NotNull(message = "请输入限购数量")
    @Schema(name="limit_num",description = "限购数量")
    private Integer limitNum;

    /**已团购数量*/
    @Schema(name="buy_num",description = "已团购数量")
    private Integer buyNum;

    /**浏览数量*/
    @Schema(name="view_num",description = "浏览数量")
    private Integer viewNum;

    /**介绍*/
    @RichTextSafeDomain
    @Schema(name="remark",description = "介绍")
    private String remark;

    /**状态*/
    @Schema(name="gb_status",description = "状态")
    private Integer gbStatus;

    /**添加时间*/
    @Schema(name="add_time",description = "添加时间")
    private Long addTime;

    /**wap缩略图*/
    @Schema(name="wap_thumbnail",description = "wap缩略图")
    private String wapThumbnail;

    /**pc缩略图*/
    @Schema(name="thumbnail",description = "pc缩略图")
    private String thumbnail;

    /**商家ID*/
    @Schema(name="seller_id",description = "商家id")
    private Long sellerId;

    /**地区Id*/
    @Schema(name="area_id",description = "地区Id")
    private Integer areaId;

    /**商家名称*/
    @Schema(name="seller_name",description = "商家名称")
    private String sellerName;

    @PrimaryKeyField
    public Long getGbId() {
        return gbId;
    }
    public void setGbId(Long gbId) {
        this.gbId = gbId;
    }

    public Long getSkuId() {
        return skuId;
    }
    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getActId() {
        return actId;
    }
    public void setActId(Long actId) {
        this.actId = actId;
    }

    public Long getCatId() {
        return catId;
    }
    public void setCatId(Long catId) {
        this.catId = catId;
    }

    public Integer getAreaId() {
        return areaId;
    }
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getGbName() {
        return gbName;
    }
    public void setGbName(String gbName) {
        this.gbName = gbName;
    }

    public String getGbTitle() {
        return gbTitle;
    }
    public void setGbTitle(String gbTitle) {
        this.gbTitle = gbTitle;
    }

    public String getGoodsName() {
        return goodsName;
    }
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Long getGoodsId() {
        return goodsId;
    }
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Double getOriginalPrice() {
        return originalPrice;
    }
    public void setOriginalPrice(Double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImgUrl() {
        return imgUrl;
    }
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }
    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public Integer getVisualNum() {
        return visualNum;
    }
    public void setVisualNum(Integer visualNum) {
        this.visualNum = visualNum;
    }

    public Integer getLimitNum() {
        return limitNum;
    }
    public void setLimitNum(Integer limitNum) {
        this.limitNum = limitNum;
    }

    public String getSpecs() {
        return specs;
    }

    public void setSpecs(String specs) {
        this.specs = specs;
    }

    public Integer getBuyNum() {
        if(buyNum == null){
            buyNum = 0;
        }
        return buyNum;
    }

    /**
     * 获取显示对购买数量=真实购买数量+虚拟购买数量
     * @return
     */
    @NotDbField
    public Integer getShowBuyNum(){
        if(buyNum == null){
            buyNum = 0;
        }
        if(visualNum==null){
            visualNum=0;
        }
        return buyNum+visualNum;
    }

    public void setBuyNum(Integer buyNum) {
        this.buyNum = buyNum;
    }

    public Integer getViewNum() {
        return viewNum;
    }
    public void setViewNum(Integer viewNum) {
        this.viewNum = viewNum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getGbStatus() {
        return gbStatus;
    }
    public void setGbStatus(Integer gbStatus) {
        this.gbStatus = gbStatus;
    }

    public Long getAddTime() {
        return addTime;
    }
    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }

    public String getWapThumbnail() {
        return wapThumbnail;
    }
    public void setWapThumbnail(String wapThumbnail) {
        this.wapThumbnail = wapThumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    @Override
    public String toString() {
        return "GroupbuyGoodsDO{" +
                "gbId=" + gbId +
                ", skuId=" + skuId +
                ", actId=" + actId +
                ", catId=" + catId +
                ", gbName='" + gbName + '\'' +
                ", gbTitle='" + gbTitle + '\'' +
                ", goodsName='" + goodsName + '\'' +
                ", goodsId=" + goodsId +
                ", originalPrice=" + originalPrice +
                ", price=" + price +
                ", imgUrl='" + imgUrl + '\'' +
                ", goodsNum=" + goodsNum +
                ", visualNum=" + visualNum +
                ", limitNum=" + limitNum +
                ", buyNum=" + buyNum +
                ", viewNum=" + viewNum +
                ", remark='" + remark + '\'' +
                ", gbStatus=" + gbStatus +
                ", addTime=" + addTime +
                ", wapThumbnail='" + wapThumbnail + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", sellerId=" + sellerId +
                ", areaId=" + areaId +
                ", sellerName='" + sellerName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }

        if (o == null || getClass() != o.getClass()){
            return false;
        }

        GroupbuyGoodsDO goodsDO = (GroupbuyGoodsDO) o;

        return new EqualsBuilder()
                .append(gbId, goodsDO.gbId)
                .append(skuId, goodsDO.skuId)
                .append(actId, goodsDO.actId)
                .append(catId, goodsDO.catId)
                .append(gbName, goodsDO.gbName)
                .append(gbTitle, goodsDO.gbTitle)
                .append(goodsName, goodsDO.goodsName)
                .append(goodsId, goodsDO.goodsId)
                .append(originalPrice, goodsDO.originalPrice)
                .append(price, goodsDO.price)
                .append(imgUrl, goodsDO.imgUrl)
                .append(goodsNum, goodsDO.goodsNum)
                .append(visualNum, goodsDO.visualNum)
                .append(limitNum, goodsDO.limitNum)
                .append(buyNum, goodsDO.buyNum)
                .append(viewNum, goodsDO.viewNum)
                .append(remark, goodsDO.remark)
                .append(gbStatus, goodsDO.gbStatus)
                .append(addTime, goodsDO.addTime)
                .append(wapThumbnail, goodsDO.wapThumbnail)
                .append(thumbnail, goodsDO.thumbnail)
                .append(sellerId, goodsDO.sellerId)
                .append(areaId, goodsDO.areaId)
                .append(sellerName, goodsDO.sellerName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(gbId)
                .append(skuId)
                .append(actId)
                .append(catId)
                .append(gbName)
                .append(gbTitle)
                .append(goodsName)
                .append(goodsId)
                .append(originalPrice)
                .append(price)
                .append(imgUrl)
                .append(goodsNum)
                .append(visualNum)
                .append(limitNum)
                .append(buyNum)
                .append(viewNum)
                .append(remark)
                .append(gbStatus)
                .append(addTime)
                .append(wapThumbnail)
                .append(thumbnail)
                .append(sellerId)
                .append(areaId)
                .append(sellerName)
                .toHashCode();
    }
}
