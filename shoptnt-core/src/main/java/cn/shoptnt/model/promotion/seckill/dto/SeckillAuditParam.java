/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.seckill.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Arrays;
import java.util.Objects;

/**
 * 限时抢购审核商品参数实体
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 */
public class SeckillAuditParam {

    @Schema(name = "apply_ids", description = "限时抢购申请ID组", required = true)
    private Long[] applyIds;

    @Schema(name = "status", description = "审核状态 PASS为通过, FAIL为不通过", required = true, allowableValues = "PASS,FAIL")
    private String status;

    @Schema(name = "fail_reason",	description = "驳回原因")
    private String failReason;

    public Long[] getApplyIds() {
        return applyIds;
    }

    public void setApplyIds(Long[] applyIds) {
        this.applyIds = applyIds;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFailReason() {
        return failReason;
    }

    public void setFailReason(String failReason) {
        this.failReason = failReason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SeckillAuditParam that = (SeckillAuditParam) o;
        return Arrays.equals(applyIds, that.applyIds) &&
                Objects.equals(status, that.status) &&
                Objects.equals(failReason, that.failReason);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(status, failReason);
        result = 31 * result + Arrays.hashCode(applyIds);
        return result;
    }

    @Override
    public String toString() {
        return "SeckillAuditParam{" +
                "applyIds=" + Arrays.toString(applyIds) +
                ", status='" + status + '\'' +
                ", failReason='" + failReason + '\'' +
                '}';
    }
}
