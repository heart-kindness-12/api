package cn.shoptnt.model.promotion.sign.dos;

import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.model.promotion.sign.enums.SignInActivityStatus;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author zh
 * @version 1.0
 * @title signInActivity
 * @description 签到活动
 * @program: api
 * 2024/3/12 11:15
 */
@TableName("es_sign_in_activity")
@Schema
@JsonNaming(value =  PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SignInActivity implements Serializable {
    private static final long serialVersionUID = -1372230197143398079L;


    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long id;
    @Column(name = "title")
    @Schema(description = "活动标题",required=true)
    private String title;
    @Column(name = "start_date")
    @Schema(description = "开始日期",required=true)
    private Long startDate;

    @Column(name = "end_date")
    @Schema(description = "结束日期",required=true)
    private Long endDate;

    /**
     * @see  SignInActivityStatus
     */
    @Column(name = "status")
    @Schema(description = "状态",required=true)
    private String status;
    @Column(name = "recommended_product_id")
    @Schema(description = "推荐产品ID",required=true)
    private String recommendedProductId;

    @Column(name = "create_time")
    @Schema(description = "创建时间",required=true)
    private Long createTime;


    @Schema(description =  "活动说明", required = true)
    @Column(name = "description")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRecommendedProductId() {
        return recommendedProductId;
    }

    public void setRecommendedProductId(String recommendedProductId) {
        this.recommendedProductId = recommendedProductId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "SignInActivity{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", status='" + status + '\'' +
                ", recommendedProductId='" + recommendedProductId + '\'' +
                ", createTime=" + createTime +
                ", description='" + description + '\'' +
                '}';
    }
}
