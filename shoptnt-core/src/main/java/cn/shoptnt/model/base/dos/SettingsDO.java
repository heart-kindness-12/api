/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.NotEmpty;


/**
 * 系统设置实体
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-27 18:47:17
 */
@TableName("es_settings")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SettingsDO implements Serializable {

	private static final long serialVersionUID = 3372606354638487L;

	/**系统设置id*/
	@TableId(type= IdType.ASSIGN_ID)
	@Schema(hidden=true)
	private Long id;
	/**系统配置信息*/
	@NotEmpty(message="系统配置信息不能为空")
	@Schema(name="cfg_value",description = "系统配置信息",required=true)
	private String cfgValue;
	/**业务设置标识*/
	@NotEmpty(message="业务设置标识不能为空")
	@Schema(name="cfg_group",description = "业务设置标识",required=true)
	private String cfgGroup;



	@Override
	public String toString() {
		return "SettingsDO [id=" + id + ", cfgValue=" + cfgValue + ", cfgGroup=" + cfgGroup + "]";
	}
	@PrimaryKeyField
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getCfgValue() {
		return cfgValue;
	}
	public void setCfgValue(String cfgValue) {
		this.cfgValue = cfgValue;
	}

	public String getCfgGroup() {
		return cfgGroup;
	}
	public void setCfgGroup(String cfgGroup) {
		this.cfgGroup = cfgGroup;
	}



}
