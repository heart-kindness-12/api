package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;

/**
 * 索引生成消息
 * @author zs
 * @since 2024-03-08
 **/
public class IndexCreateMessage implements Serializable, DirectMessage {

    private static final long serialVersionUID = -36220565893377321L;


    public IndexCreateMessage() {

    }

    @Override
    public String getExchange() {
        return AmqpExchange.INDEX_CREATE;
    }
}
