package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;

/**
 * 拼团成功消息
 *
 * @author zs
 * @since 2024-03-08
 **/
public class PinTuanSuccessMessage implements Serializable, DirectMessage {

    private static final long serialVersionUID = -53563292893377321L;
    private Long pinTuanOrderId;

    public Long getPinTuanOrderId() {
        return pinTuanOrderId;
    }

    public PinTuanSuccessMessage(Long pinTuanOrderId) {
        this.pinTuanOrderId = pinTuanOrderId;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.PINTUAN_SUCCESS;
    }
}
