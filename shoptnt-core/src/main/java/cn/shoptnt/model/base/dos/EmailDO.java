/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.Min;
import javax.validation.constraints.Email;


/**
 * 邮件记录实体
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-26 16:22:11
 */
@TableName("es_email")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class EmailDO implements Serializable {

    private static final long serialVersionUID = 5515821199866313L;

    /**邮件记录id*/
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long id;
    /**邮件标题*/
    @Schema(name="title",description = "邮件标题")
    private String title;
    /**邮件类型*/
    @Schema(name="type",description = "邮件类型")
    private String type;
    /**是否成功*/
    @Min(message="必须为数字", value = 0)
    @Schema(name="success",description = "是否成功")
    private Integer success;
    /**邮件接收者*/
    @Email(message="格式不正确")
    @Schema(name="email",description = "邮件接收者")
    private String email;
    /**邮件内容*/
    @Schema(name="content",description = "邮件内容")
    private String content;
    /**错误次数*/
    @Min(message="必须为数字", value = 0)
    @Schema(name="error_num",description = "错误次数")
    private Integer errorNum;
    /**最后发送时间*/
    @Schema(name="last_send",description = "最后发送时间")
    private Long lastSend;


    @Override
	public String toString() {
		return "EmailDO [id=" + id + ", title=" + title + ", type=" + type + ", success=" + success + ", email=" + email
				+ ", context=" + content + ", errorNum=" + errorNum + ", lastSend=" + lastSend + "]";
	}
	@PrimaryKeyField
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public Integer getSuccess() {
        return success;
    }
    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public Integer getErrorNum() {
        return errorNum;
    }
    public void setErrorNum(Integer errorNum) {
        this.errorNum = errorNum;
    }

    public Long getLastSend() {
        return lastSend;
    }
    public void setLastSend(Long lastSend) {
        this.lastSend = lastSend;
    }



}
