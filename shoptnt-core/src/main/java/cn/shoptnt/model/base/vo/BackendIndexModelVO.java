/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base.vo;

import cn.shoptnt.model.goods.vo.BackendGoodsVO;
import cn.shoptnt.model.member.vo.BackendMemberVO;
import cn.shoptnt.model.statistics.vo.SalesTotal;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.List;

/**
 * 后台首页模型
 *
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018-06-29 上午9:03
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BackendIndexModelVO {

    private SalesTotal salesTotal;

    private List<BackendGoodsVO> goodsVos;

    private List<BackendMemberVO> memberVos;

    public SalesTotal getSalesTotal() {
        return salesTotal;
    }

    public void setSalesTotal(SalesTotal salesTotal) {
        this.salesTotal = salesTotal;
    }


    public List<BackendGoodsVO> getGoodsVos() {
        return goodsVos;
    }

    public void setGoodsVos(List<BackendGoodsVO> goodsVos) {
        this.goodsVos = goodsVos;
    }

    public List<BackendMemberVO> getMemberVos() {
        return memberVos;
    }

    public void setMemberVos(List<BackendMemberVO> memberVos) {
        this.memberVos = memberVos;
    }

    @Override
    public String toString() {
        return "BackendIndexModelVO{" +
                "salesTotal=" + salesTotal +
                ", goodsVos=" + goodsVos +
                ", memberVos=" + memberVos +
                '}';
    }
}
