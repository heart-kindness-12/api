/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.distribution.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * 提现申请
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018/5/21 下午2:37
 * @Description:
 *
 */
@TableName("es_withdraw_apply")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class WithdrawApplyDO implements Serializable {
	/**id**/
	@TableId(type= IdType.ASSIGN_ID)
	@Schema(hidden=true)
	private Long id;
	/**提现金额**/
	@Schema(description="提现金额")
	private Double applyMoney;
	/**提现状态**/
	@Schema(description="提现状态")
	private String status;
	/**会员id**/
	@Schema(description="会员id")
	private Long memberId;
	/**会员名字**/
	@Schema(description="会员名")
	private String memberName;
	/**申请备注**/
	@Schema(description="申请备注")
	private String applyRemark;
	/**审核备注**/
	@Schema(description="审核备注")
	private String inspectRemark;
	/**转账备注**/
	@Schema(description="转账备注")
	private String transferRemark;
	/**申请时间**/
	@Schema(description="申请时间")
	private Long applyTime;
	/**审核时间**/
	@Schema(description="审核时间")
	private Long inspectTime;
	/**转账时间**/
	@Schema(description="转账时间")
	private Long transferTime;

	@Schema(description="sn")
	private String sn;

	@Schema(description="ip")
	private String ip;

	public WithdrawApplyDO(){}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getApplyMoney() {
		return applyMoney;
	}

	public void setApplyMoney(Double applyMoney) {
		this.applyMoney = applyMoney;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getApplyRemark() {
		return applyRemark;
	}

	public void setApplyRemark(String applyRemark) {
		this.applyRemark = applyRemark;
	}

	public String getInspectRemark() {
		return inspectRemark;
	}

	public void setInspectRemark(String inspectRemark) {
		this.inspectRemark = inspectRemark;
	}

	public String getTransferRemark() {
		return transferRemark;
	}

	public void setTransferRemark(String transferRemark) {
		this.transferRemark = transferRemark;
	}

	public Long getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Long applyTime) {
		this.applyTime = applyTime;
	}

	public Long getInspectTime() {
		return inspectTime;
	}

	public void setInspectTime(Long inspectTime) {
		this.inspectTime = inspectTime;
	}

	public Long getTransferTime() {
		return transferTime;
	}

	public void setTransferTime(Long transferTime) {
		this.transferTime = transferTime;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	@Override
	public String toString() {
		return "WithdrawApplyDO{" +
				"id=" + id +
				", applyMoney=" + applyMoney +
				", status='" + status + '\'' +
				", memberId=" + memberId +
				", memberName='" + memberName + '\'' +
				", applyRemark='" + applyRemark + '\'' +
				", inspectRemark='" + inspectRemark + '\'' +
				", transferRemark='" + transferRemark + '\'' +
				", applyTime=" + applyTime +
				", inspectTime=" + inspectTime +
				", transferTime=" + transferTime +
				'}';
	}
}

