/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * app推送设置
 *
 * @author zh
 * @version v7.0
 * @date 18/6/6 下午4:36
 * @since v7.0
 */
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AppPushSetting {

    @Schema(name = "android_push_secret", description =  "Android Secret")
    private String androidPushSecret;
    @Schema(name = "android_goods_activity", description =  "Android Activity")
    private String androidGoodsActivity;
    @Schema(name = "android_push_key", description =  "Android Key")
    private String androidPushKey;
    @Schema(name = "ios_push_secret", description =  "IOS secret")
    private String iosPushSecret;
    @Schema(name = "ios_push_key", description =  "IOS key")
    private String iosPushKey;

    public String getAndroidPushSecret() {
        return androidPushSecret;
    }

    public void setAndroidPushSecret(String androidPushSecret) {
        this.androidPushSecret = androidPushSecret;
    }

    public String getAndroidGoodsActivity() {
        return androidGoodsActivity;
    }

    public void setAndroidGoodsActivity(String androidGoodsActivity) {
        this.androidGoodsActivity = androidGoodsActivity;
    }

    public String getAndroidPushKey() {
        return androidPushKey;
    }

    public void setAndroidPushKey(String androidPushKey) {
        this.androidPushKey = androidPushKey;
    }

    public String getIosPushSecret() {
        return iosPushSecret;
    }

    public void setIosPushSecret(String iosPushSecret) {
        this.iosPushSecret = iosPushSecret;
    }

    public String getIosPushKey() {
        return iosPushKey;
    }

    public void setIosPushKey(String iosPushKey) {
        this.iosPushKey = iosPushKey;
    }

    @Override
    public String toString() {
        return "AppPushSetting{" +
                "androidPushSecret='" + androidPushSecret + '\'' +
                ", androidGoodsActivity='" + androidGoodsActivity + '\'' +
                ", androidPushKey='" + androidPushKey + '\'' +
                ", iosPushSecret='" + iosPushSecret + '\'' +
                ", iosPushKey='" + iosPushKey + '\'' +
                '}';
    }
}
