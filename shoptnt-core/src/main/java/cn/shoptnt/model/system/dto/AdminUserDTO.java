/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.dto;

import cn.shoptnt.model.system.dos.AdminUser;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 管理员对象 用于管理员列表显示
 *
 * @author zh
 * @version v7.0
 * @date 18/6/27 下午2:42
 * @since v7.0
 */

public class AdminUserDTO extends AdminUser {

    @Schema(name = "role_name", description = "角色名称")
    private String roleName;

    public String getRoleName() {
        if (this.getFounder().equals(1)) {
            roleName = "超级管理员";
        }
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }


    @Override
    public String toString() {
        return "AdminUserDTO{" +
                "roleName='" + roleName + '\'' +
                '}';
    }
}
