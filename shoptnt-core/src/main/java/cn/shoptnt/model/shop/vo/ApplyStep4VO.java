/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.shop.vo;

import javax.validation.constraints.NotEmpty;

import cn.shoptnt.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;
import org.hibernate.validator.constraints.Length;

/**
 *
 * 申请开店第四部VO
 * @author zhangjiping
 * @version v1.0
 * @since v7.0
 * 2018年3月21日 下午4:08:19
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ApplyStep4VO {
	/**店铺名称*/
	@Column(name = "shop_name")
	@Schema(name="shop_name",description = "店铺名称",required=true)
	@NotEmpty(message="店铺名称必填")
	@Length(max = 15)
	private String shopName;

	/**店铺经营类目*/
	@Column(name = "goods_management_category")
	@Schema(name="goods_management_category",description = "店铺经营类目",required=true)
	@NotEmpty(message="店铺经营类目必填")
	private String goodsManagementCategory;
	/**店铺所在省id*/
	@Column(name = "shop_province_id")
	@Schema(name="shop_province_id",description = "店铺所在省id",hidden = true)
	private Long shopProvinceId;
	/**店铺所在市id*/
	@Column(name = "shop_city_id")
	@Schema(name="shop_city_id",description = "店铺所在市id",hidden = true)
	private Long shopCityId;
	/**店铺所在县id*/
	@Column(name = "shop_county_id")
	@Schema(name="shop_county_id",description = "店铺所在县id",hidden = true)
	private Long shopCountyId;
	/**店铺所在镇id*/
	@Column(name = "shop_town_id")
	@Schema(name="shop_town_id",description = "店铺所在镇id",hidden = true)
	private Long shopTownId;
	/**店铺所在省*/
	@Column(name = "shop_province")
	@Schema(name="shop_province",description = "店铺所在省",hidden = true)
	private String shopProvince;
	/**店铺所在市*/
	@Column(name = "shop_city")
	@Schema(name="shop_city",description = "店铺所在市",hidden = true)
	private String shopCity;
	/**店铺所在县*/
	@Column(name = "shop_county")
	@Schema(name="shop_county",description = "店铺所在县",hidden = true)
	private String shopCounty;
	/**店铺所在镇*/
	@Column(name = "shop_town",allowNullUpdate = true)
	@Schema(name="shop_town",description = "店铺所在镇",hidden = true)
	private String shopTown;
	/**申请开店进度*/
	@Column(name = "step")
	@Schema(name="step",description = "申请开店进度：1,2,3,4")
	private Integer step;
	/**店铺详细地址*/
	@Column(name = "shop_add",allowNullUpdate = true)
	@Schema(name="shop_add",description ="店铺详细地址",hidden = true)
	private String shopAdd;

	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getGoodsManagementCategory() {
		return goodsManagementCategory;
	}
	public void setGoodsManagementCategory(String goodsManagementCategory) {
		this.goodsManagementCategory = goodsManagementCategory;
	}
	public Long getShopProvinceId() {
		return shopProvinceId;
	}
	public void setShopProvinceId(Long shopProvinceId) {
		this.shopProvinceId = shopProvinceId;
	}
	public Long getShopCityId() {
		return shopCityId;
	}
	public void setShopCityId(Long shopCityId) {
		this.shopCityId = shopCityId;
	}
	public Long getShopCountyId() {
		return shopCountyId;
	}
	public void setShopCountyId(Long shopCountyId) {
		this.shopCountyId = shopCountyId;
	}
	public Long getShopTownId() {
		return shopTownId;
	}
	public void setShopTownId(Long shopTownId) {
		this.shopTownId = shopTownId;
	}
	public String getShopProvince() {
		return shopProvince;
	}
	public void setShopProvince(String shopProvince) {
		this.shopProvince = shopProvince;
	}
	public String getShopCity() {
		return shopCity;
	}
	public void setShopCity(String shopCity) {
		this.shopCity = shopCity;
	}
	public String getShopCounty() {
		return shopCounty;
	}
	public void setShopCounty(String shopCounty) {
		this.shopCounty = shopCounty;
	}
	public String getShopTown() {
		return shopTown;
	}
	public void setShopTown(String shopTown) {
		this.shopTown = shopTown;
	}

	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

	public String getShopAdd() {
		return shopAdd;
	}

	public void setShopAdd(String shopAdd) {
		this.shopAdd = shopAdd;
	}

	@Override
	public String toString() {
		return "ApplyStep4VO{" +
				"shopName='" + shopName + '\'' +
				", goodsManagementCategory='" + goodsManagementCategory + '\'' +
				", shopProvinceId=" + shopProvinceId +
				", shopCityId=" + shopCityId +
				", shopCountyId=" + shopCountyId +
				", shopTownId=" + shopTownId +
				", shopProvince='" + shopProvince + '\'' +
				", shopCity='" + shopCity + '\'' +
				", shopCounty='" + shopCounty + '\'' +
				", shopTown='" + shopTown + '\'' +
				", step=" + step +
				", shopAdd='" + shopAdd + '\'' +
				'}';
	}
}
