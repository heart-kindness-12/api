/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.shop.vo;

import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.validation.annotation.SafeDomain;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;

/**
 *
 * 申请开店第三步VO
 * @author zhangjiping
 * @version v1.0
 * @since v7.0
 * 2018年3月21日 下午4:01:02
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ApplyStep3VO {
	/**银行开户名*/
	@Column(name = "bank_account_name")
	@Schema(name="bank_account_name",description ="银行开户名",required=true)
	@NotEmpty(message="银行开户名必填")
	private String bankAccountName;
	/**银行开户账号*/
	@Column(name = "bank_number")
	@Schema(name="bank_number",description ="银行开户账号",required=true)
	@NotEmpty(message="银行开户账号必填")
	private String bankNumber;
	/**开户银行支行名称*/
	@Column(name = "bank_name")
	@Schema(name="bank_name",description ="开户银行支行名称",required=true)
	@NotEmpty(message="开户银行支行名称必填")
	private String bankName;
	/**开户银行所在省id*/
	@Column(name = "bank_province_id")
	@Schema(name="bank_province_id",description ="开户银行所在省id",hidden = true)
	private Long bankProvinceId;
	/**开户银行所在市id*/
	@Column(name = "bank_city_id")
	@Schema(name="bank_city_id",description ="开户银行所在市id",hidden = true)
	private Long bankCityId;
	/**开户银行所在县id*/
	@Column(name = "bank_county_id")
	@Schema(name="bank_county_id",description ="开户银行所在县id",hidden = true)
	private Long bankCountyId;
	/**开户银行所在镇id*/
	@Column(name = "bank_town_id")
	@Schema(name="bank_town_id",description ="开户银行所在镇id",hidden = true)
	private Long bankTownId;
	/**开户银行所在省*/
	@Column(name = "bank_province")
	@Schema(name="bank_province",description ="开户银行所在省",hidden = true)
	private String bankProvince;
	/**开户银行所在市*/
	@Column(name = "bank_city")
	@Schema(name="bank_city",description ="开户银行所在市",hidden = true)
	private String bankCity;
	/**开户银行所在县*/
	@Column(name = "bank_county")
	@Schema(name="bank_county",description ="开户银行所在县",hidden = true)
	private String bankCounty;
	/**开户银行所在镇*/
	@Column(name = "bank_town",allowNullUpdate = true)
	@Schema(name="bank_town",description ="开户银行所在镇",hidden = true)
	private String bankTown;
	/**开户银行许可证电子版*/
	@SafeDomain
	@Column(name = "bank_img")
	@Schema(name="bank_img",description ="开户银行许可证电子版",required=true)
	private String bankImg;
	/**税务登记证号*/
	@Column(name = "taxes_certificate_num")
	@Schema(name="taxes_certificate_num",description ="税务登记证号")
	private String taxesCertificateNum;
	/**纳税人识别号*/
	@Column(name = "taxes_distinguish_num")
	@Schema(name="taxes_distinguish_num",description ="纳税人识别号")
	private String taxesDistinguishNum;
	/**税务登记证书*/
	@SafeDomain
	@Column(name = "taxes_certificate_img")
	@Schema(name="taxes_certificate_img",description = "税务登记证书")
	private String taxesCertificateImg;
	/**申请开店进度*/
	@Column(name = "step")
	@Schema(name="step",description = "申请开店进度：1,2,3,4")
	private Integer step;
	public String getBankAccountName() {
		return bankAccountName;
	}
	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}
	public String getBankNumber() {
		return bankNumber;
	}
	public void setBankNumber(String bankNumber) {
		this.bankNumber = bankNumber;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public Long getBankProvinceId() {
		return bankProvinceId;
	}
	public void setBankProvinceId(Long bankProvinceId) {
		this.bankProvinceId = bankProvinceId;
	}
	public Long getBankCityId() {
		return bankCityId;
	}
	public void setBankCityId(Long bankCityId) {
		this.bankCityId = bankCityId;
	}
	public Long getBankCountyId() {
		return bankCountyId;
	}
	public void setBankCountyId(Long bankCountyId) {
		this.bankCountyId = bankCountyId;
	}
	public Long getBankTownId() {
		return bankTownId;
	}
	public void setBankTownId(Long bankTownId) {
		this.bankTownId = bankTownId;
	}
	public String getBankProvince() {
		return bankProvince;
	}
	public void setBankProvince(String bankProvince) {
		this.bankProvince = bankProvince;
	}
	public String getBankCity() {
		return bankCity;
	}
	public void setBankCity(String bankCity) {
		this.bankCity = bankCity;
	}
	public String getBankCounty() {
		return bankCounty;
	}
	public void setBankCounty(String bankCounty) {
		this.bankCounty = bankCounty;
	}
	public String getBankTown() {
		return bankTown;
	}
	public void setBankTown(String bankTown) {
		this.bankTown = bankTown;
	}
	public String getBankImg() {
		return bankImg;
	}
	public void setBankImg(String bankImg) {
		this.bankImg = bankImg;
	}
	public String getTaxesCertificateNum() {
		return taxesCertificateNum;
	}
	public void setTaxesCertificateNum(String taxesCertificateNum) {
		this.taxesCertificateNum = taxesCertificateNum;
	}
	public String getTaxesDistinguishNum() {
		return taxesDistinguishNum;
	}
	public void setTaxesDistinguishNum(String taxesDistinguishNum) {
		this.taxesDistinguishNum = taxesDistinguishNum;
	}
	public String getTaxesCertificateImg() {
		return taxesCertificateImg;
	}
	public void setTaxesCertificateImg(String taxesCertificateImg) {
		this.taxesCertificateImg = taxesCertificateImg;
	}

	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

	@Override
	public String toString() {
		return "ApplyStep3VO [bankAccountName=" + bankAccountName + ", bankNumber=" + bankNumber + ", bankName="
				+ bankName + ", bankProvinceId=" + bankProvinceId + ", bankCityId=" + bankCityId + ", bankCountyId="
				+ bankCountyId + ", bankTownId=" + bankTownId + ", bankProvince=" + bankProvince + ", bankCity="
				+ bankCity + ", bankCounty=" + bankCounty + ", bankTown=" + bankTown + ", bankImg=" + bankImg
				+ ", taxesCertificateNum=" + taxesCertificateNum + ", taxesDistinguishNum=" + taxesDistinguishNum
				+ ", taxesCertificateImg=" + taxesCertificateImg + "]";
	}
}

