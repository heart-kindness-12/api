/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.security;

/**
 * 扫描状态枚举
 * @author 妙贤
 * @version 1.0
 * @data 2021/11/22 21:29
 **/
public enum ScanSateEnum {
    /**
     * 未完成
     */
    unfinished,
    /**
     * 已完成
     */
    finished;
}
