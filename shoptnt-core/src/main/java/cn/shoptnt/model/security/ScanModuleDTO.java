/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.security;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.shoptnt.model.member.dos.Member;

import java.io.Serializable;

/**
 * 扫描数据参数
 * @author zs
 * @version 1.0
 * @data 2021-12-21
 **/
public class ScanModuleDTO<T> implements Serializable {

    private String rounds;

    private QueryWrapper<T> queryWrapper;

    private Long pageSize;

    public ScanModuleDTO(){}

    public ScanModuleDTO(String rounds, QueryWrapper<T> queryWrapper, Long pageSize){
        this.rounds = rounds;
        this.queryWrapper = queryWrapper;
        this.pageSize = pageSize;
    }

    public String getRounds() {
        return rounds;
    }

    public void setRounds(String rounds) {
        this.rounds = rounds;
    }

    public QueryWrapper<T> getQueryWrapper() {
        return queryWrapper;
    }

    public void setQueryWrapper(QueryWrapper<T> queryWrapper) {
        this.queryWrapper = queryWrapper;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }
}
