/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dos;

import java.io.Serializable;
import java.util.Objects;

import com.baomidou.mybatisplus.annotation.*;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;

import cn.shoptnt.handler.annotation.Secret;
import cn.shoptnt.handler.annotation.SecretField;
import cn.shoptnt.handler.enums.SecretType;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;
import org.hibernate.validator.constraints.Length;


/**
 * 会员发票信息缓存实体
 *
 * @author duanmingyu
 * @version v7.1.4
 * @since v7.0.0
 * 2019-06-19
 */
@TableName(value = "es_member_receipt")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Secret
public class MemberReceipt implements Serializable {

    private static final long serialVersionUID = 4743090485786622L;

    /**
     * 会员发票id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long receiptId;
    /**
     * 会员id
     */
    @Schema(name = "member_id", description =  "会员id")
    private Long memberId;
    /**
     * 发票类型 ELECTRO：电子普通发票，VATORDINARY：增值税普通发票
     */
    @Schema(name = "receipt_type", description =  "发票类型 ELECTRO：电子普通发票，VATORDINARY：增值税普通发票")
    private String receiptType;
    /**
     * 发票抬头
     */
    @Schema(name = "receipt_title", description =  "发票抬头")
    @Length(min=1,max = 60,message = "发票抬头在1到60之间")
    private String receiptTitle;
    /**
     * 发票内容
     */
    @Schema(name = "receipt_content", description =  "发票内容")
    private String receiptContent;
    /**
     * 发票税号
     */
    @Schema(name = "tax_no", description =  "发票税号")
    private String taxNo;
    /**
     * 收票人手机号
     */
    @Schema(name = "member_mobile", description =  "收票人手机号")
    @SecretField(SecretType.MOBILE)
    @TableField(updateStrategy = FieldStrategy.NOT_EMPTY)
    private String memberMobile;
    /**
     * 收票人邮箱
     */
    @Schema(name = "member_email", description =  "收票人邮箱")
    private String memberEmail;
    /**
     * 是否为默认选项 0：否，1：是
     */
    @Schema(name = "is_default", description =  "是否为默认选项 0：否，1：是")
    private Integer isDefault;

    @PrimaryKeyField
    public Long getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(Long receiptId) {
        this.receiptId = receiptId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

    public String getReceiptTitle() {
        return receiptTitle;
    }

    public void setReceiptTitle(String receiptTitle) {
        this.receiptTitle = receiptTitle;
    }

    public String getReceiptContent() {
        return receiptContent;
    }

    public void setReceiptContent(String receiptContent) {
        this.receiptContent = receiptContent;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }

    public String getMemberEmail() {
        return memberEmail;
    }

    public void setMemberEmail(String memberEmail) {
        this.memberEmail = memberEmail;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MemberReceipt that = (MemberReceipt) o;
        return Objects.equals(receiptId, that.receiptId) &&
                Objects.equals(memberId, that.memberId) &&
                Objects.equals(receiptType, that.receiptType) &&
                Objects.equals(receiptTitle, that.receiptTitle) &&
                Objects.equals(receiptContent, that.receiptContent) &&
                Objects.equals(taxNo, that.taxNo) &&
                Objects.equals(memberMobile, that.memberMobile) &&
                Objects.equals(memberEmail, that.memberEmail) &&
                Objects.equals(isDefault, that.isDefault);
    }

    @Override
    public int hashCode() {
        return Objects.hash(receiptId, memberId, receiptType, receiptTitle, receiptContent, taxNo, memberMobile, memberEmail, isDefault);
    }

    @Override
    public String toString() {
        return "MemberReceipt{" +
                "receiptId=" + receiptId +
                ", memberId=" + memberId +
                ", receiptType='" + receiptType + '\'' +
                ", receiptTitle='" + receiptTitle + '\'' +
                ", receiptContent='" + receiptContent + '\'' +
                ", taxNo='" + taxNo + '\'' +
                ", memberMobile='" + memberMobile + '\'' +
                ", memberEmail='" + memberEmail + '\'' +
                ", isDefault=" + isDefault +
                '}';
    }
}
