/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 会员开票历史记录搜索参数实体
 *
 * @author duanmingyu
 * @version v7.1.4
 * @since v7.0.0
 * 2019-06-20
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class HistoryQueryParam {

    /**
     * 搜索关键字
     */
    @Schema(name = "keyword", description = "搜索关键字")
    private String keyword;

    /**
     * 订单编号
     */
    @Schema(name = "order_sn", description = "订单编号")
    private String orderSn;

    /**
     * 商家名称
     */
    @Schema(name = "seller_name", description = "商家名称")
    private String sellerName;

    /**
     * 开票状态 0：未开，1：已开
     */
    @Schema(name = "status", description = "开票状态", example = "0：未开，1：已开", allowableValues = "0,1")
    private String status;

    /**
     * 发票类型 ELECTRO：电子普通发票，VATORDINARY：增值税普通发票，VATOSPECIAL：增值税专用发票
     */
    @Schema(name = "receipt_type", description =  "发票类型", example = "ELECTRO：电子普通发票，VATORDINARY：增值税普通发票，VATOSPECIAL：增值税专用发票", allowableValues = "ELECTRO,VATORDINARY,VATOSPECIAL")
    private String receiptType;

    /**
     * 申请开票开始时间
     */
    @Schema(name = "start_time", description = "申请开票开始时间")
    private String startTime;
    /**
     * 申请开票结束时间
     */
    @Schema(name = "end_time", description = "申请开票结束时间")
    private String endTime;

    /**
     * 商家ID
     */
    @Schema(name = "seller_id", description = "商家ID")
    private Long sellerId;

    /**
     * 会员ID
     */
    @Schema(name = "member_id", description = "会员ID")
    private Long memberId;

    /**
     * 会员用户名
     */
    @Schema(name = "uname", description = "会员用户名")
    private String uname;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    @Override
    public String toString() {
        return "HistoryQueryParam{" +
                "keyword='" + keyword + '\'' +
                ", orderSn='" + orderSn + '\'' +
                ", sellerName='" + sellerName + '\'' +
                ", status='" + status + '\'' +
                ", receiptType='" + receiptType + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", sellerId=" + sellerId +
                ", memberId=" + memberId +
                ", uname='" + uname + '\'' +
                '}';
    }
}
