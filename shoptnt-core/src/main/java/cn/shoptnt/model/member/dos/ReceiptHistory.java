/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dos;

import java.io.Serializable;
import java.util.Objects;

import com.baomidou.mybatisplus.annotation.*;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;

import cn.shoptnt.handler.annotation.Secret;
import cn.shoptnt.handler.annotation.SecretField;
import cn.shoptnt.handler.enums.SecretType;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 会员开票历史记录实体
 *
 * @author duanmingyu
 * @version v7.1.4
 * @since v7.0.0
 * 2019-06-20
 */
@TableName(value = "es_receipt_history")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Secret
public class ReceiptHistory implements Serializable {

    private static final long serialVersionUID = 7024661438767317L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long historyId;
    /**
     * 订单编号
     */
    @Schema(name = "order_sn", description =  "订单编号")
    private String orderSn;
    /**
     * 订单金额
     */
    @Schema(name = "order_price", description =  "订单金额")
    private Double orderPrice;
    /**
     * 商家ID
     */
    @Schema(name = "seller_id", description =  "商家ID")
    private Long sellerId;
    /**
     * 商家名称
     */
    @Schema(name = "seller_name", description =  "商家名称")
    private String sellerName;
    /**
     * 会员ID
     */
    @Schema(name = "member_id", description =  "会员ID")
    private Long memberId;

    /**
     * 会员名称
     */
    @Schema(name = "uname", description =  "会员名称")
    private String uname;

    /**
     * 开票状态 0：未开，1：已开
     */
    @Schema(name = "status", description =  "开票状态 0：未开，1：已开")
    private Integer status;
    /**
     * 开票方式 针对增值税专用发票，暂时只有"订单完成后开票"一种方式
     */
    @Schema(name = "receipt_method", description = "开票方式")
    private String receiptMethod;
    /**
     * 发票类型 ELECTRO：电子普通发票，VATORDINARY：增值税普通发票，VATOSPECIAL：增值税专用发票
     */
    @Schema(name = "receipt_type", description = "发票类型", example = "ELECTRO：电子普通发票，VATORDINARY：增值税普通发票，VATOSPECIAL：增值税专用发票")
    private String receiptType;
    /**
     * 物流公司ID
     */
    @Schema(name = "logi_id", description = "物流公司ID")
    private Long logiId;
    /**
     * 物流公司名称
     */
    @Schema(name = "logi_name", description = "物流公司名称")
    private String logiName;
    /**
     * 快递单号
     */
    @Schema(name = "logi_code", description =  "快递单号")
    private String logiCode;
    /**
     * 发票抬头
     */
    @Schema(name = "receipt_title", description =  "发票抬头")
    private String receiptTitle;
    /**
     * 发票内容
     */
    @Schema(name = "receipt_content", description =  "发票内容")
    private String receiptContent;
    /**
     * 纳税人识别号
     */
    @Schema(name = "tax_no", description =  "纳税人识别号")
    private String taxNo;
    /**
     * 注册地址
     */
    @Schema(name = "reg_addr", description =  "注册地址")
    private String regAddr;
    /**
     * 注册电话
     */
    @Schema(name = "reg_tel", description =  "注册电话")
    @SecretField(SecretType.MOBILE)
    private String regTel;
    /**
     * 开户银行
     */
    @Schema(name = "bank_name", description =  "开户银行")
    private String bankName;
    /**
     * 银行账户
     */
    @Schema(name = "bank_account", description = "银行账户")
    @SecretField(SecretType.BANK)
    private String bankAccount;
    /**
     * 收票人姓名
     */
    @Schema(name = "member_name", description = "收票人姓名")
    private String memberName;
    /**
     * 收票人手机号
     */
    @Schema(name = "member_mobile", description = "收票人手机号")
    @SecretField(SecretType.MOBILE)
    private String memberMobile;
    /**
     * 收票人邮箱
     */
    @Schema(name = "member_email", description = "收票人邮箱")
    private String memberEmail;
    /**
     * 收票地址--所属省份ID
     */
    @Schema(name = "province_id", description = "收票地址--所属省份ID")
    private Long provinceId;

    /**
     * 收票地址--所属城市ID
     */
    @Schema(name = "city_id", description = "收票地址--所属城市ID")
    private Long cityId;

    /**
     * 收票地址--所属区县ID
     */
    @Schema(name = "county_id", description = "收票地址--所属区县ID")
    private Long countyId;

    /**
     * 收票地址--所属乡镇ID
     */
    @Schema(name = "town_id", description = "收票地址--所属乡镇ID")
    private Long townId;

    /**
     * 收票地址--所属省份
     */
    @Schema(name = "province", description = "收票地址--所属省份")
    private String province;

    /**
     * 收票地址--所属城市
     */
    @Schema(name = "city", description = "收票地址--所属城市")
    private String city;

    /**
     * 收票地址--所属区县
     */
    @Schema(name = "county", description = "收票地址--所属区县")
    private String county;

    /**
     * 收票地址--所属乡镇
     */
    @Schema(name = "town", description = "收票地址--所属乡镇")
    private String town;

    /**
     * 收票地址--详细地址
     */
    @Schema(name = "detail_addr", description = "收票地址--详细地址")
    private String detailAddr;
    /**
     * 开票时间
     */
    @Schema(name = "add_time", description = "开票时间")
    private Long addTime;

    /**
     * 商品数据json
     */
    @Schema(name = "goods_json", description = "商品数据json")
    private String goodsJson;

    /**
     * 商品数据json
     */
    @Schema(name = "order_status", description = "订单状态，NEW或者CONFIRM，出库成功的状态才可会被下一步")
    private String orderStatus;

    @PrimaryKeyField
    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Double getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(Double orderPrice) {
        this.orderPrice = orderPrice;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getReceiptMethod() {
        return receiptMethod;
    }

    public void setReceiptMethod(String receiptMethod) {
        this.receiptMethod = receiptMethod;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

    public Long getLogiId() {
        return logiId;
    }

    public void setLogiId(Long logiId) {
        this.logiId = logiId;
    }

    public String getLogiName() {
        return logiName;
    }

    public void setLogiName(String logiName) {
        this.logiName = logiName;
    }

    public String getLogiCode() {
        return logiCode;
    }

    public void setLogiCode(String logiCode) {
        this.logiCode = logiCode;
    }

    public String getReceiptTitle() {
        return receiptTitle;
    }

    public void setReceiptTitle(String receiptTitle) {
        this.receiptTitle = receiptTitle;
    }

    public String getReceiptContent() {
        return receiptContent;
    }

    public void setReceiptContent(String receiptContent) {
        this.receiptContent = receiptContent;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getRegAddr() {
        return regAddr;
    }

    public void setRegAddr(String regAddr) {
        this.regAddr = regAddr;
    }

    public String getRegTel() {
        return regTel;
    }

    public void setRegTel(String regTel) {
        this.regTel = regTel;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }

    public String getMemberEmail() {
        return memberEmail;
    }

    public void setMemberEmail(String memberEmail) {
        this.memberEmail = memberEmail;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getCountyId() {
        return countyId;
    }

    public void setCountyId(Long countyId) {
        this.countyId = countyId;
    }

    public Long getTownId() {
        return townId;
    }

    public void setTownId(Long townId) {
        this.townId = townId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getDetailAddr() {
        return detailAddr;
    }

    public void setDetailAddr(String detailAddr) {
        this.detailAddr = detailAddr;
    }

    public Long getAddTime() {
        return addTime;
    }

    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }

    public String getGoodsJson() {
        return goodsJson;
    }

    public void setGoodsJson(String goodsJson) {
        this.goodsJson = goodsJson;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReceiptHistory that = (ReceiptHistory) o;
        return Objects.equals(historyId, that.historyId) &&
                Objects.equals(orderSn, that.orderSn) &&
                Objects.equals(orderPrice, that.orderPrice) &&
                Objects.equals(sellerId, that.sellerId) &&
                Objects.equals(sellerName, that.sellerName) &&
                Objects.equals(memberId, that.memberId) &&
                Objects.equals(status, that.status) &&
                Objects.equals(receiptMethod, that.receiptMethod) &&
                Objects.equals(receiptType, that.receiptType) &&
                Objects.equals(logiId, that.logiId) &&
                Objects.equals(logiName, that.logiName) &&
                Objects.equals(logiCode, that.logiCode) &&
                Objects.equals(receiptTitle, that.receiptTitle) &&
                Objects.equals(receiptContent, that.receiptContent) &&
                Objects.equals(taxNo, that.taxNo) &&
                Objects.equals(regAddr, that.regAddr) &&
                Objects.equals(regTel, that.regTel) &&
                Objects.equals(bankName, that.bankName) &&
                Objects.equals(bankAccount, that.bankAccount) &&
                Objects.equals(memberName, that.memberName) &&
                Objects.equals(memberMobile, that.memberMobile) &&
                Objects.equals(memberEmail, that.memberEmail) &&
                Objects.equals(provinceId, that.provinceId) &&
                Objects.equals(cityId, that.cityId) &&
                Objects.equals(countyId, that.countyId) &&
                Objects.equals(townId, that.townId) &&
                Objects.equals(province, that.province) &&
                Objects.equals(city, that.city) &&
                Objects.equals(county, that.county) &&
                Objects.equals(town, that.town) &&
                Objects.equals(detailAddr, that.detailAddr) &&
                Objects.equals(addTime, that.addTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(historyId, orderSn, orderPrice, sellerId, sellerName, memberId, status, receiptMethod, receiptType, logiId, logiName, logiCode, receiptTitle, receiptContent, taxNo, regAddr, regTel, bankName, bankAccount, memberName, memberMobile, memberEmail, provinceId, cityId, countyId, townId, province, city, county, town, detailAddr, addTime);
    }

    @Override
    public String toString() {
        return "ReceiptHistory{" +
                "historyId=" + historyId +
                ", orderSn='" + orderSn + '\'' +
                ", orderPrice=" + orderPrice +
                ", sellerId=" + sellerId +
                ", sellerName='" + sellerName + '\'' +
                ", memberId=" + memberId +
                ", status=" + status +
                ", receiptMethod='" + receiptMethod + '\'' +
                ", receiptType='" + receiptType + '\'' +
                ", logiId='" + logiId +
                ", logiName='" + logiName + '\'' +
                ", logiCode='" + logiCode + '\'' +
                ", receiptTitle='" + receiptTitle + '\'' +
                ", receiptContent='" + receiptContent + '\'' +
                ", taxNo='" + taxNo + '\'' +
                ", regAddr='" + regAddr + '\'' +
                ", regTel='" + regTel + '\'' +
                ", bankName='" + bankName + '\'' +
                ", bankAccount='" + bankAccount + '\'' +
                ", memberName='" + memberName + '\'' +
                ", memberMobile='" + memberMobile + '\'' +
                ", memberEmail='" + memberEmail + '\'' +
                ", provinceId=" + provinceId +
                ", cityId=" + cityId +
                ", countyId=" + countyId +
                ", townId=" + townId +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                ", town='" + town + '\'' +
                ", detailAddr='" + detailAddr + '\'' +
                ", addTime=" + addTime +
                '}';
    }
}
