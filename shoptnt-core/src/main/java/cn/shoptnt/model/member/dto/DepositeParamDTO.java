/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @description: 会员预存款搜索参数DTO
 * @author: liuyulei
 * @create: 2019-12-31 09:17
 * @version:1.0
 * @since:7.1.5
 **/
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class DepositeParamDTO implements Serializable {
    private static final long serialVersionUID = -477285823153620967L;

    @Schema(name = "page_no",description = "页码" ,required = true)
    private Long pageNo;

    @Schema(name = "page_size",description = "页面显示大小",required = true)
    private Long pageSize;

    @Schema(name = "member_id",description = "会员id",hidden = true)
    private Long memberId;

    @Schema(name = "member_name",description = "会员名称",hidden = true)
    private String memberName;

    @Schema(name = "sn",description = "充值编号",hidden = true)
    private String sn;

    @Schema(name = "start_time",description = "开始时间",hidden = true)
    private Long startTime;

    @Schema(name = "end_time",description = "结束时间",hidden = true)
    private Long endTime;

    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }


    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }


    @Override
    public String toString() {
        return "DepositeParamDTO{" +
                "pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", memberId=" + memberId +
                ", memberName='" + memberName + '\'' +
                ", sn='" + sn + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
