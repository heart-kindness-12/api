/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.vo;

import cn.shoptnt.model.member.dos.AskReplyDO;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 会员问题咨询回复对象vo
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-28
 */
public class AskReplyVO extends AskReplyDO {
    /**
     * 咨询内容
     */
    @Schema(name = "ask_content", description =  "咨询内容")
    private String askContent;
    /**
     * 商品id
     */
    @Schema(name = "goods_id", description =  "商品id")
    private Long goodsId;
    /**
     * 商品名称
     */
    @Schema(name = "goods_name", description =  "商品名称")
    private String goodsName;
    /**
     * 商品图片
     */
    @Schema(name = "goods_img", description =  "商品图片")
    private String goodsImg;

    public String getAskContent() {
        return askContent;
    }

    public void setAskContent(String askContent) {
        this.askContent = askContent;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    @Override
    public String toString() {
        return "AskReplyVO{" +
                "askContent='" + askContent + '\'' +
                ", goodsId=" + goodsId +
                ", goodsName='" + goodsName + '\'' +
                ", goodsImg='" + goodsImg + '\'' +
                '}';
    }
}
