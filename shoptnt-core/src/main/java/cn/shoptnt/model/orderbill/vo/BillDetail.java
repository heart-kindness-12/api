/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.orderbill.vo;

import cn.shoptnt.model.orderbill.dos.Bill;
import cn.shoptnt.model.orderbill.enums.BillStatusEnum;
import cn.shoptnt.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author fk
 * @version v1.0
 * @Description: 账单信息
 * @date 2018/4/27 14:58
 * @since v7.0.0
 */
@Table(name = "es_bill")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BillDetail extends Bill implements Serializable {


    @Schema(name = "status_text", description = "状态文本")
    private String statusText;

    @Schema(name = "operate_allowable", description = "允许的操作")
    private OperateAllowable operateAllowable;


    public OperateAllowable getOperateAllowable() {
        return operateAllowable;
    }

    public void setOperateAllowable(OperateAllowable operateAllowable) {
        this.operateAllowable = operateAllowable;
    }

    public String getStatusText() {

        return BillStatusEnum.valueOf(this.getStatus()).description();
    }
}
