/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.orderbill.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;

import cn.shoptnt.handler.annotation.Secret;
import cn.shoptnt.handler.annotation.SecretField;
import cn.shoptnt.handler.enums.SecretType;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 结算单实体
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-04-26 16:21:26
 */
@TableName("es_bill")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Secret
public class Bill implements Serializable {

    private static final long serialVersionUID = 2147807368020181L;

    /**
     * 主键ID
     */
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long billId;
    /**
     * 结算单编号
     */
    @Schema(name = "bill_sn", description = "结算单编号")
    private String billSn;
    /**
     * 结算开始时间
     */
    @Schema(name = "start_time", description = "结算开始时间")
    private Long startTime;
    /**
     * 结算结束时间
     */
    @Schema(name = "end_time", description = "结算结束时间")
    private Long endTime;
    /**
     * 结算总金额
     */
    @Schema(name = "price", description = "结算总金额")
    private Double price;
    /**
     * 佣金
     */
    @Schema(name = "commi_price", description = "佣金")
    private Double commiPrice;
    /**
     * 优惠金额
     */
    @Schema(name = "discount_price", description = "优惠金额")
    private Double discountPrice;
    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private String status;
    /**
     * 账单类型 0月结 1日结 2季度结 3自定义
     */
    @Schema(name = "bill_type", description = "账单类型")
    private Integer billType;
    /**
     * 店铺id
     */
    @Schema(name = "seller_id", description = "店铺id")
    private Long sellerId;
    /**
     * 付款时间
     */
    @Schema(name = "pay_time", description = "付款时间")
    private Long payTime;
    /**
     * 出账日期
     */
    @Schema(name = "create_time", description = "出账日期")
    private Long createTime;
    /**
     * 结算金额
     */
    @Schema(name = "bill_price", description = "结算金额")
    private Double billPrice;
    /**
     * 退单金额
     */
    @Schema(name = "refund_price", description = "退单金额")
    private Double refundPrice;

    @Schema(name = "refund_commi_price", description = "退还佣金金额")
    private Double refundCommiPrice;

    /**
     * 账单号
     */
    @Schema(name = "sn", description = "账单号")
    private String sn;
    /**
     * 店铺名称
     */
    @Schema(name = "shop_name", description = "店铺名称")
    private String shopName;
    /**
     * 银行开户名
     */
    @Schema(name = "bank_account_name", description = "银行开户名")
    private String bankAccountName;
    /**
     * 公司银行账号
     */
    @Schema(name = "bank_account_number", description = "公司银行账号")
    @SecretField(SecretType.BANK)
    private String bankAccountNumber;
    /**
     * 开户银行支行名称
     */
    @Schema(name = "bank_name", description = "开户银行支行名称")
    private String bankName;
    /**
     * 支行联行号
     */
    @Schema(name = "bank_code", description = "支行联行号")
    @SecretField(SecretType.BANK)
    private String bankCode;
    /**
     * 开户银行地址
     */
    @Schema(name = "bank_address", description = "开户银行地址")
    private String bankAddress;
    /**
     * 货到付款收入金额
     */
    @Schema(name = "cod_price", description = "货到付款收入金额")
    private Double codPrice;

    /**
     * 货到付款后退款金额
     */
    @Schema(name = "cod_refund_price", description = "货到付款后退款金额")
    private Double codRefundPrice;



    @Schema(name = "distribution_rebate", description = "分销返现支出")
    private Double distributionRebate;

    @Schema(name = "distribution_return_rebate", description = "分销返现支出返还")
    private Double distributionReturnRebate;

    /**
     * 站点优惠券的佣金
     */
    @Schema(name = "site_coupon_commi", description = "站点优惠券的佣金")
    private Double siteCouponCommi;

    /**
     * 结算周期内订单付款总金额
     */
    @Schema(name = "order_total_price", description = "结算周期内订单付款总金额")
    private Double orderTotalPrice;

    /**
     * 结算周期内订单退款总金额
     */
    @Schema(name = "refund_total_price", description = "结算周期内订单退款总金额")
    private Double refundTotalPrice;

    @PrimaryKeyField
    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public String getBillSn() {
        return billSn;
    }

    public void setBillSn(String billSn) {
        this.billSn = billSn;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getCommiPrice() {
        return commiPrice;
    }

    public void setCommiPrice(Double commiPrice) {
        this.commiPrice = commiPrice;
    }

    public Double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getBillType() {
        return billType;
    }

    public void setBillType(Integer billType) {
        this.billType = billType;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getPayTime() {
        return payTime;
    }

    public void setPayTime(Long payTime) {
        this.payTime = payTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Double getBillPrice() {
        return billPrice;
    }

    public void setBillPrice(Double billPrice) {
        this.billPrice = billPrice;
    }

    public Double getRefundPrice() {
        return refundPrice;
    }

    public void setRefundPrice(Double refundPrice) {
        this.refundPrice = refundPrice;
    }

    public Double getRefundCommiPrice() {
        return refundCommiPrice;
    }

    public void setRefundCommiPrice(Double refundCommiPrice) {
        this.refundCommiPrice = refundCommiPrice;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankAddress() {
        return bankAddress;
    }

    public Double getCodPrice() {
        return codPrice;
    }

    public void setCodPrice(Double codPrice) {
        this.codPrice = codPrice;
    }

    public Double getCodRefundPrice() {
        return codRefundPrice;
    }

    public void setCodRefundPrice(Double codRefundPrice) {
        this.codRefundPrice = codRefundPrice;
    }

    public void setBankAddress(String bankAddress) {
        this.bankAddress = bankAddress;
    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Double getDistributionRebate() {
        return distributionRebate;
    }

    public void setDistributionRebate(Double distributionRebate) {
        this.distributionRebate = distributionRebate;
    }

    public Double getDistributionReturnRebate() {
        return distributionReturnRebate;
    }

    public void setDistributionReturnRebate(Double distributionReturnRebate) {
        this.distributionReturnRebate = distributionReturnRebate;
    }

    public Double getSiteCouponCommi() {
        return siteCouponCommi;
    }

    public void setSiteCouponCommi(Double siteCouponCommi) {
        this.siteCouponCommi = siteCouponCommi;
    }

    public Double getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public void setOrderTotalPrice(Double orderTotalPrice) {
        this.orderTotalPrice = orderTotalPrice;
    }

    public Double getRefundTotalPrice() {
        return refundTotalPrice;
    }

    public void setRefundTotalPrice(Double refundTotalPrice) {
        this.refundTotalPrice = refundTotalPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Bill bill = (Bill) o;

        if (billId != null ? !billId.equals(bill.billId) : bill.billId != null) {
            return false;
        }
        if (billSn != null ? !billSn.equals(bill.billSn) : bill.billSn != null) {
            return false;
        }
        if (startTime != null ? !startTime.equals(bill.startTime) : bill.startTime != null) {
            return false;
        }
        if (endTime != null ? !endTime.equals(bill.endTime) : bill.endTime != null) {
            return false;
        }
        if (price != null ? !price.equals(bill.price) : bill.price != null) {
            return false;
        }
        if (commiPrice != null ? !commiPrice.equals(bill.commiPrice) : bill.commiPrice != null) {
            return false;
        }
        if (discountPrice != null ? !discountPrice.equals(bill.discountPrice) : bill.discountPrice != null) {
            return false;
        }
        if (status != null ? !status.equals(bill.status) : bill.status != null) {
            return false;
        }
        if (billType != null ? !billType.equals(bill.billType) : bill.billType != null) {
            return false;
        }
        if (sellerId != null ? !sellerId.equals(bill.sellerId) : bill.sellerId != null) {
            return false;
        }
        if (payTime != null ? !payTime.equals(bill.payTime) : bill.payTime != null) {
            return false;
        }
        if (createTime != null ? !createTime.equals(bill.createTime) : bill.createTime != null) {
            return false;
        }
        if (billPrice != null ? !billPrice.equals(bill.billPrice) : bill.billPrice != null) {
            return false;
        }
        if (refundPrice != null ? !refundPrice.equals(bill.refundPrice) : bill.refundPrice != null) {
            return false;
        }
        if (refundCommiPrice != null ? !refundCommiPrice.equals(bill.refundCommiPrice) : bill.refundCommiPrice != null) {
            return false;
        }
        if (sn != null ? !sn.equals(bill.sn) : bill.sn != null) {
            return false;
        }
        if (shopName != null ? !shopName.equals(bill.shopName) : bill.shopName != null) {
            return false;
        }
        if (bankAccountName != null ? !bankAccountName.equals(bill.bankAccountName) : bill.bankAccountName != null) {
            return false;
        }
        if (bankAccountNumber != null ? !bankAccountNumber.equals(bill.bankAccountNumber) : bill.bankAccountNumber != null) {
            return false;
        }
        if (bankName != null ? !bankName.equals(bill.bankName) : bill.bankName != null) {
            return false;
        }
        if (bankCode != null ? !bankCode.equals(bill.bankCode) : bill.bankCode != null) {
            return false;
        }
        if (bankAddress != null ? !bankAddress.equals(bill.bankAddress) : bill.bankAddress != null) {
            return false;
        }
        if (codPrice != null ? !codPrice.equals(bill.codPrice) : bill.codPrice != null) {
            return false;
        }
        if (codRefundPrice != null ? !codRefundPrice.equals(bill.codRefundPrice) : bill.codRefundPrice != null) {
            return false;
        }
        if (distributionRebate != null ? !distributionRebate.equals(bill.distributionRebate) : bill.distributionRebate != null) {
            return false;
        }
        if (distributionReturnRebate != null ? distributionReturnRebate.equals(bill.distributionReturnRebate) : bill.distributionReturnRebate == null) {
            return false;
        }
        if (orderTotalPrice != null ? !orderTotalPrice.equals(bill.orderTotalPrice) : bill.orderTotalPrice != null) {
            return false;
        }

        return refundTotalPrice != null ? refundTotalPrice.equals(bill.refundTotalPrice) : bill.refundTotalPrice == null;
    }

    @Override
    public int hashCode() {
        int result = billId != null ? billId.hashCode() : 0;
        result = 31 * result + (billSn != null ? billSn.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (commiPrice != null ? commiPrice.hashCode() : 0);
        result = 31 * result + (discountPrice != null ? discountPrice.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (billType != null ? billType.hashCode() : 0);
        result = 31 * result + (sellerId != null ? sellerId.hashCode() : 0);
        result = 31 * result + (payTime != null ? payTime.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (billPrice != null ? billPrice.hashCode() : 0);
        result = 31 * result + (refundPrice != null ? refundPrice.hashCode() : 0);
        result = 31 * result + (refundCommiPrice != null ? refundCommiPrice.hashCode() : 0);
        result = 31 * result + (sn != null ? sn.hashCode() : 0);
        result = 31 * result + (shopName != null ? shopName.hashCode() : 0);
        result = 31 * result + (bankAccountName != null ? bankAccountName.hashCode() : 0);
        result = 31 * result + (bankAccountNumber != null ? bankAccountNumber.hashCode() : 0);
        result = 31 * result + (bankName != null ? bankName.hashCode() : 0);
        result = 31 * result + (bankCode != null ? bankCode.hashCode() : 0);
        result = 31 * result + (bankAddress != null ? bankAddress.hashCode() : 0);
        result = 31 * result + (codPrice != null ? codPrice.hashCode() : 0);
        result = 31 * result + (codRefundPrice != null ? codRefundPrice.hashCode() : 0);
        result = 31 * result + (distributionRebate != null ? distributionRebate.hashCode() : 0);
        result = 31 * result + (distributionReturnRebate != null ? distributionReturnRebate.hashCode() : 0);
        result = 31 * result + (orderTotalPrice != null ? orderTotalPrice.hashCode() : 0);
        result = 31 * result + (refundTotalPrice != null ? refundTotalPrice.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "billId=" + billId +
                ", billSn='" + billSn + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", price=" + price +
                ", commiPrice=" + commiPrice +
                ", discountPrice=" + discountPrice +
                ", status='" + status + '\'' +
                ", billType=" + billType +
                ", sellerId=" + sellerId +
                ", payTime=" + payTime +
                ", createTime=" + createTime +
                ", billPrice=" + billPrice +
                ", refundPrice=" + refundPrice +
                ", refundCommiPrice=" + refundCommiPrice +
                ", sn='" + sn + '\'' +
                ", shopName='" + shopName + '\'' +
                ", bankAccountName='" + bankAccountName + '\'' +
                ", bankAccountNumber='" + bankAccountNumber + '\'' +
                ", bankName='" + bankName + '\'' +
                ", bankCode='" + bankCode + '\'' +
                ", bankAddress='" + bankAddress + '\'' +
                ", codPrice=" + codPrice +
                ", codRefundPrice=" + codRefundPrice +
                ", distributionRebate=" + distributionRebate +
                ", distributionReturnRebate=" + distributionReturnRebate +
                ", orderTotalPrice=" + orderTotalPrice +
                ", refundTotalPrice=" + refundTotalPrice +
                '}';
    }
}
