/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.orderbill.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author fk
 * @version v2.0
 * @Description: 结算单查询条件
 * @date 2018/4/2810:49
 * @since v7.0.0
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BillQueryParam {

    @Schema(name = "page_no", description = "页码")
    private Long pageNo;
    @Schema(name = "page_size", description = "分页数")
    private Long pageSize;
    @Schema(name = "seller_id", description = "商家ID")
    private Long sellerId;
    @Schema(name = "sn", description = "账单号")
    private String sn;
    @Schema(name = "bill_sn", description = "结算单号")
    private String billSn;

    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getBillSn() {
        return billSn;
    }

    public void setBillSn(String billSn) {
        this.billSn = billSn;
    }

    @Override
    public String toString() {
        return "BillQueryParam{" +
                "pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", sellerId=" + sellerId +
                ", sn='" + sn + '\'' +
                ", billSn='" + billSn + '\'' +
                '}';
    }
}
