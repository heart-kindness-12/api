/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


/**
 * 订单日志表实体
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-05-16 12:01:34
 */
@TableName("es_order_log")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrderLogDO implements Serializable {

    private static final long serialVersionUID = 435282086399754L;

    /**主键ID*/
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long logId;

    /**订单编号*/
    @Column(name = "order_sn")
    @Schema(name="order_sn",description="订单编号")
    private String orderSn;

    /**操作者id*/
    @Column(name = "op_id")
    @Schema(name="op_id",description="操作者id")
    private Long opId;

    /**操作者名称*/
    @Column(name = "op_name")
    @Schema(name="op_name",description="操作者名称")
    private String opName;

    /**日志信息*/
    @Column(name = "message")
    @Schema(name="message",description="日志信息")
    private String message;

    /**操作时间*/
    @Column(name = "op_time")
    @Schema(name="op_time",description="操作时间")
    private Long opTime;

    @PrimaryKeyField
    public Long getLogId() {
        return logId;
    }
    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public String getOrderSn() {
        return orderSn;
    }
    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Long getOpId() {
        return opId;
    }
    public void setOpId(Long opId) {
        this.opId = opId;
    }

    public String getOpName() {
        return opName;
    }
    public void setOpName(String opName) {
        this.opName = opName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getOpTime() {
        return opTime;
    }
    public void setOpTime(Long opTime) {
        this.opTime = opTime;
    }


    @Override
    public String toString() {
        return "OrderLogDO{" +
                "logId=" + logId +
                ", orderSn='" + orderSn + '\'' +
                ", opId=" + opId +
                ", opName='" + opName + '\'' +
                ", message=" + message +
                ", opTime=" + opTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }

        if (o == null || getClass() != o.getClass()){
            return false;
        }

        OrderLogDO that = (OrderLogDO) o;

        return new EqualsBuilder()
                .append(logId, that.logId)
                .append(orderSn, that.orderSn)
                .append(opId, that.opId)
                .append(opName, that.opName)
                .append(message, that.message)
                .append(opTime, that.opTime)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(logId)
                .append(orderSn)
                .append(opId)
                .append(opName)
                .append(message)
                .append(opTime)
                .toHashCode();
    }
}
