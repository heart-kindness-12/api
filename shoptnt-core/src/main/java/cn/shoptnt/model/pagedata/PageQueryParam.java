/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.pagedata;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;

/**
 * @Author shen
 * @Date 2021/3/15 9:46
 */
public class PageQueryParam {

    @Schema(description = "客户端类型",name = "client_type", required = true)
    @NotNull(message = "客户端类型不能为空")
    private String clientType;

    @Schema(description = "关键词",name = "keyword")
    private String keyword;

    @Schema(description = "发布状态",name = "publish_status")
    private String publishStatus;

    @Schema(description = "商家id",name = "seller_id")
    private Long sellerId;

    private Long pageNo;

    private Long pageSize;

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(String publishStatus) {
        this.publishStatus = publishStatus;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "PageQueryParam{" +
                "clientType='" + clientType + '\'' +
                ", keyword='" + keyword + '\'' +
                ", publishStatus='" + publishStatus + '\'' +
                ", sellerId=" + sellerId +
                ", pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                '}';
    }
}
