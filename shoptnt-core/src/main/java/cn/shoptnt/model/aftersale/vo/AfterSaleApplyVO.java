/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.aftersale.vo;

import cn.shoptnt.framework.validation.annotation.SafeDomain;
import cn.shoptnt.model.base.context.Region;
import cn.shoptnt.model.base.context.RegionFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * 申请售后服务VO
 * 主要用于用户提交售后服务申请时参数的传递
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-18
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AfterSaleApplyVO implements Serializable {

    private static final long serialVersionUID = 8028022122776504592L;

    /**
     * 订单编号
     */
    @Schema(name = "order_sn", description = "订单编号", required = true  )
    private String orderSn;
    /**
     * 申请原因
     */
    @Schema(name = "reason", description = "申请原因", required = true  )
    private String reason;
    /**
     * 问题描述
     */
    @Schema(name = "problem_desc", description = "问题描述", required = true  )
    private String problemDesc;
    /**
     * 申请凭证
     */
    @Schema(name = "apply_vouchers", description = "申请凭证"  )
    private String applyVouchers;
    /**
     * 提交数量
     */
    @Schema(name = "return_num", description = "提交数量", required = true)
    private Integer returnNum;
    /**
     * 商品SKUID
     */
    @Schema(name = "sku_id", description = "商品SKUID", required = true)
    private Long skuId;
    /**
     * 收货人姓名
     */
    @Schema(name = "ship_name", description = "收货人姓名", required = true  )
    private String shipName;
    /**
     * 收货人手机号
     */
    @Schema(name = "ship_mobile", description = "收货人手机号", required = true  )
    private String shipMobile;
    /**
     * 收货地址详细
     */
    @Schema(name = "ship_addr", description = "收货地址详细", required = true  )
    private String shipAddr;
    /**
     * 收货地区信息
     */
    @RegionFormat
    @Schema(name = "region", description = "地区")
    private Region region;
    /**
     * 售后图片信息集合
     */
    @SafeDomain
    @Schema(name = "images", description = "售后图片集合")
    private List<String> images;

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getProblemDesc() {
        return problemDesc;
    }

    public void setProblemDesc(String problemDesc) {
        this.problemDesc = problemDesc;
    }

    public String getApplyVouchers() {
        return applyVouchers;
    }

    public void setApplyVouchers(String applyVouchers) {
        this.applyVouchers = applyVouchers;
    }

    public Integer getReturnNum() {
        return returnNum;
    }

    public void setReturnNum(Integer returnNum) {
        this.returnNum = returnNum;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipMobile() {
        return shipMobile;
    }

    public void setShipMobile(String shipMobile) {
        this.shipMobile = shipMobile;
    }

    public String getShipAddr() {
        return shipAddr;
    }

    public void setShipAddr(String shipAddr) {
        this.shipAddr = shipAddr;
    }

    @JsonIgnore
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AfterSaleApplyVO that = (AfterSaleApplyVO) o;
        return Objects.equals(orderSn, that.orderSn) &&
                Objects.equals(reason, that.reason) &&
                Objects.equals(problemDesc, that.problemDesc) &&
                Objects.equals(applyVouchers, that.applyVouchers) &&
                Objects.equals(returnNum, that.returnNum) &&
                Objects.equals(skuId, that.skuId) &&
                Objects.equals(shipName, that.shipName) &&
                Objects.equals(shipMobile, that.shipMobile) &&
                Objects.equals(shipAddr, that.shipAddr) &&
                Objects.equals(region, that.region) &&
                Objects.equals(images, that.images);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderSn, reason, problemDesc, applyVouchers, returnNum, skuId, shipName, shipMobile, shipAddr, region, images);
    }

    @Override
    public String toString() {
        return "AfterSaleApplyVO{" +
                "orderSn='" + orderSn + '\'' +
                ", reason='" + reason + '\'' +
                ", problemDesc='" + problemDesc + '\'' +
                ", applyVouchers='" + applyVouchers + '\'' +
                ", returnNum=" + returnNum +
                ", skuId=" + skuId +
                ", shipName='" + shipName + '\'' +
                ", shipMobile='" + shipMobile + '\'' +
                ", shipAddr='" + shipAddr + '\'' +
                ", region=" + region +
                ", images=" + images +
                '}';
    }
}
