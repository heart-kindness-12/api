/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.order;

import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.model.trade.order.enums.OrderMetaKeyEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2020/08/10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class OrderMetaManagerTest {

    @Autowired
    private OrderMetaManager orderMetaManager;

    @Test
    public void getMetaValue() {

        
    }

    @Test
    public void list() {

        
    }

    @Test
    public void updateMetaValue() {

        orderMetaManager.updateMetaValue("37419452492709890", OrderMetaKeyEnum.CASH_BACK, "111");
    }

    @Test
    public void updateMetaStatus() {

        orderMetaManager.updateMetaStatus("37419452492709890", OrderMetaKeyEnum.CASH_BACK, "0");
    }

    @Test
    public void getModel() {

        
    }

    @Test
    public void getGiftList() {

        
    }

}
