# SHOPTNT商城

## 商城介绍
### 官网
http://www.shoptnt.cn

SHOPTNT商城是使用java语言开发，基于SpringBoot架构体系构建的一套b2b2c商城，商城是满足集平台自营和多商户入驻于一体的多商户运营服务系统。包含PC 端、手机端（H5\APP\小程序），系统架构以及实现案例中应满足和未来可能出现的业务系统进行对接。支持常见NFS协议，支持虚拟硬盘远程挂载；支持关系模型，支持分布式集群处理；主要功能要涵盖：订单管理、会员管理、商品管理、店铺管理、促销活动、售前售后、数据统计、分销模式、页面自定义装修、文章维护、商家入驻和消费者商品选购、下单、评论、咨询等功能。


## 演示

### 演示地址

平台管理端：https://manager-bbc.shoptnt.cn 账号：admin/111111

店铺管理端：https://seller-bbc.shoptnt.cn 账号：ceshi/111111

商城PC页面：https://shop-bbc.shoptnt.cn 账号可自行注册

商城移动端，扫描如下二维码

![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/weichat-gongzhonghao.jpg?x-oss-process=style/300x300)
### 开发/部署文档中心

https://docs.shoptnt.cn/docs/5.2.3

### 单机版
为方便大家更好更快更方便的启动我们的后台程序，只需要启动一个API即可，参考文档 https://docs.shoptnt.cn/docs/5.2.3/quickly-start/localhost/localhost-api-standlone


## 项目地址

后台API：https://gitee.com/bbc-se/api

前端ui：https://gitee.com/bbc-se/pc-ui

移动端：https://gitee.com/bbc-se/mobile-ui

配置中心：https://gitee.com/bbc-se/config

## 技术栈
### 总体架构
![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/jiagou.png "微信")

### 服务端
| 框架             | 备注           | 版本          |
| ---------------- | -------------- | ------------- |
| Spring  Boot     | 核心框架       | 2.0.1.RELEASE |
| mybatis  plus    | 持久框架       | 3.4.2         |
| sharding  sphere | 分库分表       | 4.1.0         |
| Maven            | 程序构建       |               |
| Mysql            | 数据库         | 5.6/5.7       |
| RabbitMQ         | 消息中间件AMQP | 3.x(3.6.14)   |
| Redis            | 缓存           | 5.x           |
| Elasticsearch    | 搜索引擎       | 6.x(6.2.2)    |
| Spring Security  | 安全框架       | 2.0.1.RELEASE |
| Druid            | 数据库连接池   | 1.1.22        |
| xxl-job          | 定时任务       | 2.0.0         |
| Nginx            | 负载均衡       |               |
| Oss              | 静态资源分发   |               |
| Logback          | 日志处理       |               |
| alibaba/p3c      | 代码检查       |               |
### 客户端-pc端（管理端、商家端、会员端)
| 框架              | 备注         | 版本   |
| ----------------- | ------------ | ------ |
| webpack           | 构建工具     | 3.10.0 |
| ES6               | JS版本       |        |
| Vue.js            | 基础JS框架   | 2.6.14 |
| jQuery            | 辅助JS库     | 2.1.4  |
| Vue Router        | 路由管理     | 3.0.1  |
| Vuex              | 状态管理     | 3.0.1  |
| Element UI        | 基础UI库     | 2.15.5 |
| vue-element-admin | UI界面基于   |        |
| Axios             | 网络请求     | 0.18.0 |
| Scss              | CSS预处理    | 4.13.0 |
| ESLint            | 代码检查     | 4.13.1 |
| ECharts           | 报表系统     | 3.8.5  |
| 百度 UEditor      | 富文本编辑器 |        |
| 百度 Web Uploader | 图片上传插件 |        |

### 客户端-移动端
| 框架    | 备注       | 版本   |
| ------- | ---------- | ------ |
| UniApp  | 移动端框架 | 最新版 |
| Vuejs   | PC端框架   | v2     |
| UViewUI | 移动端UI库 | 1.8.4  |
## 商城功能展示
### 功能概览
#### 管理端
![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/%E7%AE%A1%E7%90%86%E4%B8%AD%E5%BF%83.png)
#### 商家端
![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/%E5%95%86%E5%AE%B6%E4%B8%AD%E5%BF%83.png)
#### 会员端
![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/%E4%BC%9A%E5%91%98%E4%B8%AD%E5%BF%83.png)

### 商城风采

#### PC端
![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/%E4%BC%9A%E5%91%98%E7%AB%AF.gif
)

#### 移动端
![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/%E7%A7%BB%E5%8A%A8%E7%AB%AF.gif)


## 使用须知

1. 允许个人学习使用。
2. 允许用于学习、毕业设计等。
3. 禁止将本开源的代码和资源进行任何形式任何名义的出售。
4. 限制商业使用，如需[商业使用](http://www.shoptnt.cn)联系QQ：2025555598 或微信扫一扫加我微信。
![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/danren.png?x-oss-process=style/300x300)

## 交流、反馈

### 推荐
官方微信群：

![](https://shoptnt-statics.oss-cn-beijing.aliyuncs.com/qun.png?x-oss-process=style/300x300)
