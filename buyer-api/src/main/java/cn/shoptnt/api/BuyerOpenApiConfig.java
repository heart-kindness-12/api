package cn.shoptnt.api;

import cn.shoptnt.framework.openapi.GlobalOperationCustomizer;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * OpenAPI3.0  分组配置
 *
 * @author gy
 * @version 1.0
 * @sinc 7.3.0
 * @date 2022年12月08日 17:04
 * Knife4j 文档访问地址为：http://ip:port/doc.html  推荐使用
 * swagger3 访问地址为：  http://ip:port/swagger-ui/index.html
 */
@Configuration
public class BuyerOpenApiConfig {


    private static final String PACKAGES_PATH = "cn.shoptnt.api.buyer";


    @Bean
    public GroupedOpenApi defaultApi() {
        return GroupedOpenApi.builder()
                .group("默认分组")
                .pathsToMatch("/**")
                .packagesToScan("cn.shoptnt.api")
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }


    @Bean
    public GroupedOpenApi distributionBuyerApi() {
        return GroupedOpenApi.builder()
                .group("买家端-分销")
                .pathsToMatch("/buyer/distribution/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();

    }


    @Bean
    public GroupedOpenApi pintuanBuyerApi() {
        return GroupedOpenApi.builder()
                .group("买家端-拼团")
                .pathsToMatch("/buyer/pintuan/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();

    }


    @Bean
    public GroupedOpenApi afterSaleBuyerApi() {
        return GroupedOpenApi.builder()
                .group("买家端-售后")
                .pathsToMatch("/buyer/after-sales/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();

    }

    @Bean
    public GroupedOpenApi sssBuyerApi() {
        return GroupedOpenApi.builder()
                .group("买家端-流量")
                .pathsToMatch("/buyer/view/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();

    }

    @Bean
    public GroupedOpenApi goodsBuyerApi() {
        return GroupedOpenApi.builder()
                .group("买家端-商品")
                .pathsToMatch("/buyer/goods/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }



    @Bean
    public GroupedOpenApi passportBuyerApi() {
        return GroupedOpenApi.builder()
                .group("买家端-会员认证中心")
                .pathsToMatch("/buyer/passport/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi memberBuyerApi() {
        return GroupedOpenApi.builder()
                .group("买家端-会员中心")
                .pathsToMatch("/buyer/members/**", "/buyer/account-binder/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi pageDataBuyerApi() {
        return GroupedOpenApi.builder()
                .group("买家端-楼层")
                .pathsToMatch("/buyer/pages/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi paymentBuyerApi() {
        return GroupedOpenApi.builder()
                .group("买家端-支付")
                .pathsToMatch("/buyer/order/pay/**", "/buyer/balance/**", "/buyer/recharge/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi shopBuyerApi() {
        return GroupedOpenApi.builder()
                .group("买家端-店铺")
                .pathsToMatch("/buyer/shops/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();

    }

    @Bean
    public GroupedOpenApi tradeBuyerApi() {
        return GroupedOpenApi.builder()
                .group("买家端-交易")
                .pathsToMatch("/buyer/trade/**", "/expresss/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi promotionBuyerApi() {
        return GroupedOpenApi.builder()
                .group("买家端-促销")
                .pathsToMatch("/buyer/promotions/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

}
