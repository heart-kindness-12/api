/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.passport;

import cn.shoptnt.client.system.ValidatorClient;
import cn.shoptnt.model.member.vo.MemberVO;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.service.passport.PassportManager;
import cn.shoptnt.framework.ShopTntConfig;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.constraints.NotEmpty;


/**
 * 会员登录注册API
 *
 * @author zh
 * @version v7.0
 * @since v7.0
 * 2018年3月23日 上午10:12:12
 */
@RestController
@RequestMapping("/buyer/passport")
@Tag(name = "会员登录API")
@Validated
public class PassportLoginBuyerController {

    @Autowired
    private PassportManager passportManager;
    @Autowired
    private ValidatorClient validatorClient;
    @Autowired
    private MemberManager memberManager;

    @Autowired
    private ShopTntConfig shoptntConfig;

    @PostMapping(value = "/login/smscode/{mobile}")
    @Operation(summary = "发送验证码")
    @Parameters({
            @Parameter(name = "mobile", description = "手机号码", required = true,   in = ParameterIn.PATH)
    })
    public String sendSmsCode(@PathVariable("mobile") String mobile) {
        //参数验证（验证图片验证码或滑动验证参数等）
        this.validatorClient.validate();

        passportManager.sendLoginSmsCode(mobile);
        return shoptntConfig.getSmscodeTimout() / 60 + "";
    }

    @PostMapping("/login")
    @Operation(summary = "用户名（手机号）/密码登录API")
    @Parameters({
            @Parameter(name = "username", description = "用户名", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "password", description = "密码", required = true,   in = ParameterIn.QUERY)
    })
    public MemberVO login(@NotEmpty(message = "用户名不能为空") String username, @NotEmpty(message = "密码不能为空") String password) {
        //参数验证（验证图片验证码或滑动验证参数等）
        this.validatorClient.validate();

        //校验账号信息是否正确
        return memberManager.login(username, password, 1);
    }

    @PostMapping("/login/{mobile}")
    @Operation(summary = "手机号码登录API")
    @Parameters({
            @Parameter(name = "mobile", description = "手机号", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "sms_code", description = "手机验证码", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "uuid", description = "客户端唯一标识", required = true,   in = ParameterIn.QUERY)
    })
    public MemberVO mobileLogin(@PathVariable String mobile, @Parameter(hidden = true) @NotEmpty(message = "短信验证码不能为空") String smsCode) {
        return memberManager.mobileLogin(mobile, smsCode, 1);

    }

}
