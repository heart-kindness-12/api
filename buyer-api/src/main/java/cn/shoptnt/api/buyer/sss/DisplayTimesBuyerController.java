/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.sss;

import cn.shoptnt.service.statistics.DisplayTimesManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * 访问次数统计
 *
 * @author liushuai
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/8/7 上午8:17
 */

@RestController
@RequestMapping("/buyer/view")
@Tag(name = "访问次数统计")
public class DisplayTimesBuyerController {

    @Autowired
    private DisplayTimesManager displayTimesManager;

    @GetMapping()
    @Operation(summary = "访问页面")
    @Parameters({
            @Parameter(name = "url", description = "url地址", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "uuid", description = "uuid", required = true,   in = ParameterIn.QUERY)
    })
    public void view(String url, String uuid) {
        displayTimesManager.view(url, uuid);
    }


}