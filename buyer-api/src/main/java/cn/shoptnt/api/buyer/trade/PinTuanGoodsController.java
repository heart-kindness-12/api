/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import cn.shoptnt.model.promotion.pintuan.PinTuanGoodsVO;
import cn.shoptnt.model.promotion.pintuan.PintuanOrder;
import cn.shoptnt.service.trade.pintuan.PintuanGoodsManager;
import cn.shoptnt.service.trade.pintuan.PintuanOrderManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

/**
 * Created by 妙贤 on 2019-01-22.
 * 拼团商品API
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2019-01-22
 */
@Tag(name = "拼团商品API")
@RestController
@RequestMapping("/buyer/pintuan/goods")
public class PinTuanGoodsController {

    @Autowired
    private PintuanGoodsManager pintuanGoodsManager;

    @Autowired
    private PintuanOrderManager pintuanOrderManager;

    @GetMapping("/skus/{sku_id}")
    @Operation(summary = "获取某个拼团的详细")
    @Parameters({
            @Parameter(name = "sku_id", description = "skuid", required = true,   in = ParameterIn.PATH)

    })
    public PinTuanGoodsVO detail(@Parameter(hidden = true) @PathVariable(name = "sku_id") Long skuId) {

        PinTuanGoodsVO pinTuanGoodsVO = pintuanGoodsManager.getDetail(skuId, null);

        return pinTuanGoodsVO;
    }

    @Operation(summary = "获取此商品拼团的所有参与的sku信息")
    @Parameters({
            @Parameter(name = "goods_id", description = "商品id", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "pintuan_id", description = "拼团id", required = true,   in = ParameterIn.QUERY)
    })
    @GetMapping("/{goods_id}/skus")
    public List<GoodsSkuVO> skus(@Parameter(hidden = true) @PathVariable(name = "goods_id") Long goodsId, @Parameter(hidden = true) Long pintuanId) {

        return pintuanGoodsManager.skus(goodsId, pintuanId);
    }


    @Operation(summary = "获取此商品待成团的订单")
    @Parameters({
            @Parameter(name = "goods_id", description = "商品id", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "sku_id", description = "skuid", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping("/{goods_id}/orders")
    public List<PintuanOrder> orders(@Parameter(hidden = true) @PathVariable(name = "goods_id") Long goodsId, @Parameter(hidden = true) Long skuId) {

        return pintuanOrderManager.getWaitOrder(goodsId, skuId);
    }

    @GetMapping("/skus/uniapp")
    @Operation(summary =  "查询拼团促销活动商品分页列表数据")
    @Parameters({
            @Parameter(name = "category_id", description = "分类id",  in= ParameterIn.QUERY),
            @Parameter(name = "page_no", description =  "页码",  required = true ,  in=ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "页大小",  required = true ,  in=ParameterIn.QUERY)
    })
    public WebPage<PinTuanGoodsVO> searchPage(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) Long categoryId) {
        WebPage<PinTuanGoodsVO> webPage  = pintuanGoodsManager.list(pageNo, pageSize, categoryId);
        return webPage;
    }

}
