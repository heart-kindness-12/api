/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.passport;

import cn.shoptnt.framework.security.model.TokenConstant;
import cn.shoptnt.framework.util.DbSecretUtil;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.base.SceneType;
import cn.shoptnt.client.system.SmsClient;
import cn.shoptnt.client.system.ValidatorClient;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.service.member.MemberSecurityManager;
import cn.shoptnt.service.passport.PassportManager;
import cn.shoptnt.framework.ShopTntConfig;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.framework.util.StringUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.HashMap;
import java.util.Map;

/**
 * 会员找回密码api
 *
 * @author zh
 * @version v7.0
 * @date 18/5/16 下午4:07
 * @since v7.0
 */
@RestController
@RequestMapping("/buyer/passport")
@Tag(name = "会员找回密码api")
@Validated
public class PassportFindPasswordBuyerController {

    @Autowired
    private ValidatorClient validatorClient;
    @Autowired
    private MemberManager memberManager;
    @Autowired
    private Cache cache;
    @Autowired
    private PassportManager passportManager;
    @Autowired
    private MemberSecurityManager memberSecurityManager;
    @Autowired
    private SmsClient smsClient;
    @Autowired
    private ShopTntConfig shoptntConfig;


    @Operation(summary = "获取账户信息")
    @GetMapping("find-pwd")
    @Parameters({
            @Parameter(name = "uuid", description = "uuid客户端的唯一标识",
                    required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "account", description = "账户名称", required = true,
                      in = ParameterIn.QUERY)
    })
    public String getMemberInfo(@NotEmpty(message = "uuid不能为空") String uuid,
                                @NotEmpty(message = "账户名称不能为空") String account) {
        // 参数验证（验证图片验证码或滑动验证参数等）
        this.validatorClient.validate();

        // 对会员状态进行校验
        Member member = memberManager.getMemberByAccount(account);
        member.checkDisable();
        // 对获得的会员信息进行处理
        String mobile = DbSecretUtil.decrypt(member.getMobile(), TokenConstant.SECRET);
        mobile = mobile.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
        // 对用户名的处理
        String name = member.getUname();
        // 将数据组织好json格式返回
        uuid = StringUtil.getUUId();
        Map map = new HashMap(16);
        map.put("mobile", mobile);
        map.put("uname", name.substring(0, 1) + "***" + name.substring(name.length() - 1, name.length()));
        map.put("uuid", uuid);
        cache.put(uuid, member, shoptntConfig.getSmscodeTimout());
        return JsonUtil.objectToJson(map);

    }


    @PostMapping(value = "/find-pwd/send")
    @Operation(summary = "发送验证码")
    @Parameters({
            @Parameter(name = "uuid", description = "uuid客户端的唯一标识",
                    required = true,   in = ParameterIn.QUERY)
    })
    public String sendSmsCode(@NotEmpty(message = "uuid不能为空") String uuid) {
        // 参数验证（验证图片验证码或滑动验证参数等）
        this.validatorClient.validate();

        Member member = (Member) cache.get(uuid);
        if (member != null) {
            passportManager.sendFindPasswordCode(member.getMobile());
            return shoptntConfig.getSmscodeTimout() / 60 + "";
        }
        throw new ServiceException(MemberErrorCode.E119.code(), "请先对当前用户进行身份校验");
    }

    @PutMapping(value = "/find-pwd/update-password")
    @Operation(summary = "修改密码")
    @Parameters({
            @Parameter(name = "uuid", description = "uuid客户端的唯一标识",
                    required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "password", description = "密码",
                    required = true,   in = ParameterIn.QUERY)
    })
    public String updatePassword(@NotEmpty(message = "uuid不能为空") String uuid, String password) {
        Object o = cache.get(CachePrefix.SMS_VERIFY.getPrefix() + uuid);
        if (o != null) {
            Member member = (Member) cache.get(uuid);
            if (member != null) {
                memberSecurityManager.updatePassword(member.getMemberId(), password);
                return null;
            }
            cache.remove(CachePrefix.SMS_VERIFY.getPrefix() + uuid);
            cache.remove(uuid);
        }
        throw new ServiceException(MemberErrorCode.E119.code(), "请先对当前用户进行身份校验");
    }

    @GetMapping(value = "/find-pwd/valid")
    @Operation(summary = "验证找回密码验证码")
    @Parameters({
            @Parameter(name = "uuid", description = "uuid客户端的唯一标识",
                    required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "sms_code", description = "验证码",
                    required = true,   in = ParameterIn.QUERY)
    })
    public String updateCodeCheck(@Valid @Parameter(hidden = true) @NotEmpty(message = "验证码不能为空") String smsCode,
                                  @NotEmpty(message = "uuid不能为空") String uuid) {
        Member member = (Member) cache.get(uuid);
        if (member == null) {
            throw new ServiceException(MemberErrorCode.E119.code(), "请先对当前用户进行身份校验");
        }
        if (StringUtil.isEmpty(member.getMobile())) {
            cache.remove(uuid);
            throw new ServiceException(MemberErrorCode.E119.code(), "请先对账户进行手机号码绑定在进行此操作");
        }
        boolean isPass = smsClient.valid(SceneType.VALIDATE_MOBILE.name(), member.getMobile(), smsCode);
        if (!isPass) {
            throw new ServiceException(MemberErrorCode.E107.code(), "短信验证码不正确");
        } else {
            // 通过验证的请求，会存放一分钟。
            cache.put(CachePrefix.SMS_VERIFY.getPrefix() + uuid, " ", 1 * 60);
        }
        return null;

    }

}
