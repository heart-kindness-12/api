/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.passport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.service.member.plugin.wechat.WechatConnectLoginPlugin;
import cn.shoptnt.service.member.ConnectManager;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.framework.cache.Cache;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.constraints.Pattern;
import java.util.Map;

/**
 * @author fk
 * @version v2.0
 * @Description: 小程序登录接口
 * @date 2018/11/20 14:56
 * @since v7.0.0
 */
@RestController
@RequestMapping("/buyer/passport/mini-program")
@Tag(name = "小程序登录api")
@Validated
public class PassportMiniProgramBuyerController {

    @Autowired
    public WechatConnectLoginPlugin wechatConnectLoginPlugin;

    @Autowired
    public ConnectManager connectManager;

    @Autowired
    private MemberManager memberManager;

    @Autowired
    private Cache cache;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/auto-login")
    @Operation(summary = "小程序自动登录")
    @Parameters({
            @Parameter(name = "code", description = "第三方code",     in = ParameterIn.QUERY),
            @Parameter(name = "uuid", description = "uuid",    in = ParameterIn.QUERY),
    })
    public Map autoLogin(String code, String uuid) {

        //获取sessionkey和openid或者unionid
        String content = wechatConnectLoginPlugin.miniProgramAutoLogin(code);

        return this.connectManager.miniProgramLogin(content, uuid);
    }

    @GetMapping("/decrypt")
    @Operation(summary = "加密数据解密验证")
    @Parameters({
            @Parameter(name = "code", description = "第三方code",     in = ParameterIn.QUERY),
            @Parameter(name = "encrypted_data", description = "要解密的数据",     in = ParameterIn.QUERY),
            @Parameter(name = "uuid", description = "uuid",     in = ParameterIn.QUERY),
            @Parameter(name = "iv", description = "偏移量",    in = ParameterIn.QUERY),
    })
    public Map decrypt(String code, String encryptedData, String uuid, String iv) {

        return connectManager.decrypt(code, encryptedData, uuid, iv);
    }


    @GetMapping("/code-unlimit")
    @Operation(summary = "获取微信小程序码")
    @Parameter(name = "goods_id", description = "商品id", required = true,  in = ParameterIn.QUERY)
    public String getWXACodeUnlimit(@Parameter(hidden = true) Long goodsId) {

        String accessTocken = wechatConnectLoginPlugin.getWXAccessToken();

        return connectManager.getWXACodeUnlimit(accessTocken, goodsId);
    }


    @PostMapping("/distribution")
    @Operation(summary = "存储小程序端分销的上级id")
    @Parameter(name = "from", description = "上级会员id加密格式", required = true,    in = ParameterIn.QUERY)
    public String distribution(String from, @RequestHeader(required = false) String uuid) {

        logger.debug("==============前台传过来的缓存key:" + from);
        logger.debug("==============前台传过来的uuid:" + uuid);
        if (StringUtil.notEmpty(uuid) && StringUtil.notEmpty(from)) {
            try {
                //从缓存中获取分销合伙人的会员ID
                Object memberId = cache.get(CachePrefix.MEMBER_SU.getPrefix() + from);
                logger.debug("==============从缓存中获取的会员ID为:" + memberId);
                //如果会员ID不为空
                if (memberId != null) {
                    //将uuid作为key值，再次将会员ID存放至缓存中
                    cache.put(CachePrefix.DISTRIBUTION_UP.getPrefix() + uuid, memberId);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        return "";
    }


    @Operation(summary = "小程序注册绑定")
    @PostMapping("/register-bind/{uuid}")
    @Parameters({
            @Parameter(name = "uuid", description = "唯一标识", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "nick_name", description = "昵称", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "face", description = "头像", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "sex", description = "性别", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "mobile", description = "手机号码", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "password", description = "密码", required = true,   in = ParameterIn.QUERY),

    })
    public Map binder(@PathVariable("uuid") String uuid, @Length(max = 20, message = "昵称超过长度限制") @Parameter(hidden = true) String nickName, String face, Integer sex, String mobile, @Pattern(regexp = "[a-fA-F0-9]{32}", message = "密码格式不正确") String password) {
        //执行注册
        Member member = new Member();
        member.setUname("m_" + mobile);
        member.setMobile(mobile);
        member.setPassword(password);
        member.setNickname(nickName);
        member.setFace(face);
        member.setSex(sex);
        memberManager.register(member);
        //执行绑定账号
        Map map = connectManager.mobileBind(mobile, uuid);
        return map;
    }


}
