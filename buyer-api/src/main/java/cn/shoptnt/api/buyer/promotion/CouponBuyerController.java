/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.promotion;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.coupon.dos.CouponDO;
import cn.shoptnt.model.promotion.coupon.enums.CouponType;
import cn.shoptnt.model.promotion.coupon.vo.GoodsCouponVO;
import cn.shoptnt.service.promotion.coupon.CouponManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import java.util.List;

/**
 * 优惠券相关API
 *
 * @author Snow create in 2018/7/13
 * @version v2.0
 * @since v7.0.0
 */
@RestController
@RequestMapping("/buyer/promotions/coupons")
@Tag(name = "优惠券相关API")
@Validated
public class CouponBuyerController {

    @Autowired
    private CouponManager couponManager;

    @Operation(summary = "查询商家优惠券列表")
    @Parameters({
            @Parameter(name = "seller_id", description ="商家ID",  in = ParameterIn.QUERY)
    })
    @GetMapping()
    public List<CouponDO> getList(@Parameter(hidden = true) Long sellerId) {

        List<CouponDO> couponDOList = this.couponManager.getList(sellerId, CouponType.FREE_GET.name());
        return couponDOList;
    }

    @Operation(summary = "查询某商品的优惠券列表")
    @Parameters({
            @Parameter(name = "goods_id", description ="商品ID",  in = ParameterIn.QUERY)
    })
    @GetMapping("/goods-use")
    public List<GoodsCouponVO> getListByGoods(@Parameter(hidden = true) Long goodsId) {

        List<GoodsCouponVO> couponDOList = this.couponManager.getListByGoods(goodsId);

        return couponDOList;
    }


    @Operation(summary = "查询所有优惠券")
    @Parameters({
            @Parameter(name = "page_no", description ="页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description ="条数",  in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description ="商家ID",  in = ParameterIn.QUERY)
    })
    @GetMapping(value = "/all")
    public WebPage<CouponDO> getPage(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) Long sellerId) {
        WebPage<CouponDO> page = this.couponManager.all(pageNo, pageSize, sellerId);
        return page;
    }


}
