/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.member.dos.MemberNoticeLog;
import cn.shoptnt.model.member.dto.MemberNoticeDTO;
import cn.shoptnt.service.member.MemberNoticeLogManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


/**
 * 会员站内消息历史控制器
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-07-05 14:10:16
 */
@RestController
@RequestMapping("/buyer/members/member-nocice-logs")
@Tag(name = "会员站内消息历史相关API")
public class MemberNoticeLogBuyerController {

    @Autowired
    private MemberNoticeLogManager memberNociceLogManager;


    @Operation(summary = "查询会员站内消息历史列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "read", description = "是否已读，1已读，0未读", in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, Integer read) {
        return this.memberNociceLogManager.list(pageNo, pageSize, read);
    }


    @PutMapping(value = "/{ids}/read")
    @Operation(summary = "将消息设置为已读")
    @Parameters({
            @Parameter(name = "ids", description = "要设置为已读消息的id", required = true,  in = ParameterIn.PATH)
    })
    public String read(@PathVariable Long[] ids) {
        this.memberNociceLogManager.read(ids);
        return null;
    }


    @DeleteMapping(value = "/{ids}")
    @Operation(summary = "删除会员站内消息历史")
    @Parameters({
            @Parameter(name = "ids", description = "要删除的消息主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long[] ids) {
        this.memberNociceLogManager.delete(ids);
        return null;
    }

    @Operation(summary = "查询会员站内消息未读消息数量")
    @GetMapping("/number")
    public MemberNoticeDTO getNum() {
        return this.memberNociceLogManager.getNum();
    }
}
