/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.debugger;

import cn.shoptnt.model.base.vo.SmsSendVO;
import cn.shoptnt.framework.message.direct.DirectMessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-04-23
 */
@RestController
@RequestMapping("/debugger/sms")
@ConditionalOnProperty(value = "shoptnt.debugger", havingValue = "true")
public class SmsCheckController {
    @Autowired
    private DirectMessageSender messageSender;

    @GetMapping(value = "/test")
    public String test( String mobile ) {

        SmsSendVO smsSendVO = new SmsSendVO();
        smsSendVO.setContent("您的注册验证码为：897383");
        smsSendVO.setMobile(mobile);

        messageSender.send(smsSendVO);

        return "ok";
    }
}
