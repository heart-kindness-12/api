/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.client.member.MemberHistoryReceiptClient;
import cn.shoptnt.client.member.DepositeClient;
import cn.shoptnt.model.member.vo.MemberDepositeVO;
import cn.shoptnt.model.errorcode.TradeErrorCode;
import cn.shoptnt.model.trade.cart.dos.OrderPermission;
import cn.shoptnt.model.trade.order.dos.OrderLogDO;
import cn.shoptnt.model.trade.order.dos.TradeDO;
import cn.shoptnt.model.trade.order.dto.OrderDetailQueryParam;
import cn.shoptnt.model.trade.order.dto.OrderQueryParam;
import cn.shoptnt.model.trade.order.enums.OrderTagEnum;
import cn.shoptnt.model.trade.order.enums.PaymentTypeEnum;
import cn.shoptnt.model.trade.order.vo.*;
import cn.shoptnt.service.trade.order.OrderLogManager;
import cn.shoptnt.service.trade.order.OrderOperateManager;
import cn.shoptnt.service.trade.order.OrderQueryManager;
import cn.shoptnt.service.trade.order.TradeQueryManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Buyer;
import cn.shoptnt.framework.util.StringUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.util.List;

/**
 * 会员订单相关控制器
 *
 * @author Snow create in 2018/5/14
 * @version v2.0
 * @since v7.0.0
 */

@Tag(name = "会员订单API")
@RestController
@RequestMapping("/buyer/trade/orders")
@Validated
public class OrderBuyerController {

    @Autowired
    private OrderQueryManager orderQueryManager;

    @Autowired
    private OrderOperateManager orderOperateManager;

    @Autowired
    private TradeQueryManager tradeQueryManager;

    @Autowired
    private MemberHistoryReceiptClient memberHistoryReceiptClient;

    @Autowired
    private OrderLogManager orderLogManager;

    @Autowired
    private DepositeClient depositeClient;


    @Operation(summary = "查询会员订单列表")
    @Parameters({
            @Parameter(name = "goods_name", description = "商品名称关键字",   in = ParameterIn.QUERY),
            @Parameter(name = "key_words", description = "关键字",   in = ParameterIn.QUERY),
            @Parameter(name = "order_status", description = "订单状态",   in = ParameterIn.QUERY,
                    example = "ALL:所有订单,WAIT_PAY:待付款,WAIT_SHIP:待发货,WAIT_ROG:待收货," +
                            "CANCELLED:已取消,COMPLETE:已完成,WAIT_COMMENT:待评论,WAIT_CHASE:待追评，REFUND:售后中"),
            @Parameter(name = "page_no", description = "页数",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "条数",  in = ParameterIn.QUERY),
            @Parameter(name = "start_time", description = "开始时间",   in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "结束时间",   in = ParameterIn.QUERY)
    })
    @GetMapping()
    public WebPage<OrderLineVO> list(@Parameter(hidden = true) String keyWords, @Parameter(hidden = true) String goodsName, @Parameter(hidden = true) String orderStatus,
                                     @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) Long startTime, @Parameter(hidden = true) Long endTime) {

        WebPage page = null;
        try {
            try {
                if (StringUtil.isEmpty(orderStatus)) {
                    orderStatus = "ALL";
                }
                OrderTagEnum.valueOf(orderStatus);
            } catch (Exception e) {
                throw new ServiceException(TradeErrorCode.E455.code(), "订单状态参数错误");
            }

            Buyer buyer = UserContext.getBuyer();
            OrderQueryParam param = new OrderQueryParam();
            param.setGoodsName(goodsName);
            param.setTag(orderStatus);
            param.setMemberId(buyer.getUid());
            param.setKeywords(keyWords);
            param.setPageNo(pageNo);
            param.setPageSize(pageSize);
            param.setStartTime(startTime);
            param.setEndTime(endTime);

            page = this.orderQueryManager.list(param);

            //货到付款的订单不允许线上支付
            List<OrderLineVO> list = page.getData();
            for (OrderLineVO order : list) {
                if (PaymentTypeEnum.COD.value().equals(order.getPaymentType())) {
                    order.getOrderOperateAllowableVO().setAllowPay(false);
                }
            }
            page.setData(list);
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        return page;
    }


    @Operation(summary = "查询单个订单明细")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", required = true,   in = ParameterIn.PATH)
    })
    @GetMapping(value = "/{order_sn}")
    public OrderDetailVO get(@Parameter(hidden = true) @PathVariable("order_sn") String orderSn) {
        Buyer buyer = UserContext.getBuyer();
        OrderDetailQueryParam queryParam = new OrderDetailQueryParam();
        queryParam.setBuyerId(buyer.getUid());
        OrderDetailVO detailVO = this.orderQueryManager.getModel(orderSn, queryParam);
        // 校验订单权限
        if (!detailVO.getMemberId().equals(buyer.getUid())){
            throw new ServiceException(TradeErrorCode.E460.code(),"操作订单无权限");
        }

        if (detailVO.getNeedReceipt().intValue() == 1) {
            detailVO.setReceiptHistory(memberHistoryReceiptClient.getReceiptHistory(orderSn));
        }

        //货到付款的订单不允许线上支付
        if (PaymentTypeEnum.COD.value().equals(detailVO.getPaymentType())) {
            detailVO.getOrderOperateAllowableVO().setAllowPay(false);
        }

        return detailVO;
    }


    @Operation(summary = "确认收货")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", required = true,   in = ParameterIn.PATH)
    })
    @PostMapping(value = "/{order_sn}/rog")
    public String rog(@Parameter(hidden = true) @PathVariable("order_sn") String orderSn) {

        Buyer buyer = UserContext.getBuyer();
        RogVO rogVO = new RogVO();
        rogVO.setOrderSn(orderSn);
        rogVO.setOperator(buyer.getUsername());

        orderOperateManager.rog(rogVO, OrderPermission.buyer);
        return "";
    }


    @Operation(summary = "取消订单")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "reason", description = "取消原因", required = true,   in = ParameterIn.QUERY),
    })
    @PostMapping(value = "/{order_sn}/cancel")
    public String cancel(@Parameter(hidden = true) @PathVariable("order_sn") String orderSn, String reason) {

        Buyer buyer = UserContext.getBuyer();
        CancelVO cancelVO = new CancelVO();
        cancelVO.setOperator(buyer.getUsername());
        cancelVO.setOrderSn(orderSn);
        cancelVO.setReason(reason);
        orderOperateManager.cancel(cancelVO, OrderPermission.buyer);


        return "";
    }


    @Operation(summary = "查询订单状态的数量")
    @GetMapping(value = "/status-num")
    public OrderStatusNumVO getStatusNum() {
        Buyer buyer = UserContext.getBuyer();
        OrderStatusNumVO orderStatusNumVO = this.orderQueryManager.getOrderStatusNum(buyer.getUid(), null);
        return orderStatusNumVO;
    }

    @Operation(summary = "查询会员订单列表--im使用")
    @Parameters({
            @Parameter(name = "page_no", description = "页数",   in = ParameterIn.QUERY, required = true),
            @Parameter(name = "page_size", description = "条数",   in = ParameterIn.QUERY, required = true),
            @Parameter(name = "shop_id", description = "条数",  in = ParameterIn.PATH, required = true),
    })
    @GetMapping("/im/{shop_id}")
    public WebPage listByShopId(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @PathVariable("shop_id") Long shopId) {
        //获取当前登录会员信息
        Buyer buyer = UserContext.getBuyer();
        //初始化订单查询参数
        OrderQueryParam param = new OrderQueryParam();
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setMemberId(buyer.getUid());
        param.setSellerId(shopId);
        //查询订单分页列表数据
        return this.orderQueryManager.list(param);
    }

    @Operation(summary = "根据交易编号查询订单列表")
    @Parameters({
            @Parameter(name = "trade_sn", description = "交易编号", required = true,   in = ParameterIn.PATH),
    })
    @GetMapping(value = "/{trade_sn}/list")
    public List<OrderDetailVO> getOrderList(@Parameter(hidden = true) @PathVariable("trade_sn") String tradeSn) {
        Buyer buyer = UserContext.getBuyer();
        List<OrderDetailVO> orderDetailVOList = this.orderQueryManager.getOrderByTradeSn(tradeSn, buyer.getUid());
        return orderDetailVOList;
    }


    @Operation(summary = "根据交易编号或者订单编号查询收银台数据")
    @Parameters({
            @Parameter(name = "trade_sn", description = "交易编号",   in = ParameterIn.QUERY),
            @Parameter(name = "order_sn", description = "订单编号",   in = ParameterIn.QUERY)
    })
    @GetMapping(value = "/cashier")
    public CashierVO getCashier(@Parameter(hidden = true) String tradeSn, @Parameter(hidden = true) String orderSn) {

        //判断查询的订单是否属于当前登录用户  add by liuyulei 2020-01-01
        Buyer buyer = UserContext.getBuyer();
        String sn = tradeSn;
        if (StringUtil.isEmpty(sn)) {
            sn = orderSn;
        }

        //检测交易  订单是否属于当前登录会员
        this.tradeQueryManager.checkIsOwner(sn, buyer.getUid());


        String shipName, shipAddr, shipMobile, shipTel, shipProvince, shipCity, shipCounty, shipTown, payTypeText;
        Double needPayPrice;

        if (tradeSn != null) {

            TradeDO tradeDO = this.tradeQueryManager.getModel(tradeSn);
            shipName = tradeDO.getConsigneeName();
            shipAddr = tradeDO.getConsigneeAddress();
            shipMobile = tradeDO.getConsigneeMobile();
            shipTel = tradeDO.getConsigneeTelephone();
            shipProvince = tradeDO.getConsigneeProvince();
            shipCity = tradeDO.getConsigneeCity();
            shipCounty = tradeDO.getConsigneeCounty();
            shipTown = tradeDO.getConsigneeTown();
            needPayPrice = tradeDO.getTotalPrice();
            payTypeText = tradeDO.getPaymentType();

        } else if (orderSn != null) {

            OrderDetailQueryParam queryParam = new OrderDetailQueryParam();
            queryParam.setBuyerId(UserContext.getBuyer().getUid());

            OrderDetailVO detailVO = this.orderQueryManager.getModel(orderSn, queryParam);
            shipName = detailVO.getShipName();
            shipAddr = detailVO.getShipAddr();
            shipMobile = detailVO.getShipMobile();
            shipTel = detailVO.getShipTel();
            shipProvince = detailVO.getShipProvince();
            shipCity = detailVO.getShipCity();
            shipCounty = detailVO.getShipCounty();
            shipTown = detailVO.getShipTown();
            needPayPrice = detailVO.getNeedPayMoney();
            payTypeText = detailVO.getPaymentType();

        } else {
            throw new ServiceException(TradeErrorCode.E455.code(), "参数错误");
        }

        MemberDepositeVO depositeVO = this.depositeClient.getDepositeVO(buyer.getUid());

        CashierVO cashierVO = new CashierVO();
        cashierVO.setShipProvince(shipProvince);
        cashierVO.setShipCity(shipCity);
        cashierVO.setShipCounty(shipCounty);
        cashierVO.setShipTown(shipTown);
        cashierVO.setShipAddr(shipAddr);
        cashierVO.setShipMobile(shipMobile);
        cashierVO.setShipName(shipName);
        cashierVO.setNeedPayPrice(needPayPrice);
        cashierVO.setShipTel(shipTel);
        cashierVO.setPayTypeText(payTypeText);
        cashierVO.setDeposite(depositeVO);
        return cashierVO;
    }


    @Operation(summary = "订单流程图数据")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单sn", required = true,   in = ParameterIn.PATH),
    })
    @GetMapping(value = "/{order_sn}/flow")
    public List<OrderFlowNode> getOrderStatusFlow(@Parameter(hidden = true) @PathVariable(name = "order_sn") String orderSn) {
        List<OrderFlowNode> orderFlowList = this.orderQueryManager.getOrderFlow(orderSn);
        return orderFlowList;
    }

    @Operation(summary = "查询订单日志")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", required = true,   in = ParameterIn.PATH)
    })
    @GetMapping(value = "/{order_sn}/log")
    public List<OrderLogDO> getList(@Parameter(hidden = true) @PathVariable("order_sn") String orderSn) {
        List<OrderLogDO> logDOList = this.orderLogManager.listAll(orderSn);
        return logDOList;
    }

    @Operation(summary = "查询支付剩余时间")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "trade_type", description = "交易类型ORDER，TRADE", required = true,   in = ParameterIn.QUERY)
    })
    @GetMapping(value = "/{order_sn}/cancel-left-time")
    public Long getCancelLeftTime(@Parameter(hidden = true) @PathVariable("order_sn") String orderSn, @Parameter(hidden = true) String tradeType) {
        Long leftTime = this.orderQueryManager.getCancelLeftTime(orderSn, tradeType);
        return leftTime;
    }

}
