/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.aftersale;

import cn.shoptnt.model.aftersale.dos.AfterSaleLogDO;
import cn.shoptnt.service.aftersale.AfterSaleLogManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

/**
 * 售后服务日志相关API
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-11-27
 */
@Tag(name ="售后服务相关API")
@RestController
@RequestMapping("/buyer/after-sales/log")
@Validated
public class AfterSaleLogBuyerController {

    @Autowired
    private AfterSaleLogManager afterSaleLogManager;

    @Operation(summary = "获取售后服务操作日志信息")
    @Parameters({
            @Parameter(name = "service_sn", description = "售后服务单号", required = true,   in = ParameterIn.PATH)
    })
    @GetMapping(value = "/{service_sn}")
    public List<AfterSaleLogDO> list(@PathVariable("service_sn") String serviceSn){

        return afterSaleLogManager.list(serviceSn);
    }
}
