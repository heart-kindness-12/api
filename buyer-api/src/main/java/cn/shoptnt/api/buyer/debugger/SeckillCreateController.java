/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.debugger;

import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.exception.SystemErrorCodeV1;
import cn.shoptnt.framework.trigger.Interface.TimeTrigger;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.mapper.promotion.PromotionGoodsMapper;
import cn.shoptnt.mapper.promotion.seckill.SeckillApplyMapper;
import cn.shoptnt.mapper.promotion.seckill.SeckillMapper;
import cn.shoptnt.model.base.message.PromotionScriptMsg;
import cn.shoptnt.framework.message.TimeExecute;
import cn.shoptnt.model.errorcode.PromotionErrorCode;
import cn.shoptnt.model.goods.vo.CacheGoods;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import cn.shoptnt.model.promotion.seckill.dos.SeckillApplyDO;
import cn.shoptnt.model.promotion.seckill.dos.SeckillDO;
import cn.shoptnt.model.promotion.seckill.enums.SeckillGoodsApplyStatusEnum;
import cn.shoptnt.model.promotion.seckill.enums.SeckillStatusEnum;
import cn.shoptnt.model.promotion.seckill.vo.SeckillGoodsVO;
import cn.shoptnt.model.promotion.seckill.vo.SeckillVO;
import cn.shoptnt.model.promotion.tool.dos.PromotionGoodsDO;
import cn.shoptnt.model.promotion.tool.dto.PromotionDetailDTO;
import cn.shoptnt.model.promotion.tool.dto.PromotionPriceDTO;
import cn.shoptnt.model.promotion.tool.enums.PromotionTypeEnum;
import cn.shoptnt.model.promotion.tool.enums.ScriptOperationTypeEnum;
import cn.shoptnt.model.system.enums.DeleteStatusEnum;
import cn.shoptnt.service.goods.GoodsSkuManager;
import cn.shoptnt.service.promotion.seckill.SeckillManager;
import cn.shoptnt.service.promotion.seckill.SeckillRangeManager;
import cn.shoptnt.service.promotion.seckill.SeckillScriptManager;
import cn.shoptnt.service.promotion.tool.PromotionGoodsManager;
import cn.shoptnt.service.promotion.tool.support.PromotionCacheKeys;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/debugger/seckill")
@ConditionalOnProperty(value = "shoptnt.debugger", havingValue = "true")
public class SeckillCreateController {

    @Autowired
    private SeckillManager seckillManager;

    @Autowired
    private GoodsSkuManager goodsSkuManager;

    @Autowired
    private PromotionGoodsMapper promotionGoodsMapper;

    @Autowired
    private SeckillMapper seckillMapper;

    @Autowired
    private SeckillApplyMapper seckillApplyMapper;

    @Autowired
    private SeckillRangeManager seckillRangeManager;

    @Autowired
    private TimeTrigger timeTrigger;

    @Autowired
    private PromotionGoodsManager promotionGoodsManager;

    @Autowired
    private SeckillScriptManager seckillScriptManager;

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private Cache cache;

    @Operation(summary = "添加限时抢购入库")
    @GetMapping
    @Transactional(value = "tradeTransactionManager",propagation = Propagation.REQUIRED, rollbackFor = {RuntimeException.class, ServiceException.class})
    public SeckillVO add(@NotNull String startTime,@NotNull String endTime,@NotNull String times,@NotNull String skuIds) {
        SeckillVO seckill = new SeckillVO();
        seckill.setApplyEndTime(DateUtil.getDateline(endTime,"yyyy-MM-dd HH:mm:ss"));
        seckill.setStartDay(DateUtil.getDateline(startTime,"yyyy-MM-dd"));
        seckill.setSeckillName("测试" + startTime);
        seckill.setRangeList(getRangeList(times));
        seckill.setSeckillStatus(SeckillStatusEnum.RELEASE.name());
        //验证活动名称是否为空
        this.checkName(seckill.getSeckillName(), null);


        SeckillDO seckillDO = new SeckillDO();
        seckillDO.setDeleteStatus(DeleteStatusEnum.NORMAL.value());
        BeanUtils.copyProperties(seckill, seckillDO);
        seckillMapper.insert(seckillDO);

        Long id = seckillDO.getSeckillId();

        this.seckillRangeManager.addList(seckill.getRangeList(), id);

        //开启延时任务执行器
        openTimeExecuter(seckill, DateUtil.getDateline(endTime,"yyyy-MM-dd HH:mm:ss"), id);
        seckill.setSeckillId(id);
        addSeckillGoods(seckill,skuIds);
        return seckill;
    }


    @Transactional(value = "tradeTransactionManager",propagation = Propagation.REQUIRED, rollbackFor = {RuntimeException.class, ServiceException.class})
    void addSeckillGoods(SeckillVO seckill, String skuIds){
        List<String> skuIdList = this.getSkuIdList(skuIds,";");
        for (int i = 0; i < skuIdList.size(); i++) {
            List<String> skuidList = this.getSkuIdList(skuIdList.get(i),",");
            for (String skuidStr: skuidList) {
                Long skuid = StringUtil.toLong(skuidStr,0);
                GoodsSkuVO goodsSkuVO = goodsSkuManager.getSkuFromCache(skuid);
                if (goodsSkuVO == null ){
                    continue;
                }
                SeckillApplyDO seckillApplyDO = new SeckillApplyDO();
                seckillApplyDO.setSeckillId(seckill.getSeckillId());
                seckillApplyDO.setGoodsId(goodsSkuVO.getGoodsId());
                seckillApplyDO.setSkuId(skuid);
                seckillApplyDO.setGoodsName(goodsSkuVO.getGoodsName());
                seckillApplyDO.setOriginalPrice(goodsSkuVO.getPrice());
                seckillApplyDO.setPrice(0.01);
                seckillApplyDO.setStatus(SeckillGoodsApplyStatusEnum.PASS.name());
                seckillApplyDO.setSellerId(goodsSkuVO.getSellerId());
                seckillApplyDO.setShopName(goodsSkuVO.getSellerName());
                seckillApplyDO.setSalesNum(0);
                seckillApplyDO.setSpecs(goodsSkuVO.getSpecs());
                seckillApplyDO.setSoldQuantity(1);
                seckillApplyDO.setStartDay(seckill.getStartDay());
                seckillApplyDO.setTimeLine(seckill.getRangeList().get(i));
                Long skuId = seckillApplyDO.getSkuId();
                //判断参加活动的数量和库存数量
                if (seckillApplyDO.getSoldQuantity() > goodsSkuVO.getEnableQuantity()) {
                    throw new ServiceException(PromotionErrorCode.E402.code(), seckillApplyDO.getGoodsName() + ",此商品库存不足");
                }

                /**
                 * *************两种情况：******************
                 * 团购时间段：      |________________|
                 * 秒杀时间段：  |_____|           |_______|
                 *
                 * ************第三种情况：******************
                 * 团购时间段：        |______|
                 * 秒杀时间段：   |________________|
                 *
                 * ************第四种情况：******************
                 * 团购时间段：   |________________|
                 * 秒杀时间段：        |______|
                 *
                 */
                //这个商品的开始时间计算要用他参与的时间段来计算，结束时间是当天晚上23：59：59
                String date = DateUtil.toString(seckill.getStartDay(), "yyyy-MM-dd");
                long startTime = DateUtil.getDateline(date + " " + seckillApplyDO.getTimeLine() + ":00:00", "yyyy-MM-dd HH:mm:ss");
                long endTime = DateUtil.getDateline(date + " 23:59:59", "yyyy-MM-dd HH:mm:ss");

                int count = promotionGoodsMapper.selectCountByTime(skuId, startTime, endTime);

                if (count > 0) {
                    throw new ServiceException(PromotionErrorCode.E400.code(), "商品[" + goodsSkuVO.getGoodsName() + "]已经在重叠的时间段参加了团购活动，不能参加限时抢购活动");
                }
                seckillApplyMapper.insert(seckillApplyDO);
                long applyId = seckillApplyDO.getApplyId();
                seckillApplyDO.setApplyId(applyId);
                this.seckillManager.sellerApply(goodsSkuVO.getSellerId(), seckill.getSeckillId());
                //参与限时抢购促销活动并且已被平台审核通过的商品集合
                List<SeckillApplyDO> goodsList = new ArrayList<>();
                //审核通过的限时抢购商品集合
                List<PromotionGoodsDO> promotionGoodsDOS = new ArrayList<>();
                Long actId = 0L;

                SeckillApplyDO apply = seckillApplyDO;

                //查询商品
                CacheGoods goods = goodsClient.getFromCache(apply.getGoodsId());
                    //将审核通过的商品放入集合中
                    goodsList.add(apply);

                    //促销商品表
                    PromotionGoodsDO promotion = new PromotionGoodsDO();
                    promotion.setTitle("限时抢购");
                    promotion.setGoodsId(apply.getGoodsId());
                    promotion.setSkuId(apply.getSkuId());
                    promotion.setPromotionType(PromotionTypeEnum.SECKILL.name());
                    promotion.setActivityId(apply.getSeckillId());
                    promotion.setNum(apply.getSoldQuantity());
                    promotion.setPrice(apply.getPrice());
                    promotion.setSellerId(goods.getSellerId());
                    promotion.setStartTime(startTime);
                    promotion.setEndTime(endTime);
                    promotionGoodsDOS.add(promotion);


                    //从缓存读取限时抢购的活动的商品
                    String redisKey = getRedisKey(apply.getStartDay());
                    Map<Integer, List<SeckillGoodsVO>> map = this.cache.getHash(redisKey);
                    //如果redis中有当前审核商品参与的限时抢购活动商品信息，就删除掉
                    if (map != null && !map.isEmpty()) {
                        this.cache.remove(redisKey);
                    }

                    //设置延迟加载任务，到活动开始时间后将搜索引擎中的优惠价格设置为0
                    PromotionPriceDTO promotionPriceDTO = new PromotionPriceDTO();
                    promotionPriceDTO.setGoodsId(apply.getGoodsId());
                    promotionPriceDTO.setPrice(apply.getPrice());
                    timeTrigger.add(TimeExecute.PROMOTION_EXECUTER, promotionPriceDTO, startTime, null);
                    //此活动结束后将索引的优惠价格重置为0
                    promotionPriceDTO.setPrice(0.0);
                    timeTrigger.add(TimeExecute.PROMOTION_EXECUTER, promotionPriceDTO, endTime, null);

                //活动信息DTO
                PromotionDetailDTO detailDTO = new PromotionDetailDTO();
                detailDTO.setActivityId(seckill.getSeckillId());
                detailDTO.setStartTime(startTime);
                detailDTO.setEndTime(endTime);
                detailDTO.setPromotionType(PromotionTypeEnum.SECKILL.name());
                detailDTO.setTitle("限时抢购");
                this.promotionGoodsManager.addAndCheck(promotionGoodsDOS, detailDTO);
                //创建审核通过的商品限时抢购促销活动脚本信息
                this.seckillScriptManager.createCacheScript(goodsList.get(0).getSeckillId(), goodsList);
            }

        }
    }





    /**
     * 转换商品ID数据
     * @param skuIds
     * @return
     */
    private List<String> getSkuIdList(String skuIds,String split){
        if (StringUtil.isEmpty(skuIds)) {
            throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER,"参数错误");
        }
        String[] timesS = skuIds.split(split);
        List<String> list = new ArrayList<>();

        for (int i = 0; i < timesS.length; i++) {
            list.add(timesS[i]);
        }
        return list;
    }

    /**
     * 转换时刻数据结构
     * @param times
     * @return
     */
    private List<Integer> getRangeList(String times){
        if (StringUtil.isEmpty(times)) {
            throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER,"参数错误");
        }
        String[] timesS = times.split(",");
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < timesS.length; i++) {
            list.add(StringUtil.toInt(timesS[i],true));
        }
        return list;
    }

    /**
     * 验证活动名称是否重复
     *
     * @param name
     * @param seckillId
     */
    private void checkName(String name, Long seckillId) {

        List<SeckillDO> list = new QueryChainWrapper<>(seckillMapper)
                //拼接活动名称查询条件
                .eq("seckill_name", name)
                //id不为空
                .ne(seckillId != null, "seckill_id", seckillId)
                //列表查询
                .list();

//        if (list.size() > 0) {
//            throw new ServiceException(PromotionErrorCode.E400.code(), "活动名称重复");
//        }

    }

    /**
     * 开启延时任务执行器
     *
     * @param seckill 限时抢购信息
     * @param endTime 活动结束时间
     * @param id      限时抢购活动ID
     */
    private void openTimeExecuter(SeckillVO seckill, long endTime, Long id) {
        //如果活动状态是已发布
        if (SeckillStatusEnum.RELEASE.value().equals(seckill.getSeckillStatus())) {
            //启用延时任务,限时抢购促销活动结束时删除脚本信息
            PromotionScriptMsg promotionScriptMsg = new PromotionScriptMsg();
            promotionScriptMsg.setPromotionId(id);
            promotionScriptMsg.setPromotionName(seckill.getSeckillName());
            promotionScriptMsg.setPromotionType(PromotionTypeEnum.SECKILL);
            promotionScriptMsg.setOperationType(ScriptOperationTypeEnum.DELETE);
            promotionScriptMsg.setEndTime(endTime);
            String uniqueKey = "{TIME_TRIGGER_" + PromotionTypeEnum.SECKILL.name() + "}_" + id;
            timeTrigger.add(TimeExecute.SECKILL_SCRIPT_EXECUTER, promotionScriptMsg, endTime, uniqueKey);
        }
    }

    /**
     * 获取限时抢购key
     *
     * @param dateline
     * @return
     */
    private String getRedisKey(long dateline) {
        return PromotionCacheKeys.getSeckillKey(DateUtil.toString(dateline, "yyyyMMdd"));
    }

}
