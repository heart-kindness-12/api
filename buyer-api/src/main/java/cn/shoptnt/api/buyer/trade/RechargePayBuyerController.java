/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.model.payment.dto.PayParam;
import cn.shoptnt.service.trade.order.OrderPayManager;
import cn.shoptnt.model.trade.deposite.RechargeDO;
import cn.shoptnt.service.trade.deposite.RechargeManager;
import cn.shoptnt.model.trade.order.enums.TradeTypeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Map;

/**
 * @description: 预存款充值
 * @author: liuyulei
 * @create: 2019-12-30 19:53
 * @version:1.0
 * @since:7.1.4
 **/
@Tag(name = "预存款充值相关API")
@RestController
@RequestMapping("/buyer/recharge")
@Validated
public class RechargePayBuyerController {

    @Autowired
    private RechargeManager rechargeManager;

    @Autowired
    private OrderPayManager orderPayManager;

    @PostMapping
    @Operation(summary	= "创建充值订单")
    @Parameters({
            @Parameter(name = "price", description = "充值金额", required = true,  in = ParameterIn.QUERY)
    })
    public RechargeDO create(@Max(value = 10000,message = "充值金额输入有误，单次最多允许充值10000元") @Min(value = 1, message = "充值金额有误，单次最少充值金额为1元") Double price)	{
        return this.rechargeManager.recharge(price);
    }


    @PostMapping(value = "/{sn}")
    @Operation(summary	= "支付充值订单")
    @Parameters({
            @Parameter(name = "sn", description = "充值订单编号", required = true,   in = ParameterIn.PATH)
    })
    public Map pay(@PathVariable(name = "sn") String sn,  @Validated PayParam payParam)	{
        payParam.setSn(sn);
        payParam.setTradeType(TradeTypeEnum.RECHARGE.name());
        return orderPayManager.pay(payParam);
    }

}
