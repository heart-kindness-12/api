package cn.shoptnt.api;

import cn.shoptnt.framework.openapi.GlobalOperationCustomizer;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * OpenAPI3.0 分组配置
 *
 * @author gy
 * @version 1.0
 * @sinc 7.3.0
 * @date 2022年12月08日 17:04
 * Knife4j 文档访问地址为：http://ip:port/doc.html  推荐使用
 * swagger3 访问地址为：  http://ip:port/swagger-ui/index.html
 */
@Configuration
public class SellerOpenApiConfig {


    private static final String PACKAGES_PATH = "cn.shoptnt.api.seller";

    @Bean
    public GroupedOpenApi defaultApi() {
        return GroupedOpenApi.builder()
                .group("默认分组")
                .pathsToMatch("/**")
                .packagesToScan("cn.shoptnt.api")
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi afterSaleSellerApi() {

        return GroupedOpenApi.builder()
                .group("商家端-售后模块")
                .pathsToMatch("/seller/after-sales/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();

    }

    @Bean
    public GroupedOpenApi goodsSellerApi() {

        return GroupedOpenApi.builder()
                .group("商家端-商品模块")
                .pathsToMatch("/seller/goods/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi memberSellerApi() {

        return GroupedOpenApi.builder()
                .group("商家端-会员模块")
                .pathsToMatch("/seller/members/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();

    }

    @Bean
    public GroupedOpenApi orderBillSellerApi() {

        return GroupedOpenApi.builder()
                .group("商家端-结算模块")
                .pathsToMatch("/seller/order/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();


    }

    @Bean
    public GroupedOpenApi promotionSellerApi() {


        return GroupedOpenApi.builder()
                .group("商家端-促销模块")
                .pathsToMatch("/seller/promotion/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi shopSellerApi() {

        return GroupedOpenApi.builder()
                .group("商家端-店铺模块")
                .pathsToMatch("/seller/shops/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();


    }

    @Bean
    public GroupedOpenApi passportSellerApi() {
        return GroupedOpenApi.builder()
                .group("商家端-商家认证中心")
                .pathsToMatch("/seller/login/**", "/seller/register/**", "/seller/check/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi tradeSellerApi() {


        return GroupedOpenApi.builder()
                .group("商家端-交易模块")
                .pathsToMatch("/seller/trade/**", "/seller/waybill/**", "/seller/express/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi statisticsSellerApi() {

        return GroupedOpenApi.builder()
                .group("商家端-统计模块")
                .pathsToMatch("/seller/statistics/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();

    }

    @Bean
    public GroupedOpenApi distributionSellerApi() {

        return GroupedOpenApi.builder()
                .group("商家端-分销")
                .pathsToMatch("/seller/distribution/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();

    }
    @Bean
    public GroupedOpenApi i18nSellerApi() {
        return GroupedOpenApi.builder()
                .group("商家端-国际化模块")
                .pathsToMatch("/seller/i18n/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

}
