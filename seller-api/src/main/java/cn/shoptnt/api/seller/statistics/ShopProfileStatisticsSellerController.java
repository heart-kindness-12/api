/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.statistics;

import cn.shoptnt.model.statistics.vo.ShopProfileVO;
import cn.shoptnt.model.statistics.vo.SimpleChart;
import cn.shoptnt.service.statistics.ShopProfileStatisticsManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商家中心统计，店铺概况
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018年4月18日上午11:30:50
 */
@Tag(name = "商家统计 店铺概况")
@RestController
@RequestMapping("/seller/statistics/shop_profile")
public class ShopProfileStatisticsSellerController {

    @Autowired
    private ShopProfileStatisticsManager shopProfileStatisticsManager;

    @Operation(summary = "获取30天店铺概况展示数据")
    @GetMapping("/data")
    public ShopProfileVO getLast30dayStatus() {
        return this.shopProfileStatisticsManager.data();
    }

    @Operation(summary = "获取30天店铺下单金额统计图数据")
    @GetMapping("/chart")
    public SimpleChart getLast30dayLineChart() {
        return this.shopProfileStatisticsManager.chart();
    }

}
