/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.goods;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goods.dos.DraftGoodsDO;
import cn.shoptnt.model.goods.dos.GoodsDO;
import cn.shoptnt.model.goods.dto.GoodsDTO;
import cn.shoptnt.model.goods.vo.DraftGoodsVO;
import cn.shoptnt.model.goods.vo.GoodsParamsGroupVO;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import cn.shoptnt.service.goods.DraftGoodsManager;
import cn.shoptnt.service.goods.DraftGoodsParamsManager;
import cn.shoptnt.service.goods.DraftGoodsSkuManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 草稿商品控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-03-26 10:40:34
 */
@RestController
@RequestMapping("/seller/goods/draft-goods")
@Tag(name = "草稿商品相关API")
public class DraftGoodsSellerController {

    @Autowired
    private DraftGoodsManager draftGoodsManager;
    @Autowired
    private DraftGoodsParamsManager draftGoodsParamsManager;
    @Autowired
    private DraftGoodsSkuManager draftGoodsSkuManager;


    @Operation(summary = "查询草稿商品列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "keyword", description = "关键字查询", in = ParameterIn.QUERY),
            @Parameter(name = "shop_cat_path", description = "店铺分类path，如0|10|", in = ParameterIn.QUERY),
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) @NotEmpty(message = "页码不能为空") Long pageNo,
                        @Parameter(hidden = true) @NotEmpty(message = "每页数量不能为空") Long pageSize,
                        String keyword, @Parameter(hidden = true) String shopCatPath) {

        return this.draftGoodsManager.list(pageNo, pageSize, keyword, shopCatPath);
    }

    @Operation(summary = "添加商品")
    @PostMapping
    public DraftGoodsDO add(@Parameter(hidden = true) @RequestBody GoodsDTO goods) {

        DraftGoodsDO draftGoods = this.draftGoodsManager.add(goods);

        return draftGoods;
    }

    @PutMapping(value = "/{draft_goods_id}")
    @Operation(summary = "修改草稿商品")
    @Parameters({
            @Parameter(name = "draft_goods_id", description = "主键", required = true, in = ParameterIn.PATH),
            @Parameter(name = "goods", description = "商品信息", required = true)
    })
    public DraftGoodsDO edit(@RequestBody GoodsDTO goods, @PathVariable("draft_goods_id") Long draftGoodsId) {

        DraftGoodsDO draftGoods = this.draftGoodsManager.edit(goods, draftGoodsId);

        return draftGoods;
    }


    @DeleteMapping(value = "/{draft_goods_ids}")
    @Operation(summary = "删除草稿商品")
    @Parameters({
            @Parameter(name = "draft_goods_ids", description = "要删除的草稿商品主键", required = true, in = ParameterIn.PATH)
    })
    public String delete(@PathVariable("draft_goods_ids") Long[] draftGoodsIds) {

        this.draftGoodsManager.delete(draftGoodsIds);

        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个草稿商品,商家编辑草稿商品使用")
    @Parameters({
            @Parameter(name = "id", description = "要查询的草稿商品主键", required = true, in = ParameterIn.PATH)
    })
    public DraftGoodsVO get(@PathVariable Long id) {
        return this.draftGoodsManager.getVO(id);
    }

    @GetMapping(value = "/{draft_goods_id}/params")
    @Operation(summary = "查询草稿商品关联的参数，包括没有添加的参数")
    @Parameters({
            @Parameter(name = "draft_goods_id", description = "要删除的草稿商品主键", required = true, in = ParameterIn.PATH)
    })
    public List<GoodsParamsGroupVO> queryDraftParam(@PathVariable("draft_goods_id") Long draftGoodsId) {

        DraftGoodsDO draftGoods = this.draftGoodsManager.getModel(draftGoodsId);

        List<GoodsParamsGroupVO> list = draftGoodsParamsManager.getParamByCatAndDraft(draftGoods.getCategoryId(), draftGoodsId);

        return list;

    }

    @Operation(summary = "查询草稿箱商品sku信息")
    @Parameter(name = "draft_goods_id", description = "草稿商品id", required = true, in = ParameterIn.PATH)
    @GetMapping(value = "/{draft_goods_id}/skus")
    public List<GoodsSkuVO> queryByDraftGoodsid(@PathVariable("draft_goods_id") Long draftGoodsId) {

        List<GoodsSkuVO> list = draftGoodsSkuManager.getSkuList(draftGoodsId);

        return list;
    }

    @Operation(summary = "修改草稿箱商品sku信息")
    @Parameter(name = "draft_goods_id", description = "草稿商品id", required = true, in = ParameterIn.PATH)
    @PutMapping(value = "/{draft_goods_id}/skus")
    public List<GoodsSkuVO> updateByDraftGoodsid(@PathVariable("draft_goods_id") Long draftGoodsId, @RequestBody List<GoodsSkuVO> skuList) {

        draftGoodsSkuManager.add(skuList, draftGoodsId);

        return skuList;
    }

    @Operation(summary = "草稿箱商品上架接口")
    @Parameters({
            @Parameter(name = "draft_goods_id", description = "草稿商品id", required = true, in = ParameterIn.PATH),
            @Parameter(name = "goods", description = "商品信息", required = true)
    })
    @PutMapping(value = "/{draft_goods_id}/market")
    public GoodsDO addMarket(@Parameter(hidden = true) @Valid @RequestBody GoodsDTO goodsVO, @PathVariable("draft_goods_id") Long draftGoodsId) {

        GoodsDO goods = draftGoodsManager.addMarket(goodsVO, draftGoodsId);

        return goods;
    }


}
