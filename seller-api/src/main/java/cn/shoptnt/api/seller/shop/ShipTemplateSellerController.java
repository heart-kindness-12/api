/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.shop;

import cn.shoptnt.model.shop.dos.ShipTemplateDO;
import cn.shoptnt.model.shop.vo.ShipTemplateSellerVO;
import cn.shoptnt.model.shop.vo.ShipTemplateVO;
import cn.shoptnt.model.shop.vo.operator.SellerEditShop;
import cn.shoptnt.service.shop.ShipTemplateManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.security.model.Seller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

/**
 * 运费模版控制器
 *
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-28 21:44:49
 */
@RestController
@RequestMapping("/seller/shops/ship-templates")
@Tag(name = "运费模版相关API")
@Validated
public class ShipTemplateSellerController {

    @Autowired
    private ShipTemplateManager shipTemplateManager;


    @Operation(summary = "查询运费模版列表")
    @GetMapping
    public List<ShipTemplateSellerVO> list() {
        return this.shipTemplateManager.getStoreTemplate(UserContext.getSeller().getSellerId());
    }


    @Operation(summary = "添加运费模版")
    @PostMapping
    public ShipTemplateDO add(@Valid @RequestBody ShipTemplateSellerVO shipTemplate) {

        return this.shipTemplateManager.save(shipTemplate);
    }

    @PutMapping(value = "/{template_id}")
    @Operation(summary = "修改运费模版")
    @Parameter(name = "template_id", description = "模版id", required = true,  in = ParameterIn.PATH)
    public ShipTemplateDO edit(@Valid @RequestBody ShipTemplateSellerVO shipTemplate, @Parameter(hidden = true) @PathVariable("template_id") Long templateId) {
        shipTemplate.setId(templateId);
        return this.shipTemplateManager.edit(shipTemplate);
    }


    @DeleteMapping(value = "/{template_id}")
    @Operation(summary = "删除运费模版")
    @Parameters({
            @Parameter(name = "template_id", description = "要删除的运费模版主键", required = true,  in = ParameterIn.PATH)
    })
    public SellerEditShop delete(@Parameter(hidden = true) @PathVariable("template_id") Long templateId) {
        Seller seller = UserContext.getSeller();
        SellerEditShop sellerEditShop = new SellerEditShop();
        sellerEditShop.setSellerId(seller.getSellerId());
        sellerEditShop.setOperator("删除店铺运费模版");
        this.shipTemplateManager.delete(templateId);
        return sellerEditShop;
    }

    @GetMapping(value = "/{template_id}")
    @Operation(summary = "查询一个运费模版")
    @Parameters({
            @Parameter(name = "template_id", description = "要查询的运费模版主键", required = true,  in = ParameterIn.PATH)
    })
    public ShipTemplateSellerVO get(@Parameter(hidden = true) @PathVariable("template_id") Long templateId) {

        ShipTemplateSellerVO shipTemplate = this.shipTemplateManager.getFromDB(templateId);

        return shipTemplate;
    }

}
