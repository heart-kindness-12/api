/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.promotion;

import cn.shoptnt.model.promotion.halfprice.dos.HalfPriceDO;
import cn.shoptnt.model.promotion.halfprice.vo.HalfPriceVO;
import cn.shoptnt.service.promotion.halfprice.HalfPriceManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.model.util.PromotionValid;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.NoPermissionException;
import cn.shoptnt.framework.security.model.Seller;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.Valid;

/**
 * 第二件半价促销活动控制器
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-23 19:53:42
 */
@SuppressWarnings("Duplicates")
@RestController
@RequestMapping("/seller/promotion/half-prices")
@Tag(name = "第二件半价促销活动相关API")
@Validated
public class HalfPriceSellerController {

    @Autowired
    private HalfPriceManager halfPriceManager;

    @Operation(summary = "查询第二件半价促销活动分页数据列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", in = ParameterIn.QUERY),
            @Parameter(name = "keywords", description = "查询关键字", in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage<HalfPriceVO> list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String keywords) {
        return this.halfPriceManager.list(pageNo, pageSize, keywords);
    }

    @Operation(summary = "添加第二件半价促销活动信息")
    @PostMapping
    public HalfPriceVO add(@Valid @RequestBody HalfPriceVO halfPrice) {
        //参数验证 活动时间和参与活动商品的验证
        PromotionValid.paramValid(halfPrice.getStartTime(), halfPrice.getEndTime(),
                halfPrice.getRangeType(), halfPrice.getGoodsList());
        //获取当前登录的商家信息
        Seller seller = UserContext.getSeller();
        //获取商家ID
        Long sellerId = seller.getSellerId();
        //为活动设置商家ID
        halfPrice.setSellerId(sellerId);
        //添加第二件半价促销活动信息
        this.halfPriceManager.add(halfPrice);
        return halfPrice;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改第二件半价促销活动信息")
    @Parameters({
            @Parameter(name = "id", description = "第二件半价促销活动主键ID", required = true, in = ParameterIn.PATH)
    })
    public HalfPriceVO edit(@Valid @RequestBody HalfPriceVO halfPrice, @PathVariable Long id) {
        //参数验证 活动时间和参与活动商品的验证
        PromotionValid.paramValid(halfPrice.getStartTime(), halfPrice.getEndTime(),
                halfPrice.getRangeType(), halfPrice.getGoodsList());
        //设置主键ID
        halfPrice.setHpId(id);
        //操作权限校验（如果当前修改的第二件半价活动不属于当前操作的商家则不允许操作）
        this.halfPriceManager.verifyAuth(id);
        //修改第二件半价促销活动信息
        this.halfPriceManager.edit(halfPrice, id);
        return halfPrice;
    }

    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除第二件半价促销活动信息")
    @Parameters({
            @Parameter(name = "id", description = "要删除的第二件半价促销活动主键ID", required = true, in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        //操作权限校验（如果当前删除的第二件半价活动不属于当前操作的商家则不允许操作）
        this.halfPriceManager.verifyAuth(id);
        //删除第二件半价促销活动信息
        this.halfPriceManager.delete(id);
        return "";
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个第二件半价促销活动信息")
    @Parameters({
            @Parameter(name = "id", description = "要查询的第二件半价活动主键ID", required = true, in = ParameterIn.PATH)
    })
    public HalfPriceVO get(@PathVariable Long id) {
        //根据ID获取第二件半价促销活动信息
        HalfPriceVO halfPrice = this.halfPriceManager.getFromDB(id);
        //获取当前登录的商家信息
        Seller seller = UserContext.getSeller();
        //验证越权操作
        if (halfPrice == null || !seller.getSellerId().equals(halfPrice.getSellerId())) {
            throw new NoPermissionException("无权操作");
        }

        return halfPrice;
    }

}
