/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.statistics;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.errorcode.StatisticsErrorCode;
import cn.shoptnt.model.statistics.exception.StatisticsException;
import cn.shoptnt.model.statistics.vo.MultipleChart;
import cn.shoptnt.model.statistics.vo.SimpleChart;
import cn.shoptnt.service.statistics.ReportsStatisticsManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import javax.validation.Valid;
import java.util.*;

/**
 * 商家统计 运营报告
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018年4月18日下午4:28:23
 */
@Tag(name = "商家统计 运营报告")
@RestController
@RequestMapping("/seller/statistics/reports")
public class ReportsStatisticsSellerController {

    @Autowired
    private ReportsStatisticsManager reportsStatisticsManager;

    @Operation(summary = "销售统计  下单金额图表数据")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY)})
    @GetMapping("/sales_money")
    public MultipleChart getSalesMoney(@Valid @Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.reportsStatisticsManager.getSalesMoney(searchCriteria);
    }

    @Operation(summary = "销售统计 下单量图表数据")
    @Parameters({
            @Parameter(name = "cycle_type", description = "周期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY)})
    @GetMapping("/sales_num")
    public MultipleChart getSalesNum(@Valid @Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.reportsStatisticsManager.getSalesNum(searchCriteria);
    }

    @Operation(summary = "销售统计  表格数据")
    @Parameters({
            @Parameter(name = "cycle_type", description = "周期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "当前页码", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "页面数据容量", in = ParameterIn.QUERY)})
    @GetMapping("/sales_page")
    public WebPage getSalesPage(@Valid @Parameter(hidden = true) SearchCriteria searchCriteria, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        return this.reportsStatisticsManager.getSalesPage(searchCriteria, pageNo, pageSize);
    }

    @Operation(summary = "销售统计  数据小结")
    @Parameters({
            @Parameter(name = "cycle_type", description = "周期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY)})
    @GetMapping("/sales_summary")
    public Map getSalesSummary(@Valid @Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.reportsStatisticsManager.getSalesSummary(searchCriteria);
    }

    @Operation(summary = "区域分析")
    @Parameters({
            @Parameter(name = "cycle_type", description = "周期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "type", description = "数据类型", in = ParameterIn.QUERY, required = true)
    })
    @GetMapping("/regions/data")
    public List regionMemberMap(@Valid @Parameter(hidden = true) SearchCriteria searchCriteria,@Parameter(hidden = true) String type) {
        if (null == type || "".equals(type)) {
            throw new StatisticsException(StatisticsErrorCode.E801.code(), "请指定需要的数据");
        }
        return this.reportsStatisticsManager.regionsMap(searchCriteria, type);
    }

    @Operation(summary = "购买分析 客单价分布")
    @Parameters({
            @Parameter(name = "cycle_type", description = "周期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "ranges", description = "价格区间，不传有默认值", in = ParameterIn.QUERY)})
    @GetMapping("/purchase/ranges")
    public SimpleChart orderDistribution(@Valid @Parameter(hidden = true) SearchCriteria searchCriteria, @Parameter(hidden = true) @RequestParam(value = "ranges") Integer[] ranges) {

        // 如果为空，设默认值
        if (null == ranges || ranges.length == 0) {
            ranges = new Integer[5];
            ranges[0] = 0;
            ranges[1] = 500;
            ranges[2] = 1000;
            ranges[3] = 1500;
            ranges[4] = 2000;
        }

        // 转为list，去除null，数组转换的集合有限制，只能用这种笨办法
        List<Integer> list = Arrays.asList(ranges);
        List<Integer> newList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) != null) {
                newList.add(list.get(i));
            }
        }
        Integer[] newRanges = newList.toArray(new Integer[newList.size()]);

        if (newRanges.length < 2) {
            throw new StatisticsException(StatisticsErrorCode.E801.code(), "应至少上传两个数字，才可构成价格区间");
        }
        return this.reportsStatisticsManager.orderDistribution(searchCriteria, newRanges);
    }

    @Operation(summary = "购买分析 购买时段分布")
    @Parameters({
            @Parameter(name = "cycle_type", description = "周期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY)})
    @GetMapping("/purchase/period")
    public SimpleChart purchasePeriod(@Valid @Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.reportsStatisticsManager.purchasePeriod(searchCriteria);
    }


}
