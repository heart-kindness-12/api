/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.promotion;

import cn.shoptnt.model.promotion.fulldiscount.dos.FullDiscountDO;
import cn.shoptnt.model.promotion.fulldiscount.vo.FullDiscountVO;
import cn.shoptnt.service.promotion.fulldiscount.FullDiscountManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.model.util.PromotionValid;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.NoPermissionException;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.exception.SystemErrorCodeV1;
import cn.shoptnt.framework.security.model.Seller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

/**
 * 满减满赠促销活动控制器
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-30 17:34:32
 */
@SuppressWarnings("Duplicates")
@RestController
@RequestMapping("/seller/promotion/full-discounts")
@Tag(name = "满减满赠促销活动相关API")
@Validated
public class FullDiscountSellerController	{

	@Autowired
	private FullDiscountManager fullDiscountManager;


	@Operation(summary	= "查询满减满赠促销活动信息分页数据列表")
	@Parameters({
		 @Parameter(name	= "page_no",	description =	"页码", 	in = ParameterIn.QUERY),
		 @Parameter(name	= "page_size",	description =	"每页显示数量",	 	in = ParameterIn.QUERY),
		 @Parameter(name	= "keywords",	description =	"搜索关键字",  	in = ParameterIn.QUERY)
	})
	@GetMapping
	public WebPage<FullDiscountVO> list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String keywords)	{

		return	this.fullDiscountManager.list(pageNo,pageSize,keywords);
	}


	@Operation(summary	= "添加满减满赠促销活动信息")
	@PostMapping
	public FullDiscountVO add(@Valid @RequestBody FullDiscountVO fullDiscountVO)	{
		//验证满减满赠促销活动的参数信息
		this.verifyFullDiscountParam(fullDiscountVO);
		//获取当前登录的商家信息
		Seller seller = UserContext.getSeller();
		//获取商家ID
		Long sellerId = seller.getSellerId();
		//设置活动所属的商家ID
		fullDiscountVO.setSellerId(sellerId);
		//添加满减满赠促销活动
		this.fullDiscountManager.add(fullDiscountVO);
		return fullDiscountVO;
	}

	@PutMapping(value = "/{id}")
	@Operation(summary	= "修改满减满赠促销活动信息")
	@Parameters({
		 @Parameter(name	= "id",	description =	"满减满赠促销活动主键ID",	required = true, 	in = ParameterIn.PATH)
	})
	public FullDiscountVO edit(@Valid @RequestBody FullDiscountVO fullDiscountVO, @PathVariable Long id) {
		//设置活动ID
		fullDiscountVO.setFdId(id);
		//验证满减满赠促销活动的参数信息
		this.verifyFullDiscountParam(fullDiscountVO);
		//校验当前修改的满减满赠活动是否属于当前登录的商家
		this.fullDiscountManager.verifyAuth(id);
		//修改满减满赠促销活动
		this.fullDiscountManager.edit(fullDiscountVO,id);
		return fullDiscountVO;
	}


	@DeleteMapping(value = "/{id}")
	@Operation(summary	= "删除满优惠活动")
	@Parameters({
		 @Parameter(name	= "id",	description =	"要删除的满减满赠促销活动主键ID",	required = true, 	in = ParameterIn.PATH)
	})
	public String delete(@PathVariable Long id) {
		//校验当前删除的满减满赠活动是否属于当前登录的商家
		this.fullDiscountManager.verifyAuth(id);
		//删除满减满赠活动信息
		this.fullDiscountManager.delete(id);
		return "";
	}


	@GetMapping(value =	"/{id}")
	@Operation(summary	= "查询一个满减满赠促销活动信息")
	@Parameters({
		@Parameter(name = "id",	description = "要查询的满减满赠促销活动主键ID",	required = true, 	in = ParameterIn.PATH)
	})
	public FullDiscountVO get(@PathVariable Long id) {
		//根据主键ID获取满减满赠促销活动信息
		FullDiscountVO fullDiscount = this.fullDiscountManager.getModel(id);

		//获取当前登录的商家信息
		Seller seller = UserContext.getSeller();
		//验证越权操作（如果获取的满减满赠促销活动信息不属于当前登录的商家则提示无权操作）
		if (fullDiscount == null || !seller.getSellerId().equals(fullDiscount.getSellerId())){
			throw new NoPermissionException("无权操作");
		}

		return fullDiscount;
	}


	/**
	 * 验证满减满赠促销活动的参数信息
	 * @param fullDiscountVO 满减满赠促销活动信息
	 */
	private void verifyFullDiscountParam(FullDiscountVO fullDiscountVO){
		//验证满减满赠促销活动的活动时间以及商品参与活动的方式
		PromotionValid.paramValid(fullDiscountVO.getStartTime(),fullDiscountVO.getEndTime(),
				fullDiscountVO.getRangeType(),fullDiscountVO.getGoodsList());

		//促销活动的优惠方式不能全部为空，至少要选择一项
		if (fullDiscountVO.getIsFullMinus() == null && fullDiscountVO.getIsFreeShip() == null && fullDiscountVO.getIsSendGift() == null
				&& fullDiscountVO.getIsSendBonus() == null && fullDiscountVO.getIsDiscount() == null) {

			throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER,"请选择优惠方式");
		}

		// 如果促销活动优惠详细是否包含满减不为空
		if (fullDiscountVO.getIsFullMinus() != null && fullDiscountVO.getIsFullMinus() == 1) {

			if (fullDiscountVO.getMinusValue() == null || fullDiscountVO.getMinusValue() == 0) {
				throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER,"请填写减多少元");
			}

			// 减少的现金必须小于优惠门槛
			if (fullDiscountVO.getMinusValue() > fullDiscountVO.getFullMoney()) {
				throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER,"减少的金额不能大于门槛金额");
			}

		}

		// 如果促销活动优惠详细是否包含满送赠品不为空
		if (fullDiscountVO.getIsSendGift() != null && fullDiscountVO.getIsSendGift() == 1) {
			// 赠品id不能为0
			if (fullDiscountVO.getGiftId() == null || fullDiscountVO.getGiftId() == 0) {
				throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER,"请选择赠品");
			}
		}

		// 如果促销活动优惠详细是否包含满送优惠券不为空
		if (fullDiscountVO.getIsSendBonus() != null && fullDiscountVO.getIsSendBonus() == 1) {
			// 优惠券id不能为0
			if (fullDiscountVO.getBonusId() == null || fullDiscountVO.getBonusId() == 0) {
				throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER,"请选择优惠券");
			}
		}

		// 如果促销活动优惠详细是否包含打折不为空
		if (fullDiscountVO.getIsDiscount() != null && fullDiscountVO.getIsDiscount() == 1) {
			// 打折的数值不能为空也不能为0
			if (fullDiscountVO.getDiscountValue() == null || fullDiscountVO.getDiscountValue() == 0) {

				throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER,"请填写打折数值");
			}
			// 打折的数值应介于0-10之间
			if (fullDiscountVO.getDiscountValue() >= 10 || fullDiscountVO.getDiscountValue() <= 0) {
				throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER,"打折的数值应介于0到10之间");
			}
		}

	}
}
