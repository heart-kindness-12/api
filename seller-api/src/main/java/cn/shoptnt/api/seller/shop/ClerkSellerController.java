/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.shop;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.base.SceneType;
import cn.shoptnt.client.system.SmsClient;
import cn.shoptnt.client.system.ValidatorClient;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.service.member.MemberSecurityManager;
import cn.shoptnt.service.passport.PassportManager;
import cn.shoptnt.model.shop.dos.ClerkDO;
import cn.shoptnt.model.shop.dto.ClerkDTO;
import cn.shoptnt.model.shop.vo.ClerkAddVO;
import cn.shoptnt.model.shop.vo.ClerkShowVO;
import cn.shoptnt.model.shop.vo.ClerkVO;
import cn.shoptnt.service.shop.ClerkManager;
import cn.shoptnt.framework.ShopTntConfig;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.exception.NoPermissionException;
import cn.shoptnt.framework.exception.ResourceNotFoundException;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.StringUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

/**
 * 店员控制器
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-08-04 18:48:39
 */
@RestController
@RequestMapping("/seller/shops/clerks")
@Tag(name = "店员相关API")
public class ClerkSellerController {

    @Autowired
    private ClerkManager clerkManager;
    @Autowired
    private MemberManager memberManager;
    @Autowired
    private MemberSecurityManager memberSecurityManager;
    @Autowired
    private ValidatorClient validatorClient;
    @Autowired
    private PassportManager passportManager;
    @Autowired
    private SmsClient smsClient;
    @Autowired
    private ShopTntConfig shoptntConfig;


    @PostMapping(value = "/sms-code/{mobile}")
    @Operation(summary = "发送验证码")
    @Parameters({
            @Parameter(name = "mobile", description = "手机号码", required = true,   in = ParameterIn.PATH)
    })
    public String smsCode(@PathVariable("mobile") String mobile) {
        //参数验证（验证图片验证码或滑动验证参数等）
        this.validatorClient.validate();
        //发送短信验证码
        passportManager.sendSmsCode(mobile);
        //短信验证码失效时间 单位秒
        return shoptntConfig.getSmscodeTimout() / 60 + "";
    }

    @GetMapping("/check/{mobile}")
    @Operation(summary = "对手机号码进行校验")
    @Parameters({
            @Parameter(name = "mobile", description = "手机号码", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "sms_code", description = "短信验证码", required = true,   in = ParameterIn.QUERY)
    })
    public ClerkAddVO checkAddClerkMobile(@PathVariable("mobile")String mobile, @Parameter(hidden = true) String smsCode) {

        //验证手机验证码-添加店员
        boolean bool = smsClient.valid(SceneType.ADD_CLERK.name(), mobile, smsCode);
        if (!bool) {
            throw new ServiceException(MemberErrorCode.E107.code(), "短信验证码错误");
        }
        ClerkAddVO clerkAddVO = new ClerkAddVO();
        //根据用户手机号码查询会员
        Member member = memberManager.getMemberByMobile(mobile);
        if (member != null) {
            //校验要添加的会员是否已经是店主
            if (member.getHaveShop().equals(1)) {
                throw new ServiceException(MemberErrorCode.E144.code(), "此账号已经拥有店铺");
            }
            //校验会员的有效性
            member.checkDisable();
            //校验此会员是否已经是店员
            ClerkDO clerk = clerkManager.getClerkByMemberId(member.getMemberId());
            if (clerk != null && !clerk.getShopId().equals(UserContext.getSeller().getSellerId())) {
                throw new ServiceException(MemberErrorCode.E145.code(), "此会员已经成为其他店铺店员");
            }
            if (clerk != null && clerk.getShopId().equals(UserContext.getSeller().getSellerId())) {
                throw new ServiceException(MemberErrorCode.E146.code(), "此会员已经是本店店员");
            }
            clerkAddVO.setResult("exist");
        } else {
            clerkAddVO.setResult("no exist");
        }
        //店员手机号码
        clerkAddVO.setMobile(mobile);
        return clerkAddVO;

    }

    @Operation(summary = "添加老会员为店员")
    @PostMapping("/old")
    @Log(client = LogClient.seller, detail = "添加店员，店员：${clerkDTO.mobile}",level = LogLevel.important)
    public ClerkDO addOldMemberClerk(@Valid ClerkDTO clerkDTO) {
        return this.clerkManager.addOldMemberClerk(clerkDTO);
    }


    @Operation(summary = "查询店员列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "disabled", description = "店员状态-1为禁用，0为正常", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "keyword", description = "关键字",   in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) Integer disabled, @Parameter(hidden = true) String keyword) {
        return this.clerkManager.list(pageNo, pageSize, disabled, keyword);
    }


    @Operation(summary = "添加店员")
    @PostMapping("/new")
    @Log(client = LogClient.seller, detail = "添加店员，店员:${clerkVO.uname}",level = LogLevel.important)
    public ClerkDO addNewMemberClerk(@Valid ClerkVO clerkVO) {
        return this.clerkManager.addNewMemberClerk(clerkVO);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除店员")
    @Parameters({
            @Parameter(name = "id", description = "要删除的店员主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        this.clerkManager.delete(id);
        return "";
    }


    @PutMapping(value = "/{id}")
    @Operation(summary = "修改店员")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    @Log(client = LogClient.seller, detail = "修改店员，店员id：${id}",level = LogLevel.important)
    public ClerkDO edit(@Valid ClerkVO clerkVO, @PathVariable Long id) {
        //获取店员
        ClerkDO clerk = this.clerkManager.getModel(id);
        if (clerk == null || !clerk.getShopId().equals(UserContext.getSeller().getSellerId())) {
            throw new NoPermissionException("无权限");
        }
        //校验权限，超级管理员不能修改成为普通管理员，普通管理员也不能修改成为超级管理员
        boolean bool = (clerkVO.getRoleId() == 0 && clerk.getRoleId() == 0) || (clerkVO.getRoleId() != 0 && clerk.getRoleId() != 0);
        if (!bool) {
            throw new ServiceException(MemberErrorCode.E136.code(), "权限操作错误");
        }
        //如果密码不为空则修改店员密码
        if (!StringUtil.isEmpty(clerkVO.getPassword())) {
            this.memberSecurityManager.updatePassword(clerk.getMemberId(), clerkVO.getPassword());
        }
        //修改邮箱及手机号码
        Member member = this.memberManager.getModel(clerk.getMemberId());
        if (member == null) {
            throw new ResourceNotFoundException("当前会员不存在");
        }
        member.setEmail(clerkVO.getEmail());
        member.setMobile(clerkVO.getMobile());
        this.memberManager.edit(member, member.getMemberId());
        //修改店员
        clerk.setRoleId(clerkVO.getRoleId());
        clerk.setClerkName(clerkVO.getUname());
        this.clerkManager.edit(clerk, id);
        return clerk;
    }


    @PutMapping(value = "/{id}/recovery")
    @Operation(summary = "恢复店员")
    @Parameters({
            @Parameter(name = "id", description = "店员id", required = true,  in = ParameterIn.PATH)
    })
    public String recovery(@PathVariable Long id) {
        //获取店员
        ClerkDO clerk = this.clerkManager.getModel(id);
        if (clerk == null || !clerk.getShopId().equals(UserContext.getSeller().getSellerId())) {
            throw new NoPermissionException("无权限");
        }
        //修改店员
        this.clerkManager.recovery(id);
        return "";
    }


}
