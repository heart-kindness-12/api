/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.trade;

import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.model.trade.snapshot.SnapshotVO;
import cn.shoptnt.service.trade.snapshot.GoodsSnapshotManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * 交易快照控制器
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-08-01 14:55:26
 */
@RestController
@RequestMapping("/seller/trade/snapshots")
@Tag(name = "交易快照相关API")
@Validated
public class GoodsSnapshotSellerController {

	@Autowired
	private	GoodsSnapshotManager goodsSnapshotManager;


	@GetMapping(value =	"/{id}")
	@Operation(summary	= "查询一个交易快照")
	@Parameters({
		@Parameter(name = "id",	description = "要查询的交易快照主键",	required = true, 	in = ParameterIn.PATH),
	})
	public SnapshotVO get(@PathVariable	Long	id)	{

		SnapshotVO goodsSnapshot = this.goodsSnapshotManager.get(id, Permission.SELLER.name());

		return	goodsSnapshot;
	}

}
