/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.aftersale;

import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Seller;
import cn.shoptnt.model.aftersale.dto.AfterSaleQueryParam;
import cn.shoptnt.model.aftersale.vo.AfterSaleExportVO;
import cn.shoptnt.model.aftersale.vo.ApplyAfterSaleVO;
import cn.shoptnt.model.aftersale.vo.PutInWarehouseVO;
import cn.shoptnt.model.errorcode.TradeErrorCode;
import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.service.aftersale.AfterSaleManager;
import cn.shoptnt.service.aftersale.AfterSaleQueryManager;
import cn.shoptnt.service.aftersale.SellerCreateTradeManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

/**
 * 售后服务相关API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-24
 */
@Tag(name = "售后服务相关API")
@RestController
@RequestMapping("/seller/after-sales")
@Validated
public class AfterSaleSellerController {

    @Autowired
    private AfterSaleManager afterSaleManager;

    @Autowired
    private AfterSaleQueryManager afterSaleQueryManager;

    @Autowired
    private SellerCreateTradeManager sellerCreateTradeManager;

    @Operation(summary = "获取申请售后服务记录列表")
    @GetMapping()
    public WebPage list(@Valid AfterSaleQueryParam param) {
        param.setSellerId(UserContext.getSeller().getSellerId());

        return afterSaleQueryManager.list(param);
    }

    @Operation(summary = "获取售后服务详细信息")
    @Parameters({
            @Parameter(name = "service_sn", description = "售后服务单号", required = true, in = ParameterIn.PATH)
    })
    @GetMapping(value = "/detail/{service_sn}")
    public ApplyAfterSaleVO detail(@PathVariable("service_sn") String serviceSn) {
        ApplyAfterSaleVO detail = afterSaleQueryManager.detail(serviceSn, Permission.SELLER);

        Seller seller = UserContext.getSeller();
        if (!seller.getSellerId().equals(detail.getSellerId())) {
            throw new ServiceException(TradeErrorCode.E460.code(), "操作订单无权限");
        }
        return detail;
    }

    @Operation(summary = "商家审核售后服务申请")
    @Parameters({
            @Parameter(name = "service_sn", description = "售后服务单号", required = true, in = ParameterIn.PATH),
            @Parameter(name = "audit_status", description = "审核状态 PASS：审核通过，REFUSE：审核未通过", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "refund_price", description = "退款金额", in = ParameterIn.QUERY),
            @Parameter(name = "return_addr", description = "退货地址信息", in = ParameterIn.QUERY),
            @Parameter(name = "audit_remark", description = "商家审核备注", in = ParameterIn.QUERY)
    })
    @PostMapping(value = "/audit/{service_sn}")
    @Log(client = LogClient.seller, detail = "审核售后服务单，单号：${serviceSn}")
    public void audit(@PathVariable("service_sn") String serviceSn, @Parameter(hidden = true) String auditStatus, @Parameter(hidden = true) Double refundPrice, @Parameter(hidden = true) String returnAddr, @Parameter(hidden = true) String auditRemark) {

        this.afterSaleManager.audit(serviceSn, auditStatus, refundPrice, returnAddr, auditRemark);
    }


    @Operation(summary = "导出售后服务信息")
    @GetMapping(value = "/export")
    public List<AfterSaleExportVO> export(@Valid AfterSaleQueryParam param) {
        param.setSellerId(UserContext.getSeller().getSellerId());
        return afterSaleQueryManager.exportAfterSale(param);
    }

    @Operation(summary = "商家为售后服务手动创建新订单")
    @Parameters({
            @Parameter(name = "service_sn", description = "售后服务单号", required = true, in = ParameterIn.PATH)
    })
    @PostMapping(value = "/create-order/{service_sn}")
    public void createOrder(@PathVariable("service_sn") String serviceSn) {

        this.sellerCreateTradeManager.sellerCreateTrade(serviceSn);
    }

    @Operation(summary = "商家关闭售后服务单")
    @Parameters({
            @Parameter(name = "service_sn", description = "售后服务单号", required = true, in = ParameterIn.PATH),
            @Parameter(name = "reason", description = "关闭原因", in = ParameterIn.QUERY)
    })
    @PostMapping(value = "/close/{service_sn}")
    public void close(@PathVariable("service_sn") String serviceSn, @Parameter(hidden = true) String reason) {

        this.afterSaleManager.closeAfterSale(serviceSn, reason);
    }

    @Operation(summary = "商家将申请售后服务退还的商品入库")
    @PostMapping(value = "/put-in/warehouse")
    @Log(client = LogClient.seller, detail = "售后服务单入库，单号：${putInWarehouseVO.serviceSn}")
    public void putInWarehouse(@Valid @RequestBody PutInWarehouseVO putInWarehouseVO) {

        this.afterSaleManager.putInWarehouse(putInWarehouseVO.getServiceSn(), putInWarehouseVO.getStorageList(), putInWarehouseVO.getRemark());
    }
}
