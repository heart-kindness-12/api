/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goods;

import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.client.trade.PintuanClient;
import cn.shoptnt.model.base.message.IndexCreateMessage;
import cn.shoptnt.model.promotion.pintuan.Pintuan;
import cn.shoptnt.model.promotion.tool.enums.PromotionStatusEnum;
import cn.shoptnt.model.system.vo.TaskProgressConstant;
import cn.shoptnt.model.util.progress.ProgressManager;
import cn.shoptnt.framework.exception.ResourceNotFoundException;
import cn.shoptnt.framework.message.direct.DirectMessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

/**
 * @author fk
 * @version v2.0
 * @Description: 商品全文检索
 * @date 2018/6/1915:55
 * @since v7.0.0
 */
@RestController
@RequestMapping("/admin/goods/search")
@Tag(name = "商品检索相关API")
public class GoodsSearchManagerController {

    @Autowired
    private ProgressManager progressManager;
    @Autowired
    private DirectMessageSender messageSender;

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private PintuanClient pintuanClient;

    @GetMapping
    @Operation(summary = "商品索引初始化")
    public String create(){

        if (progressManager.getProgress(TaskProgressConstant.GOODS_INDEX) != null) {
            throw new ResourceNotFoundException("有索引任务正在进行中，需等待本次任务完成后才能再次生成。");
        }
        /** 发送索引生成消息 */
        messageSender.send(new IndexCreateMessage());

        List<Pintuan> list = pintuanClient.get(PromotionStatusEnum.UNDERWAY.name());
        /** 获取商品数 */
        int goodsCount = this.goodsClient.queryGoodsCount();


        //检测此次任务是否发送成功
        while ((goodsCount + list.size()) > 0) {
            if(progressManager.getProgress(TaskProgressConstant.GOODS_INDEX) != null) {
                break;
            }
        }

        return TaskProgressConstant.GOODS_INDEX;
    }
}
