/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.member;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.member.dos.MemberAsk;
import cn.shoptnt.model.member.dto.AskQueryParam;
import cn.shoptnt.model.member.vo.BatchAuditVO;
import cn.shoptnt.service.member.MemberAskManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 会员商品咨询API
 *
 * @author duanmingyu
 * @version v2.0
 * @since v7.1.5
 * 2019-09-17
 */
@RestController
@RequestMapping("/admin/members/asks")
@Tag(name = "会员商品咨询API")
@Validated
public class MemberAskManagerController {

    @Autowired
    private MemberAskManager memberAskManager;


    @Operation(summary = "查询咨询列表")
    @GetMapping
    public WebPage list(@Valid AskQueryParam param) {

        return this.memberAskManager.list(param);
    }

    @Operation(summary = "批量审核会员商品咨询")
    @PostMapping("/batch/audit")
    public String batchAuditAsk(@Valid @RequestBody BatchAuditVO batchAuditVO) {

        this.memberAskManager.batchAudit(batchAuditVO);

        return "";
    }

    @Operation(summary = "删除咨询")
    @Parameters({
            @Parameter(name = "ask_id", description = "会员商品咨询id",  in = ParameterIn.PATH),
    })
    @DeleteMapping("/{ask_id}")
    public String delete(@PathVariable("ask_id")Long askId) {

        this.memberAskManager.delete(askId);

        return "";
    }

    @Operation(summary = "查询会员商品咨询详请")
    @Parameters({
            @Parameter(name = "ask_id", description = "主键ID", required = true,  in = ParameterIn.PATH)
    })
    @GetMapping("/{ask_id}")
    public MemberAsk get(@PathVariable("ask_id")Long askId) {
        return this.memberAskManager.getModel(askId);
    }

}
