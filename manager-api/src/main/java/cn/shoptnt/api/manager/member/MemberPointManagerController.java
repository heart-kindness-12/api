/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.member;

import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dos.MemberPointHistory;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.service.member.MemberPointHistoryManager;
import cn.shoptnt.service.member.MemberPointManager;
import cn.shoptnt.framework.context.user.AdminUserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.security.model.Admin;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 会员积分后台管理API
 *
 * @author zh
 * @version v7.0
 * @date 18/7/14 下午2:05
 * @since v7.0
 */
@RestController
@RequestMapping("/admin/members/point")
@Validated
@Tag(name = "会员积分后台管理API")
public class MemberPointManagerController {

    @Autowired
    private MemberPointManager memberPointManager;
    @Autowired
    private MemberManager memberManager;

    @Autowired
    MemberPointHistoryManager memberPointHistoryManager;

    @PutMapping(value = "/{member_id}")
    @Operation(summary = "修改会消费积分")
    @Parameters({
            @Parameter(name = "member_id", description = "会员id", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "point", description = "调整后的会员消费积分", required = true,  in = ParameterIn.QUERY)
    })
    public void editPoint(@PathVariable("member_id") Long memberId, @Min(value = 0, message = "消费积分不能小于0") Integer point) {
        //获取当前会员的积分 如果当前会员积分大于调整后的积分 则为消费，反之则为新增
        Member member = memberManager.getModel(memberId);
        Long currentPoint = member.getConsumPoint();
        //增加或者消费的积分数
        Long operationPoint = point - currentPoint;
        //操作类型  1为加积分 0为减积分或无操作
        Integer type = 0;
        if (operationPoint > 0) {
            type = 1;
        }
        //获取管理员信息
        Admin admin = AdminUserContext.getAdmin();
        MemberPointHistory memberPointHistory = new MemberPointHistory();
        memberPointHistory.setMemberId(memberId);
        memberPointHistory.setGradePointType(0);
        memberPointHistory.setGradePoint(0);
        memberPointHistory.setConsumPoint(Math.abs(operationPoint));
        memberPointHistory.setConsumPointType(type);
        memberPointHistory.setReason("管理员手工修改");
        memberPointHistory.setOperator(admin.getUsername());
        memberPointManager.pointOperation(memberPointHistory);

    }

    @Operation(summary = "查询会员积分列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "member_id", description = "会员id", required = true,  in = ParameterIn.PATH)
    })
    @GetMapping("/{member_id}")
    public WebPage list(@PathVariable("member_id") Long memberId, @Parameter(hidden = true) @NotNull(message = "页码不能为空") Long pageNo, @Parameter(hidden = true) @NotNull(message = "每页数量不能为空") Long pageSize) {
        return this.memberPointHistoryManager.list(pageNo, pageSize, memberId);
    }


}
