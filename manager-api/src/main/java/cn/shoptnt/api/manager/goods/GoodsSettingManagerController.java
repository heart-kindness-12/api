/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goods;

import cn.shoptnt.handler.AdminTwoStepAuthentication;
import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.client.system.SettingClient;
import cn.shoptnt.model.goods.dto.GoodsSettingVO;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.Valid;

/**
 * @author fk
 * @version v1.0
 * @Description: 商品设置控制器
 * @date 2018/5/25 10:31
 * @since v7.0.0
 */
@RestController
@RequestMapping("/admin/goods/settings")
@Tag(name = "商品设置API")
public class GoodsSettingManagerController {

    @Autowired
    private SettingClient settingClient;

    @Autowired
    private AdminTwoStepAuthentication adminTwoStepAuthentication;

    @GetMapping
    @Operation(summary = "获取商品审核设置信息")
    public GoodsSettingVO  getGoodsSetting(){

        String json = this.settingClient.get(SettingGroup.GOODS);
        return JsonUtil.jsonToObject(json,GoodsSettingVO.class);
    }

    @PostMapping
    @Operation(summary = "保存商品审核设置信息")
    @Log(client = LogClient.admin,detail = "修改商品设置参数",level = LogLevel.important)
    public GoodsSettingVO  save(@Valid GoodsSettingVO goodsSetting){

        //调用二次验证
        this.adminTwoStepAuthentication.sensitive();

        this.settingClient.save(SettingGroup.GOODS,goodsSetting);

        return goodsSetting;
    }

}
