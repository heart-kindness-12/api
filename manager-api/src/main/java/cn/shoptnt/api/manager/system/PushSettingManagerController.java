/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.service.base.service.SettingManager;
import cn.shoptnt.model.system.vo.AppPushSetting;
import cn.shoptnt.framework.util.JsonUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 推送设置api
 *
 * @author zh
 * @version v7.0
 * @date 18/5/18 下午6:55
 * @since v7.0
 */
@RestController
@RequestMapping("/admin/systems/push")
@Tag(name = "app推送设置")
@Validated
public class PushSettingManagerController {
    @Autowired
    private SettingManager settingManager;


    @GetMapping
    @Operation(summary = "获取推送设置")
    public AppPushSetting getPushSetting() {
        String appPushSettingJson = settingManager.get(SettingGroup.PUSH);
        AppPushSetting appPushSetting = JsonUtil.jsonToObject(appPushSettingJson,AppPushSetting.class);
        if (appPushSetting == null) {
            return new AppPushSetting();
        }
        return appPushSetting;
    }

    @PutMapping
    @Operation(summary = "修改推送设置")
    public AppPushSetting editPushSetting(@Valid AppPushSetting appPushSetting) {
        settingManager.save(SettingGroup.PUSH, appPushSetting);
        return appPushSetting;
    }

}
