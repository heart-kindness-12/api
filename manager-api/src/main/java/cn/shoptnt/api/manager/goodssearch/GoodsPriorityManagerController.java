/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goodssearch;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goods.dto.GoodsQueryParam;
import cn.shoptnt.model.goods.vo.GoodsPriorityVO;
import cn.shoptnt.service.goods.GoodsManager;
import cn.shoptnt.service.goods.GoodsQueryManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
* @author liuyulei
 * @version 1.0
 * @Description:  商品优先级相关API
 * @date 2019/6/10 14:49
 * @since v7.0
 */
@RestController
@RequestMapping("/admin/goodssearch/priority")
@Tag(name = "商品优先级相关API")
public class GoodsPriorityManagerController {


    @Autowired
    private GoodsQueryManager goodsQueryManager;

    @Autowired
    private GoodsManager goodsManager;

    @Operation(summary = "查询商品优先级列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "keywords", description = "关键字",   in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String keywords) {
        GoodsQueryParam goodsQueryParam = new GoodsQueryParam();
        goodsQueryParam.setPageNo(pageNo);
        goodsQueryParam.setPageSize(pageSize);
        goodsQueryParam.setKeyword(keywords);
        return this.goodsQueryManager.goodsPriorityList(goodsQueryParam);
    }

    @Operation(summary = "修改商品优先级")
    @Parameters({
            @Parameter(name = "goods_id", description = "商品id", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "priority", description = "优先级", required = true,  in = ParameterIn.QUERY,
            example = "高(3),中(2),低(1)")
    })
    @PutMapping
    public void updatePriority(@Parameter(hidden = true) Long goodsId, @Parameter(hidden = true) Integer priority) {
        this.goodsManager.updatePriority(goodsId,priority);
    }

}
