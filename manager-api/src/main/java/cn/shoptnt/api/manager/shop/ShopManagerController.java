/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.shop;


import cn.shoptnt.framework.security.model.TokenConstant;
import cn.shoptnt.framework.util.DbSecretUtil;
import cn.shoptnt.model.base.context.Region;
import cn.shoptnt.model.base.context.RegionFormat;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.model.errorcode.ShopErrorCode;
import cn.shoptnt.model.shop.vo.ShopParamsVO;
import cn.shoptnt.model.shop.vo.ShopVO;
import cn.shoptnt.model.shop.vo.operator.AdminEditShop;
import cn.shoptnt.service.shop.ShopManager;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.ServiceException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 店铺相关API
 *
 * @author zhangjiping
 * @version v7.0.0
 * @since v7.0.0
 * 2018年3月30日 上午10:58:45
 */
@Tag(name = "店铺相关API")
@RestController
@RequestMapping("/admin/shops")
@Validated
public class ShopManagerController {
    @Autowired
    private ShopManager shopManager;


    @Operation(summary = "分页查询店铺列表")
    @GetMapping()
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "分页数", required = true,  in = ParameterIn.QUERY)
    })
    public WebPage listPage(ShopParamsVO shopParams, @Parameter(hidden = true) @NotNull(message = "页码不能为空") Long pageNo, @Parameter(hidden = true) @NotNull(message = "每页数量不能为空") Long pageSize) {
        shopParams.setPageNo(pageNo);
        shopParams.setPageSize(pageSize);
        return shopManager.list(shopParams);
    }

    @Operation(summary = "分页查询列表")
    @GetMapping(value = "/list")
    public List<ShopVO> list() {
        return this.shopManager.list();
    }

    @Operation(summary = "管理员禁用店铺")
    @PutMapping(value = "/disable/{shop_id}")
    @Parameter(name = "shop_id", description = "店铺id", required = true,  in = ParameterIn.PATH)
    public AdminEditShop disShop(@Parameter(hidden = true) @PathVariable("shop_id") Long shopId) {
        //管理员对店铺进行操作
        AdminEditShop adminEditShop = new AdminEditShop();
        adminEditShop.setSellerId(shopId);
        adminEditShop.setOperator("管理员禁用店铺");
        shopManager.disShop(shopId);
        return adminEditShop;
    }

    @Operation(summary = "管理员恢复店铺使用")
    @PutMapping(value = "/enable/{shop_id}")
    @Parameter(name = "shop_id", description = "店铺id", required = true,  in = ParameterIn.PATH)
    public AdminEditShop useShop(@Parameter(hidden = true) @PathVariable("shop_id") Long shopId) {
        //管理员对店铺进行操作
        AdminEditShop adminEditShop = new AdminEditShop();
        adminEditShop.setSellerId(shopId);
        adminEditShop.setOperator("管理员恢复店铺");
        shopManager.useShop(shopId);
        return adminEditShop;
    }

    @Operation(summary = "管理员获取店铺详细")
    @GetMapping("/{shop_id}")
    @Parameter(name = "shop_id", description = "店铺id", required = true,  in = ParameterIn.PATH)
    public ShopVO getShopVO(@Parameter(hidden = true) @PathVariable("shop_id") @NotNull(message = "店铺id不能为空") Long shopId) {
        ShopVO shop = shopManager.getShop(shopId);

        if (shop == null) {
            throw new ServiceException(ShopErrorCode.E206.name(), "店铺不存在");
        }
        //服务器秘钥解密，会话秘钥加密
        if (shop.getCompanyPhone() != null) {
            String plainValue = DbSecretUtil.decrypt(shop.getCompanyPhone(), TokenConstant.SECRET);
            shop.setCompanyPhone(plainValue);
        }
        //服务器秘钥解密，会话秘钥加密
        if (shop.getLinkPhone() != null) {
            String plainValue = DbSecretUtil.decrypt(shop.getLinkPhone(), TokenConstant.SECRET);
            shop.setLinkPhone(plainValue);
        }

        //服务器秘钥解密，会话秘钥加密
        if (shop.getLegalId() != null) {
            shop.setLegalId(DbSecretUtil.decrypt(shop.getLegalId(), TokenConstant.SECRET));
        }

        //服务器秘钥解密，会话秘钥加密
        if (shop.getBankNumber() != null) {
            shop.setBankNumber(DbSecretUtil.decrypt(shop.getBankNumber(), TokenConstant.SECRET));
        }

        return shop;
    }

    @Operation(summary = "管理员修改审核店铺信息")
    @PutMapping("/{shop_id}")
    @Log(client = LogClient.admin, detail = "审核店铺，店铺id：${shopId}", level = LogLevel.important)
    @Parameter(name = "pass", description = "是否通过审核 1 通过 0 拒绝 编辑操作则不需传递", in = ParameterIn.QUERY)
    public ShopVO edit(@Valid ShopVO shop, Integer pass, @Parameter(hidden = true) @PathVariable("shop_id") Long shopId,
                       @RegionFormat @RequestParam("license_region") Region licenseRegion,
                       @RegionFormat @RequestParam("bank_region") Region bankRegion,
                       @RegionFormat @RequestParam("shop_region") Region shopRegion) {
        return this.shopManager.auditShopInfo(shop, pass, shopId, licenseRegion, bankRegion, shopRegion);
    }

    @Operation(summary = "后台添加店铺")
    @PostMapping()
    public ShopVO save(@Valid ShopVO shop,
                       @RegionFormat @RequestParam("license_region") Region licenseRegion,
                       @RegionFormat @RequestParam("bank_region") Region bankRegion,
                       @RegionFormat @RequestParam("shop_region") Region shopRegion) {

        //店铺信息
        shop = this.shopManager.buildRegionInfo(shop, licenseRegion, bankRegion, shopRegion);
        //后台注册店铺
        this.shopManager.registStore(shop);
        return shop;
    }
}
