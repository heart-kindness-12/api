/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.promotion;

import cn.shoptnt.model.promotion.groupbuy.vo.GroupbuyGoodsVO;
import cn.shoptnt.model.promotion.groupbuy.vo.GroupbuyQueryParam;
import cn.shoptnt.service.promotion.groupbuy.GroupbuyGoodsManager;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.validation.constraints.NotNull;


/**
 * 平台团购商品管理控制器
 *
 * @author Snow create in 2018/4/25
 * @version v2.0
 * @since v7.0.0
 */
@RestController
@RequestMapping("/admin/promotion/group-buy-goods")
@Tag(name = "团购商品管理API")
@Validated
public class GroupbuyGoodsManagerController {

    @Autowired
    private GroupbuyGoodsManager groupbuyGoodsManager;

    @Operation(summary = "查询团购商品列表")
    @Parameters({
            @Parameter(name = "act_id", description = "团购活动id", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "goods_name", description = "商品名称", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺ID", in = ParameterIn.QUERY),
            @Parameter(name = "gb_name", description = "团购名称", in = ParameterIn.QUERY),
            @Parameter(name = "gb_status", description = "审核状态 0:待审核，1：通过审核，2：未通过审核", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "条数", in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage<GroupbuyGoodsVO> list(@Parameter(hidden = true) @NotNull(message = "活动ID必传") Long actId, @Parameter(hidden = true) String goodsName, @Parameter(hidden = true) Long sellerId, @Parameter(hidden = true) String gbName,
                                         @Parameter(hidden = true) Integer gbStatus, @Parameter(hidden = true) Long pageNo,@Parameter(hidden = true) Long pageSize) {

        GroupbuyQueryParam param = new GroupbuyQueryParam();
        param.setActId(actId);
        param.setGoodsName(goodsName);
        param.setSellerId(sellerId);
        param.setGbName(gbName);
        param.setGbStatus(gbStatus);
        param.setPage(pageNo);
        param.setPageSize(pageSize);
        param.setClientType("ADMIN");
        WebPage webPage = this.groupbuyGoodsManager.listPage(param);
        return webPage;
    }


    @Operation(summary = "查询团购商品信息")
    @Parameters({
            @Parameter(name = "gb_id", description = "团购商品信息", required = true, in = ParameterIn.PATH)
    })
    @GetMapping(value = "/{gb_id}")
    public GroupbuyGoodsVO get(@Parameter(hidden = true) @PathVariable("gb_id") Long gbId) {
        GroupbuyGoodsVO goodsDO = this.groupbuyGoodsManager.getModel(gbId);
        return goodsDO;
    }

}
