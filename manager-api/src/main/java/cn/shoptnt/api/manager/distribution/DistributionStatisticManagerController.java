/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.distribution;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.errorcode.DistributionErrorCode;
import cn.shoptnt.service.distribution.exception.DistributionException;
import cn.shoptnt.service.distribution.DistributionStatisticManager;
import cn.shoptnt.model.statistics.vo.SimpleChart;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 统计
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/24 下午3:15
 */
@RestController
@RequestMapping("/admin/distribution/statistic")
@Tag(name = "统计")
public class DistributionStatisticManagerController {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DistributionStatisticManager distributionStatisticManager;

    @Operation(summary = "订单金额统计")
    @GetMapping("/order")
    @Parameters({
            @Parameter(name = "circle", description = "搜索类型：YEAR/MONTH", in = ParameterIn.QUERY),
            @Parameter(name = "member_id", description = "会员id", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY)
    })
    public SimpleChart order(String circle, Long memberId, Integer year, Integer month) {
        try {
            if (memberId == null) {
                throw new DistributionException(DistributionErrorCode.E1011.code(), DistributionErrorCode.E1011.des());
            }
            return distributionStatisticManager.getOrderMoney(circle, memberId, year, month);
        } catch (DistributionException e) {
            throw e;
        } catch (Exception e) {
            logger.error("统计金额异常：", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }

    @Operation(summary = "订单数量统计")
    @GetMapping("/count")
    @Parameters({
            @Parameter(name = "circle", description = "搜索类型：YEAR/MONTH", in = ParameterIn.QUERY),
            @Parameter(name = "member_id", description = "会员id", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY)
    })
    public SimpleChart count(String circle, Long memberId, Integer year, Integer month) {
        try {
            if (memberId == null) {
                throw new DistributionException(DistributionErrorCode.E1011.code(), DistributionErrorCode.E1011.des());
            }
            return distributionStatisticManager.getOrderCount(circle, memberId, year, month);
        } catch (DistributionException e) {
            throw e;
        } catch (Exception e) {
            logger.error("统计金额异常：", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }

    @Operation(summary = "订单返现统计")
    @GetMapping("/push")
    @Parameters({
            @Parameter(name = "circle", description = "搜索类型：YEAR/MONTH", in = ParameterIn.QUERY),
            @Parameter(name = "member_id", description = "会员id", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY)
    })
    public SimpleChart push(String circle, Long memberId, Integer year, Integer month) {
        try {
            if (memberId == null) {
                throw new DistributionException(DistributionErrorCode.E1011.code(), DistributionErrorCode.E1011.des());
            }
            return distributionStatisticManager.getPushMoney(circle, memberId, year, month);
        } catch (DistributionException e) {
            throw e;
        } catch (Exception e) {
            logger.error("统计金额异常：", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }


    @Operation(summary = "店铺返现统计")
    @GetMapping("/push/seller")
    @Parameters({
            @Parameter(name = "circle", description = "搜索类型：YEAR/MONTH", in = ParameterIn.QUERY),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY)
    })
    public WebPage pushShop(String circle, Integer year, Integer month, Long pageSize, Long pageNo) {
        try {
            return distributionStatisticManager.getShopPush(circle, year, month, pageSize, pageNo);
        } catch (DistributionException e) {
            throw e;
        } catch (Exception e) {
            logger.error("统计金额异常：", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }


}
