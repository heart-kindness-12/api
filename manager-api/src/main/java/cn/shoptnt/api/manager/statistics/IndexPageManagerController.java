/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.statistics;

import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.base.vo.BackendIndexModelVO;
import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.client.member.MemberClient;
import cn.shoptnt.model.statistics.enums.QueryDateType;
import cn.shoptnt.model.statistics.vo.SalesTotal;
import cn.shoptnt.service.statistics.OrderStatisticManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

/**
 * 后台首页api
 *
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018-06-29 上午8:13
 */
@RestController
@RequestMapping("/admin/index/page")
@Tag(name = "后台首页api")
@Validated
public class IndexPageManagerController {

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private MemberClient memberClient;

    @Autowired
    private OrderStatisticManager orderStatisticManager;

    @GetMapping
    @Operation(summary = "首页响应")
    public BackendIndexModelVO index() {
        SearchCriteria searchCriteria = new SearchCriteria();
        LocalDate localDate = LocalDate.now();
        searchCriteria.setYear(localDate.getYear());
        searchCriteria.setMonth(localDate.getMonthValue());
        searchCriteria.setCycleType(QueryDateType.MONTH.value());
        SalesTotal salesTotal = orderStatisticManager.getSalesMoneyTotal(searchCriteria);

        BackendIndexModelVO backendIndexModelVO = new BackendIndexModelVO();
        backendIndexModelVO.setSalesTotal(salesTotal);
        backendIndexModelVO.setGoodsVos(goodsClient.newGoods(5));
        backendIndexModelVO.setMemberVos(memberClient.newMember(5));
        return backendIndexModelVO;
    }


}
