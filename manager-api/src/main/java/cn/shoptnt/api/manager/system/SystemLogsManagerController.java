/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.model.system.dos.SystemLogs;
import cn.shoptnt.model.system.dto.SystemLogsParam;
import cn.shoptnt.service.system.SystemLogsManager;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


/**
 * 系统日志控制器
 *
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2021-03-22 16:05:59
 */
@RestController
@RequestMapping("/admin/system-logs")
@Tag(name = "系统日志相关API")
public class SystemLogsManagerController {

    @Autowired
    private SystemLogsManager systemLogsManager;


    @Operation(summary = "查询系统日志列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true, in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(SystemLogsParam param, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {

        param.setPageNo(pageNo);
        param.setPageSize(pageSize);


        return this.systemLogsManager.list(param, "admin");
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个系统日志")
    @Parameters({
            @Parameter(name = "id", description = "要查询的系统日志主键", required = true, in = ParameterIn.PATH)
    })
    public SystemLogs get(@PathVariable Long id) {

        SystemLogs systemLogs = this.systemLogsManager.getModel(id);

        return systemLogs;
    }

}