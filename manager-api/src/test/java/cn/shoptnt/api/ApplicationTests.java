/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by 妙贤 on 2018/3/28.
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/3/28
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan("cn.shoptnt")
@Transactional
@Rollback()
public class ApplicationTests {
    @Test
    public void contextLoads() {
    }
}
