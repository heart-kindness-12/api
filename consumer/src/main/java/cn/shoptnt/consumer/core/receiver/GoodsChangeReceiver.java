/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.goods.GoodsChangeDispatcher;
import cn.shoptnt.model.base.message.GoodsChangeMsg;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 商品变化消费者
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018年3月23日 上午10:29:54
 */
@Component
public class GoodsChangeReceiver {

    @Autowired
    private GoodsChangeDispatcher dispatcher;

    /**
     * 商品变化
     *
     * @param goodsChangeMsg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.GOODS_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.GOODS_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void goodsChange(GoodsChangeMsg goodsChangeMsg) {
        dispatcher.dispatch(goodsChangeMsg);
    }
}
