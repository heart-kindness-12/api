/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.distribution;

import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.database.DaoSupport;
import cn.shoptnt.framework.test.BaseTest;
import cn.shoptnt.message.consumer.distribution.DistributionRegisterConsumer;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.base.message.MemberRegisterMsg;
import cn.shoptnt.model.distribution.dos.DistributionDO;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.service.distribution.DistributionManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;

/**
 * 注册后添加分销商
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/6/13 下午11:33
 */
@Rollback
public class DistributionRegisterConsumerTest extends BaseTest {


    @Autowired
    private Cache cache;


    @Autowired
    private DistributionManager distributionManager;

    @Autowired
    @Qualifier("distributionDaoSupport")
    private DaoSupport daoSupport;


    @Autowired
    private DistributionRegisterConsumer distributionRegisterConsumer;


    @Before
    public void beforeDistribution() {
       // DistributionBeforeTest.before(daoSupport);
    }




    @Test
    public void memberRegister() throws Exception {
        Member member = new Member();
        member.setUname("test123");
        member.setMemberId(123L);
        MemberRegisterMsg memberRegisterMsg = new MemberRegisterMsg();
        memberRegisterMsg.setMember(member);
        String uuid="uuid_uuid";
        memberRegisterMsg.setUuid(uuid);
        cache.put(CachePrefix.DISTRIBUTION_UP+uuid, "1");

        distributionRegisterConsumer.memberRegister(memberRegisterMsg);
        DistributionDO ddo = distributionManager.getDistributorByMemberId(123l);

        DistributionDO distributionDO = new DistributionDO();
        distributionDO.setMemberName("test123");
        distributionDO.setMemberId(123L);
        distributionDO.setPath("|0|1|123|");
        distributionDO.setMemberIdLv1(1l);
        distributionDO.setCurrentTplId(1l);
        distributionDO.setCurrentTplName("模版1");

        Assert.assertEquals(ddo.toString(), distributionDO.toString());

    }
    @Test
    public void memberRegister1() throws Exception {
        Member member = new Member();
        member.setUname("test123");
        member.setMemberId(123L);
        MemberRegisterMsg memberRegisterMsg = new MemberRegisterMsg();
        memberRegisterMsg.setMember(member);
        String uuid="uuid_uuid";
        memberRegisterMsg.setUuid(uuid);
        cache.put(CachePrefix.DISTRIBUTION_UP+uuid, "3");

        distributionRegisterConsumer.memberRegister(memberRegisterMsg);
        DistributionDO ddo = distributionManager.getDistributorByMemberId(123l);

        DistributionDO distributionDO = new DistributionDO();
        distributionDO.setMemberName("test123");
        distributionDO.setMemberId(123L);
        distributionDO.setPath("|0|1|2|3|123|");
        distributionDO.setMemberIdLv1(3l);
        distributionDO.setMemberIdLv2(2l);
        distributionDO.setCurrentTplId(1l);
        distributionDO.setCurrentTplName("模版1");

        Assert.assertEquals(ddo.toString(), distributionDO.toString());

    }
}
