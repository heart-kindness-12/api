/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.auth;

/**
 * Token创建接口
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-06-21
 */
public interface TokenCreater {


    /**
     * 创建token
     * @param user 用户
     * @return token
     */
    Token create(AuthUser user);

}
