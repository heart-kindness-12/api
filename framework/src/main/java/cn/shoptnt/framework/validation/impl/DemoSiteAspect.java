/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.validation.impl;

import cn.shoptnt.framework.ShopTntConfig;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.exception.SystemErrorCodeV1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.validation.annotation.DemoSiteDisable;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 演示站点是否可以被操作切面类
 *
 * @author zh
 * @version v7.0
 * @date 19/2/25 上午8:58
 * @since v7.0
 */
@Component
@Aspect
public class DemoSiteAspect {

    @Autowired
    private ShopTntConfig shoptntConfig;

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Before("@annotation(demoSiteDisable)")
    public void doAfter(DemoSiteDisable demoSiteDisable) throws Exception {
        if (shoptntConfig.getIsDemoSite()) {
            logger.debug("演示站点禁止此操作");
            throw new ServiceException(SystemErrorCodeV1.NO_PERMISSION, "演示站点禁止此操作");
        }
    }

}
