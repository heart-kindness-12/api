/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.elasticsearch.core;

import cn.shoptnt.framework.elasticsearch.ElasticBuilderUtil;
import com.google.gson.JsonElement;
import io.searchbox.core.SearchResult;

/**
 * 重写 jest 搜索结果类
 * @author liuyulei
 * @version 1.0
 * @since 7.2.2
 * 2021/1/21  11:10
 */
public class ElasticSearchResult extends SearchResult {


    public static final String[] NEW_PATH_TO_TOTAL = "hits/total/value".split("/");

    public ElasticSearchResult(SearchResult searchResult) {
        super(searchResult);
    }

    @Override
    public Long getTotal() {
        if (ElasticBuilderUtil.version.startsWith(ElasticEnumVersion.ES6.getVersion())) {
            return super.getTotal();
        } else  if (ElasticBuilderUtil.version.startsWith(ElasticEnumVersion.ES7.getVersion())) {
            // 7.0 以上版本 使用此方式获取
            Long total = null;
            JsonElement obj = getPath(NEW_PATH_TO_TOTAL);
            if (obj != null) {
                total = obj.getAsLong();
            }
            return total;
        } else {
            return 0L;
        }
    }
}
