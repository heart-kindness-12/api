package cn.shoptnt.framework.message.direct;

/**
 * 消息接口
 * 消息机制中发送的消息应该实现此接口
 * 并定义交换机名字
 * @author kingapex
 * @version 1.0
 * @data 2022/10/20 14:59
 **/
public interface DirectMessage {

    /**
     * 定义mq中的交换机名称
     * @return
     */
    String getExchange();



}
