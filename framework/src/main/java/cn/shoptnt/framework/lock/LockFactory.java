package cn.shoptnt.framework.lock;

/**
 * 全局锁
 * @author kingapex
 * @version 1.0
 * @data 2022/10/21 18:52
 **/
public interface LockFactory {
    Lock getLock(String lockName);
}
